@echo off
title MMOCore Dev.Team: Login Server Console
:start
echo MMOCore Dev.Team Login Server.
echo.
java -Xms32m -Xmx32m -cp ./../libs/*;l2pserver.jar l2p.loginserver.L2LoginServer
if ERRORLEVEL 2 goto restart
if ERRORLEVEL 1 goto error
goto end
:restart
echo.
echo Admin Restart ...
echo.
goto start
:error
echo.
echo Server terminated abnormaly
echo.
:end
echo.
echo server terminated
echo.
pause