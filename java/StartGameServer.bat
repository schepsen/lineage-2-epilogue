@echo off
title MMOCore Dev.Team: Game Server Console
:start
echo Starting MMOCore Dev.Team Game Server.
echo.
java -Dfile.encoding=UTF-8 -Xms1024m -Xmx1024m -cp ./../libs/*;l2pserver.jar l2p.gameserver.GameServer
if ERRORLEVEL 2 goto restart
if ERRORLEVEL 1 goto error
goto end
:restart
echo.
echo Admin Restart ...
echo.
goto start
:error
echo.
echo Server terminated abnormaly
echo.
:end
echo.
echo server terminated
echo.
pause