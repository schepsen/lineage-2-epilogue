package l2p.extensions.scripts;

import l2p.gameserver.handler.IAdminCommandHandler;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.model.quest.Quest;
import l2p.gameserver.model.quest.QuestState;

public class AdminScripts implements IAdminCommandHandler
{
	private static enum Commands
	{
		admin_scripts_reload,
		admin_sreload,
		admin_sqreload
	}

	public boolean useAdminCommand(Enum comm, String[] wordList, String fullString, L2Player activeChar)
	{
		Commands command = (Commands) comm;
		if(!activeChar.isGM())
		{
			return false;
		}
		switch(command)
		{
			case admin_scripts_reload:
			case admin_sreload:
				if(wordList.length < 2)
				{
					return false;
				}
				String param = wordList[1];
				if(param.equalsIgnoreCase("all"))
				{
					activeChar.sendMessage("Scripts reload starting...");
					if(ScriptManager.getInstance().reload())
					{
						activeChar.sendMessage("Scripts reloaded with errors. Loaded " + ScriptManager.getInstance().getClasses().size() + " classes.");
					}
					else
					{
						activeChar.sendMessage("Scripts successfully reloaded. Loaded " + ScriptManager.getInstance().getClasses().size() + " classes.");
					}
				}
				else if(ScriptManager.getInstance().reloadClass(param))
				{
					activeChar.sendMessage("Scripts reloaded with errors. Loaded " + ScriptManager.getInstance().getClasses().size() + " classes.");
				}
				else
				{
					activeChar.sendMessage("Scripts successfully reloaded. Loaded " + ScriptManager.getInstance().getClasses().size() + " classes.");
				}
				break;
			case admin_sqreload:
				if(wordList.length < 2)
				{
					return false;
				}
				String quest = wordList[1];
				if(ScriptManager.getInstance().reloadQuest(quest))
				{
					activeChar.sendMessage("Quest '" + quest + "' reloaded with errors.");
				}
				else
				{
					activeChar.sendMessage("Quest '" + quest + "' successfully reloaded.");
				}
				reloadQuestStates(activeChar);
				break;
		}
		return true;
	}

	private void reloadQuestStates(L2Player p)
	{
		for(QuestState qs : p.getAllQuestsStates())
		{
			p.delQuestState(qs.getQuest().getName());
		}
		Quest.playerEnter(p);
	}

	public Enum[] getAdminCommandEnum()
	{
		return Commands.values();
	}
}