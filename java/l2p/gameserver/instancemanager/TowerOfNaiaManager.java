package l2p.gameserver.instancemanager;

import javolution.util.FastList;
import l2p.Config;
import l2p.common.ThreadPoolManager;
import l2p.gameserver.ai.NaiaSpore;
import l2p.gameserver.idfactory.IdFactory;
import l2p.gameserver.model.instances.L2MonsterInstance;
import l2p.gameserver.tables.NpcTable;
import l2p.util.Location;

/**
 * @author hex1r0
 */
public class TowerOfNaiaManager
{
	public static enum Attribute
	{
		FIRE,
		WATER,
		WIND,
		EARTH
	}
	
	public static final int SPORE_FIRE_ID = 25605;
	public static final int SPORE_WATER_ID = 25606;
	public static final int SPORE_WIND_ID = 25607;
	public static final int SPORE_EARTH_ID = 25608;
	
	public static final int EPIDOS_FIRE_ID = 25609;
	public static final int EPIDOS_WATER_ID = 25610;
	public static final int EPIDOS_WIND_ID = 256011;
	public static final int EPIDOS_EARTH_ID = 25612;
	
	public static final Location CENTRAL_COLUMN = new Location(-45474, 247450, -13994);
	public static final Location TELEPORT_LOCATION = new Location(-46344, 247784, -14207);
	
	private static Attribute 	_currentAttribute = Attribute.FIRE;
	private static int 			_currentEpidosIndex = 0;
	private static boolean 		_isEpidosSpawned = false;
	
	private static L2MonsterInstance _roofLock = null;
	
	private static FastList<L2MonsterInstance> _spores = new FastList<L2MonsterInstance>();
	
	
	public synchronized static void handleEpidosIndex(L2MonsterInstance naiaSpore)
	{
		if (naiaSpore == null)
			return;
		
		if (_isEpidosSpawned)
			return;
		
		/*int i = _spores.indexOf(naiaSpore);
		if (i != -1)
			_spores.remove(i);*/
		
		int sporeId = naiaSpore.getNpcId();
		switch(sporeId)
		{
			case SPORE_FIRE_ID:
				if (_currentAttribute == Attribute.FIRE)
					_currentEpidosIndex += Config.NAIA_SAME_ELEMENT_INDEX_STEP;
				else if (_currentAttribute == Attribute.WATER)
					_currentEpidosIndex += Config.NAIA_OPPOSITE_ELEMENT_INDEX_STEP;
				else
					_currentEpidosIndex += Config.NAIA_RELATIVE_ELEMENT_INDEX_STEP;
				break;
				
			case SPORE_WATER_ID:
				if (_currentAttribute == Attribute.WATER)
					_currentEpidosIndex += Config.NAIA_SAME_ELEMENT_INDEX_STEP;
				else if (_currentAttribute == Attribute.FIRE)
					_currentEpidosIndex += Config.NAIA_OPPOSITE_ELEMENT_INDEX_STEP;
				else
					_currentEpidosIndex += Config.NAIA_RELATIVE_ELEMENT_INDEX_STEP;
				break;
				
			case SPORE_WIND_ID:
				if (_currentAttribute == Attribute.WIND)
					_currentEpidosIndex += Config.NAIA_SAME_ELEMENT_INDEX_STEP;
				else if (_currentAttribute == Attribute.EARTH)
					_currentEpidosIndex += Config.NAIA_OPPOSITE_ELEMENT_INDEX_STEP;
				else
					_currentEpidosIndex += Config.NAIA_RELATIVE_ELEMENT_INDEX_STEP;
				break;
				
			case SPORE_EARTH_ID:
				if (_currentAttribute == Attribute.EARTH)
					_currentEpidosIndex += Config.NAIA_SAME_ELEMENT_INDEX_STEP;
				else if (_currentAttribute == Attribute.WIND)
					_currentEpidosIndex += Config.NAIA_OPPOSITE_ELEMENT_INDEX_STEP;
				else
					_currentEpidosIndex += Config.NAIA_RELATIVE_ELEMENT_INDEX_STEP;
				break;
		}
		
		if (_currentEpidosIndex >= Config.NAIA_MAX_EPIDOS_INDEX)
		{
			for (L2MonsterInstance spore : _spores)
			{
				if (spore != null /*&& !spore.isDead()*/)
				{
					NaiaSpore ai = (NaiaSpore) spore.getAI();
					if (ai != null)
					{
						ai.notifyEpidosIndexReached();
						ThreadPoolManager.getInstance().scheduleGeneral(new SpawnEpidosTask(_currentAttribute), 3000);
						_isEpidosSpawned = true;
					}
				}
			}
		}
		else if (_currentEpidosIndex <= 0)
		{
			switch(sporeId)
			{
				case SPORE_FIRE_ID:
					_currentAttribute = Attribute.FIRE;
					break;
					
				case SPORE_WATER_ID:
					_currentAttribute = Attribute.WATER;
					break;
					
				case SPORE_WIND_ID:
					_currentAttribute = Attribute.WIND;
					break;
					
				case SPORE_EARTH_ID:
					_currentAttribute = Attribute.EARTH;
					break;
			}
		}
	}
	
	public synchronized static void addSpore(L2MonsterInstance naiaSpore)
	{
		int i = _spores.indexOf(naiaSpore);
		if (i != -1)
			_spores.remove(i);
		
		_spores.add(naiaSpore);
	}
	
	public static void registerRoofLock(L2MonsterInstance roofLock)
	{
		_roofLock = roofLock; 
	}
	
	public static L2MonsterInstance getRoofLock()
	{
		return _roofLock;
	}
	
	private static class SpawnEpidosTask implements Runnable
	{
		private Attribute _attribute;
		
		public SpawnEpidosTask(Attribute attribute)
		{
			_attribute = attribute;
		}

		@Override
		public void run()
		{
			int epidosId = 0;
			switch (_attribute)
			{
				case FIRE:
					epidosId = EPIDOS_WATER_ID;
					break;
					
                case WATER:
                	epidosId = EPIDOS_FIRE_ID;
                	break;
                	
                case WIND:
                	epidosId = EPIDOS_EARTH_ID;
                	break;
                	
                case EARTH:
                	epidosId = EPIDOS_WIND_ID;
                	break;
			}
			
			L2MonsterInstance epidos = new L2MonsterInstance(IdFactory.getInstance().getNextId(), NpcTable.getTemplate(epidosId));
			epidos.setSpawnedLoc(CENTRAL_COLUMN);
			epidos.onSpawn();
			epidos.spawnMe(CENTRAL_COLUMN);
			
			ThreadPoolManager.getInstance().scheduleGeneral(new TeleportEpidosTask(epidos), 3000);
		}	
	}
	
	private static class TeleportEpidosTask implements Runnable
	{
		L2MonsterInstance _mob = null;
		
		public TeleportEpidosTask(L2MonsterInstance mob)
		{
			_mob = mob;
		}

		@Override
		public void run()
		{
			if (_mob != null)
				_mob.teleToLocation(TELEPORT_LOCATION);
		}
	}

	public static boolean isEpidosSpawned()
	{
		return _isEpidosSpawned;
	}
}
