package l2p.gameserver.tables;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javolution.util.FastMap;

import l2p.database.DatabaseUtils;
import l2p.database.FiltredPreparedStatement;
import l2p.database.L2DatabaseFactory;
import l2p.database.ThreadConnection;
import l2p.gameserver.model.FakePc;

/**
 * Fake PCs <BR>
 * 
 * @author Fonduee aka FewG
 */

public class FakePcsTable {

    private static boolean _initialized = false;
    private static Logger _log = Logger.getLogger(FakePcsTable.class.getName());
    private FastMap<Integer, FakePc> _fakePcs = new FastMap<Integer, FakePc>();

    private FakePcsTable() {
        loadData();
    }

    private void loadData() {

        ThreadConnection con = null;
        FiltredPreparedStatement statement = null;
        ResultSet rs = null;

        try {
            con = L2DatabaseFactory.getInstance().getConnection();
            statement = con.prepareStatement("SELECT * FROM `fake_pcs`");
            rs = statement.executeQuery();
            FakePc fpc = null;
            /**
             * Anzahl der "Fake Pc"-Einträgen
             */
            int count = 0;
            while (rs.next()) {

                fpc = new FakePc();
                int npcId = rs.getInt("npc_id");
                /**
                 * NPC Info
                 */
                fpc.name = rs.getString("name");
                fpc.nameColor = Integer.decode("0x" + rs.getString("name_color"));
                fpc.title = rs.getString("title");
                fpc.titleColor = Integer.decode("0x" + rs.getString("title_color"));
                fpc.race = rs.getInt("race");
                fpc.sex = rs.getInt("sex");
                fpc.clazz = rs.getInt("class");
                fpc.hairStyle = rs.getInt("hair_style");
                fpc.hairColor = rs.getInt("hair_color");
                fpc.face = rs.getInt("face");
                /**
                 * Equipment items 1
                 */
                fpc.pdUnder = rs.getInt("pd_under");
                fpc.pdHead = rs.getInt("pd_head");
                fpc.pdRHand = rs.getInt("pd_rhand");
                fpc.pdLHand = rs.getInt("pd_lhand");
                fpc.pdGloves = rs.getInt("pd_gloves");
                fpc.pdChest = rs.getInt("pd_chest");
                fpc.pdLegs = rs.getInt("pd_legs");
                fpc.pdFeet = rs.getInt("pd_feet");
                fpc.pdCloak = rs.getInt("pd_cloak");
                fpc.pdLRHand = 0; // Fix me!
                /**
                 * Equipment items 2
                 */
                fpc.pdHair = rs.getInt("pd_hair");
                fpc.pdHair2 = rs.getInt("pd_hair2");
                fpc.pdBelt = rs.getInt("pd_belt");
                fpc.pdRBracelet = rs.getInt("pd_rbracelet");
                fpc.pdLBracelet = rs.getInt("pd_lbracelet");
                /**
                 * Equipment items 3
                 */
                fpc.pdDeco1 = rs.getInt("pd_deco1");
                fpc.pdDeco2 = rs.getInt("pd_deco2");
                fpc.pdDeco3 = rs.getInt("pd_deco3");
                fpc.pdDeco4 = rs.getInt("pd_deco4");
                fpc.pdDeco5 = rs.getInt("pd_deco5");
                fpc.pdDeco6 = rs.getInt("pd_deco6");
                /**
                 * Equipment augmentation 1
                 */
                fpc.pdUnderAug = rs.getInt("pd_under_aug");
                fpc.pdHeadAug = rs.getInt("pd_head_aug");
                fpc.pdRHandAug = rs.getInt("pd_rhand_aug");
                fpc.pdLHandAug = rs.getInt("pd_lhand_aug");
                fpc.pdGlovesAug = rs.getInt("pd_gloves_aug");
                fpc.pdChestAug = rs.getInt("pd_chest_aug");
                fpc.pdLegsAug = rs.getInt("pd_legs_aug");
                fpc.pdFeetAug = rs.getInt("pd_feet_aug");
                fpc.pdCloakAug = rs.getInt("pd_cloak_aug");
                fpc.pdLRHandAug = 0; // Fix me!
                /**
                 * Equipment augmentation 2
                 */
                fpc.pdHairAug = rs.getInt("pd_hair_aug");
                fpc.pdHair2Aug = rs.getInt("pd_hair2_aug");
                fpc.pdBeltAug = rs.getInt("pd_belt_aug");
                fpc.pdRBraceletAug = rs.getInt("pd_rbracelet_aug");
                fpc.pdLBraceletAug = rs.getInt("pd_lbracelet_aug");
                /**
                 * Equipment augmentation 3
                 */
                fpc.pdDeco1Aug = rs.getInt("pd_deco1_aug");
                fpc.pdDeco2Aug = rs.getInt("pd_deco2_aug");
                fpc.pdDeco3Aug = rs.getInt("pd_deco3_aug");
                fpc.pdDeco4Aug = rs.getInt("pd_deco4_aug");
                fpc.pdDeco5Aug = rs.getInt("pd_deco5_aug");
                fpc.pdDeco6Aug = rs.getInt("pd_deco6_aug");
                /**
                 * NPC Status
                 */
                fpc.pvpFlag = rs.getBoolean("pvp_flag");
                fpc.karma = rs.getInt("karma");
                fpc.invisible = rs.getBoolean("invisible");
                fpc.mount = rs.getByte("mount");
                fpc.team = rs.getByte("team");
                fpc.hero = rs.getBoolean("hero");
                /**
                 * Fishing Status
                 */
                fpc.fishing = rs.getBoolean("fishing");
                fpc.fishingX = rs.getInt("fishing_x");
                fpc.fishingY = rs.getInt("fishing_y");
                fpc.fishingZ = rs.getInt("fishing_z");

                _fakePcs.put(npcId, fpc);
                count++;
            }
            rs.close();
            statement.close();
            _log.log(Level.INFO, "FakePCsTable: Loaded ===> " + count + " Fake PC(s).");
        } catch (SQLException e) {
            _log.log(Level.SEVERE, "Error while loading FakePcsTable: " + e.getMessage(), e);
        } finally {
            DatabaseUtils.closeDatabaseCS(con, statement);
        }
        _initialized = true;
    }

    public void reloadData() {
        /**
         * Leere den _fakePcs -Array
         */
        _fakePcs.clear();
        /**
         * Lade die FakePcsTabele erneut
         */
        loadData();
    }

    public FakePc getFakePc(int npcId) {
        return _fakePcs.get(npcId);
    }

    public static FakePcsTable getInstance() {
        return SingletonHolder._instance;
    }

    public static boolean isInitialized() {
        return _initialized;
    }

    @SuppressWarnings("synthetic-access")
    private static class SingletonHolder {
        protected static final FakePcsTable _instance = new FakePcsTable();
    }
}