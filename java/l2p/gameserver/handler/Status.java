package l2p.gameserver.handler;

import javolution.text.TextBuilder;
import l2p.Config;
import l2p.extensions.scripts.Functions;
import l2p.gameserver.Shutdown;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.taskmanager.MemoryWatchDog;
import l2p.util.Stats;

import java.util.Calendar;

public class Status extends Functions implements IVoicedCommandHandler
{
	private final String[] _commandList = new String[] {"status", "serverdate"};

	public String[] getVoicedCommandList()
	{
		return _commandList;
	}

	public boolean useVoicedCommand(String command, L2Player activeChar, String target)
	{
		if(command.equals("status"))
		{
			TextBuilder ret = TextBuilder.newInstance();
			if(Config.StatusOnline)
			{
				ret.append("<font color=\"LEVEL\">Онлайн сервера:</font> " + (Config.StatusOnlinePercent == 0 ? Stats.getOnline() : Stats.getOnline() + Stats.getOnline() * Config.StatusOnlinePercent / 100));
			}
			ret.append("<br1><font color=\"LEVEL\">Статус сервера:</font>");
			ret.append("<br1>Версия: ").append(Config.SERVER_VERSION.equalsIgnoreCase("${l2p.revision}") ? Config.SERVER_VERSION_UNSUPPORTED : Config.SERVER_VERSION);
			if(activeChar.getPlayerAccess().CanRestart)
			{
				ret.append("<br1>Memory usage: ").append(MemoryWatchDog.getMemUsedPerc());
			}
			int mtc = Shutdown.getInstance().getSeconds();
			if(mtc > 0)
			{
				ret.append("<br1>До рестарта: ");
				int numDays = mtc / 86400;
				mtc -= numDays * 86400;
				int numHours = mtc / 3600;
				mtc -= numHours * 3600;
				int numMins = mtc / 60;
				mtc -= numMins * 60;
				int numSeconds = mtc;
				if(numDays > 0)
				{
					ret.append(numDays + "d ");
				}
				if(numHours > 0)
				{
					ret.append(numHours + "h ");
				}
				if(numMins > 0)
				{
					ret.append(numMins + "m ");
				}
				if(numSeconds > 0)
				{
					ret.append(numSeconds + "s");
				}
			}
			else
			{
				ret.append("<br1>Restart task not launched.");
			}
			ret.append("<br><center><button value=\"");
			ret.append("Обновить");
			ret.append("\" action=\"bypass -h user_status\" width=100 height=15 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\" /></center>");
			show(ret.toString(), activeChar);
			TextBuilder.recycle(ret);
			return true;
		}
		else if(command.equals("serverdate"))
		{
			activeChar.sendMessage("Server date:" + Calendar.getInstance().get(Calendar.DAY_OF_MONTH) + "." + Calendar.getInstance().get(Calendar.MONTH) + "." + Calendar.getInstance().get(Calendar.YEAR));
			return true;
		}
		return false;
	}
}