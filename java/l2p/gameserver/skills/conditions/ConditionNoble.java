package l2p.gameserver.skills.conditions;

import l2p.gameserver.model.L2Player;
import l2p.gameserver.skills.Env;

/**
 * User: voron.dev
 * Date: 31.10.11
 * Time: 8:30
 */
public class ConditionNoble extends Condition
{
	private boolean noble = false;

	public ConditionNoble(String value)
	{
		noble = Boolean.parseBoolean(value);
	}

	@Override
	protected boolean testImpl(Env env)
	{
		if(!env.character.isPlayer())
		{
			return false;
		}
		return ((L2Player) env.character).isNoble() && noble;
	}
}