package l2p.gameserver.skills.effects;

import l2p.gameserver.instancemanager.DimensionalRiftManager;
import l2p.gameserver.model.L2Effect;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.model.L2Zone;
import l2p.gameserver.serverpackets.SystemMessage;
import l2p.gameserver.skills.EffectType;
import l2p.gameserver.skills.Env;

public class EffectClanGate extends L2Effect
{
	public EffectClanGate(Env env, EffectTemplate template)
	{
		super(env, template);
	}

	@Override
	public EffectType getEffectType()
	{
		return EffectType.ClanGate;
	}

	@Override
	public boolean checkCondition()
	{
		if(!getEffector().isPlayer())
			return false;

		L2Player player = getEffector().getPlayer();

		if(player.getReflection().getId() != 0)
			return false;
		if(player.isAlikeDead())
			return false;
		if(player.getClanId() == 0)
			return false;
		if(!player.isClanLeader())
			return false;
		if(player.isInOlympiadMode())
			return false;
		if(player.isInCombat())
			return false;
		if(player.isInZoneBattle() || player.isInZone(L2Zone.ZoneType.Siege))
			return false;
		if(DimensionalRiftManager.getInstance().checkIfInRiftZone(player.getLoc(), false))
			return false;

		return super.checkCondition();
	}
	
	@Override
	public void onStart()
	{
		L2Player player = getEffector().getPlayer();
		player.setParalyzed(true);
		player.getClan().broadcastToOtherOnlineMembers(new SystemMessage(SystemMessage.COURT_MAGICIAN__THE_PORTAL_HAS_BEEN_CREATED), player);
		super.onStart();
	}

	@Override
	public void onExit()
	{
		getEffected().setParalyzed(false);
		super.onExit();
	}

	@Override
	public boolean onActionTime()
	{
		return false;
	}
}
