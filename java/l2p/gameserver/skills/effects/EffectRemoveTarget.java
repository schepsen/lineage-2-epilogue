package l2p.gameserver.skills.effects;

import l2p.gameserver.ai.CtrlIntention;
import l2p.gameserver.model.L2Effect;
import l2p.gameserver.skills.Env;

public class EffectRemoveTarget extends L2Effect
{
	public EffectRemoveTarget(Env env, EffectTemplate template)
	{
		super(env, template);
	}
	
	@Override
	public void onStart()
	{
		super.onStart();
		getEffected().setTarget(null);
		getEffected().abortAttack(true, true);
		getEffected().abortCast(true);
		getEffected().getAI().setIntention(CtrlIntention.AI_INTENTION_IDLE, getEffector());
	}
	
	@Override
	public boolean onActionTime()
	{
		return false;
	}
}
