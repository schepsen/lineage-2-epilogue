package l2p.gameserver.scripts;

import l2p.common.ThreadPoolManager;
import l2p.gameserver.instancemanager.TowerOfNaiaManager;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.model.L2Spawn;
import l2p.gameserver.model.Reflection;
import l2p.gameserver.model.instances.L2DoorInstance;
import l2p.gameserver.model.instances.L2MonsterInstance;
import l2p.gameserver.model.quest.Quest;
import l2p.gameserver.modules.data.DoorTable;
import l2p.gameserver.modules.instance.mInstance;
import l2p.util.Location;

import java.util.concurrent.ScheduledFuture;

/**
 * @author hex1r0
 */
public class TowerOfNaia extends L2CoreScript {
    private static final int REFLECTION_ID = 519;
    private static final int BELETHS_SLAVE = 18490;
    private static final int BELETHS_FOLLOWER1 = 22393;
    private static final int BELETHS_DEVOTEE1 = 22394;
    private static final int BELETHS_PROJECTION = 22395;
    private static final int BELETHS_RETAINER = 22411;
    private static final int BELETHS_FIGHTER = 22412;
    private static final int BELETHS_LOW_SERVANT = 22413;
    //private static final int BELETHS_FOLLOWER2 		= 22414;
    //private static final int BELETHS_DEVOTEE2 		= 22415;
    private static final int BELETHS_ASSASSIN1 = 22439;
    private static final int BELETHS_ASSASSIN2 = 22440;
    private static final int BELETHS_MINION1 = 22441;
    private static final int BELETHS_MINION2 = 22442;
    private static final int[][] ROOM_MONSTERS =
            {
                    {BELETHS_FOLLOWER1, 1, 0, 0, 0, BELETHS_DEVOTEE1, 2, 0, 0, 0, 0, 0, 0, BELETHS_FOLLOWER1, 2, 0, 0, 0, 0, 0, 0, BELETHS_DEVOTEE1, 1, 0, 0, 0},
                    {BELETHS_ASSASSIN1, 8, -48146, 249597, -9124, -48144, 248711, -9124, -48704, 249597, -9104, -49219, 249596, -9104, -49715, 249601, -9104, -49714, 248696, -9104, -49225, 248710, -9104, -48705, 248708, -9104},
                    {BELETHS_MINION1, 2, 0, 0, 0, 0, 0, 0, BELETHS_MINION2, 2, 0, 0, 0, 0, 0, 0},
                    {BELETHS_ASSASSIN2, 8, -49754, 243866, -9968, -49754, 242940, -9968, -48733, 243858, -9968, -48745, 242936, -9968, -49264, 242946, -9968, -49268, 243869, -9968, -48186, 242934, -9968, -48185, 243855, -9968},
                    {BELETHS_RETAINER, 2, 0, 0, 0, 0, 0, 0, BELETHS_FOLLOWER1, 1, 0, 0, 0, BELETHS_DEVOTEE1, 1, 0, 0, 0, BELETHS_FOLLOWER1, 1, 0, 0, 0, BELETHS_DEVOTEE1, 1, 0, 0, 0},
                    {BELETHS_PROJECTION, 2, 0, 0, 0, 0, 0, 0},
                    {BELETHS_FOLLOWER1, 1, 0, 0, 0, BELETHS_DEVOTEE1, 2, 0, 0, 0, 0, 0, 0, BELETHS_FOLLOWER1, 2, 0, 0, 0, 0, 0, 0, BELETHS_DEVOTEE1, 1, 0, 0, 0, BELETHS_FIGHTER, 1, 0, 0, 0},
                    {BELETHS_PROJECTION, 2, 0, 0, 0, 0, 0, 0},
                    {BELETHS_MINION1, 2, 0, 0, 0, 0, 0, 0, BELETHS_MINION2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                    {BELETHS_PROJECTION, 2, 0, 0, 0, 0, 0, 0},
                    {BELETHS_LOW_SERVANT, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                    {BELETHS_SLAVE, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
            };
    private static final int BEFORE_SPAWN = 0;
    private static final int AFTER_SPAWN = 1;
    private static final int AFTER_KILL = 2;
    private static final int CLOSE = 0;
    private static final int OPEN = 1;
    private static final int[][] ROOM_DOOR_MECHANICS =
            {
                    {18250001, BEFORE_SPAWN, OPEN, 18250001, AFTER_SPAWN, CLOSE, 18250002, AFTER_KILL, OPEN, 18250003, AFTER_KILL, OPEN},
                    {18250003, AFTER_SPAWN, CLOSE, 18250004, AFTER_KILL, OPEN, 18250005, AFTER_KILL, OPEN},
                    {18250005, AFTER_SPAWN, CLOSE, 18250006, AFTER_KILL, OPEN, 18250007, AFTER_KILL, OPEN},
                    {18250007, AFTER_SPAWN, CLOSE, 18250008, AFTER_KILL, OPEN, 18250009, AFTER_KILL, OPEN},
                    {18250009, AFTER_SPAWN, CLOSE, 18250010, AFTER_KILL, OPEN, 18250011, AFTER_KILL, OPEN},
                    {18250011, AFTER_SPAWN, CLOSE, 18250101, AFTER_KILL, OPEN, 18250013, AFTER_KILL, OPEN},
                    {18250013, AFTER_SPAWN, CLOSE, 18250014, AFTER_KILL, OPEN, 18250015, AFTER_KILL, OPEN},
                    {18250015, AFTER_SPAWN, CLOSE, 18250102, AFTER_KILL, OPEN, 18250017, AFTER_KILL, OPEN},
                    {18250017, AFTER_SPAWN, CLOSE, 18250018, AFTER_KILL, OPEN, 18250019, AFTER_KILL, OPEN},
                    {18250019, AFTER_SPAWN, CLOSE, 18250103, AFTER_KILL, OPEN, 18250021, AFTER_KILL, OPEN},
                    {18250021, AFTER_SPAWN, CLOSE, 18250022, AFTER_KILL, OPEN, 18250023, AFTER_KILL, OPEN},
                    {18250023, AFTER_SPAWN, CLOSE, 18250024, AFTER_KILL, OPEN}
            };
    private static final int[][] SPORE_LOCATIONS =
            {
                    {-46152, 246776, -14207},
                    {-46392, 247176, -14208},
                    {-46392, 247688, -14209},
                    {-46152, 248120, -14208},
                    {-45736, 248360, -14208},
                    {-45208, 248360, -14208},
                    {-44824, 248136, -14208},
                    {-44568, 247688, -14209},
                    {-44552, 247208, -14208},
                    {-44792, 246776, -14208},
                    {-45224, 246536, -14208},
                    {-45752, 246536, -14208},
            };
    private ScheduledFuture<?> _deadMonsterCheckTask = null;

    public void enterNaia() {
        int roofLockHpPercent = 100;
        L2Player player = (L2Player) getSelf();
        L2MonsterInstance roofLock = TowerOfNaiaManager.getRoofLock();
        if (roofLock != null) {
            roofLockHpPercent = (int) roofLock.getCurrentHpPercents();
        }
        if (roofLockHpPercent <= 10) {
            if (mInstance.enterInstance(player, REFLECTION_ID, true, true)) {
                handleDoors(player.getReflection(), 0, BEFORE_SPAWN);
            }
        } else {
            getNpc().doDie(null);
        }
    }

    public void startRoomInvasion(String[] args) {
        int roomId = -1;
        try {
            roomId = Integer.parseInt(args[0]);
        } catch (Exception e) {
        }
        if (roomId < 0 || roomId > 11) {
            return;
        }
        getNpc().doDie(null);
        Reflection ref = getSelfPlayer().getReflection();
        ref.startCollapseTimer(300000);
        handleDoors(ref, roomId, AFTER_SPAWN);
        int i = 0;
        while (i < ROOM_MONSTERS[roomId].length) {
            int monsterId = ROOM_MONSTERS[roomId][i++];
            int count = ROOM_MONSTERS[roomId][i++];
            for (int j = 0; j < count; j++) {
                Location loc = new Location(ROOM_MONSTERS[roomId][i++], ROOM_MONSTERS[roomId][i++], ROOM_MONSTERS[roomId][i++]);
                Quest.addSpawnToInstance(monsterId, loc, 150, ref.getReflectionId());
            }
        }
        if (_deadMonsterCheckTask != null) {
            _deadMonsterCheckTask.cancel(true);
        }
        _deadMonsterCheckTask = ThreadPoolManager.getInstance().scheduleAiAtFixedRate(new DeadMonsterCheckTask(ref, roomId), 1000, 1000, false);
    }

    public void finishRoomInvasion(Reflection ref, int roomId) {
        if (_deadMonsterCheckTask != null) {
            _deadMonsterCheckTask.cancel(true);
        }
        ref.stopCollapseTimer();
        handleDoors(ref, roomId, AFTER_KILL);
        if (roomId == 11) {
            for (L2Player player : ref.getPlayers()) {
                player.setReflection(0);
            }
            DoorTable.getInstance().getDoor(18250024).openMe();
            DoorTable.getInstance().getDoor(18250025).openMe();
        }
    }

    private void handleDoors(Reflection ref, int roomId, int criterion) {
        for (int i = 0; i < ROOM_DOOR_MECHANICS[roomId].length; i += 3) {
            int doorId = ROOM_DOOR_MECHANICS[roomId][i];
            int order = ROOM_DOOR_MECHANICS[roomId][i + 1];
            int action = ROOM_DOOR_MECHANICS[roomId][i + 2];
            if (order == criterion) {
                for (L2DoorInstance door : ref.getDoors()) {
                    if (door.getDoorId() == doorId) {
                        if (action == OPEN) {
                            door.openMe();
                        } else {
                            door.closeMe();
                        }
                    }
                }
            }
        }
    }

    public void spawnSpores() {
        getNpc().doDie(null);
        for (int i = 0; i < SPORE_LOCATIONS.length; i++) {
            Location loc = new Location(SPORE_LOCATIONS[i][0], SPORE_LOCATIONS[i][1], SPORE_LOCATIONS[i][2]);
            int monsterId;
            if (i % 2 == 0 && i % 4 != 0) {
                monsterId = TowerOfNaiaManager.SPORE_EARTH_ID;
            } else if (i % 3 == 0) {
                monsterId = TowerOfNaiaManager.SPORE_FIRE_ID;
            } else if (i % 4 == 0) {
                monsterId = TowerOfNaiaManager.SPORE_WATER_ID;
            } else {
                monsterId = TowerOfNaiaManager.SPORE_WIND_ID;
            }
            L2Spawn spawn;
            try {
                spawn = new L2Spawn(monsterId);
                spawn.setLoc(loc);
                spawn.setLocation(0);
                spawn.setAmount(1);
                spawn.setRespawnDelay(30);
                spawn.init();
            } catch (ClassNotFoundException e) {
            }
        }
    }

    private class DeadMonsterCheckTask implements Runnable {
        private int _roomId;
        private Reflection _ref;

        public DeadMonsterCheckTask(Reflection ref, int roomId) {
            _ref = ref;
            _roomId = roomId;
        }

        @Override
        public void run() {
            boolean alive = false;
            for (L2MonsterInstance monster : _ref.getMonsters()) {
                if (monster != null && !monster.isDead()) {
                    alive = true;
                    break;
                }
            }
            if (!alive) {
                finishRoomInvasion(_ref, _roomId);
            }
        }
    }

    @Override
    public void onLoad() {
    }

    @Override
    public void onReload() {
    }

    @Override
    public void onShutdown() {
    }
}