package l2p.gameserver.modules;

import java.util.HashMap;

/**
 * User: Shaitan
 * Date: 23.01.11
 * Time: 7:58
 */
public class mDialog
{
	private static HashMap<Integer, String> npcDialog = new HashMap<Integer, String>();

	public static String getNpcDialog(int id)
	{
		return npcDialog.get(id);
	}

	public static void addNpcDialog(int id, String text)
	{
		npcDialog.put(id, text);
	}
}