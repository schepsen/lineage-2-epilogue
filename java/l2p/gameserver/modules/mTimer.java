package l2p.gameserver.modules;

/**
 * User: Shaitan
 * Date: 22.03.11
 * Time: 13:17
 */
public class mTimer
{
	private long startTime;
	private long endTime;
	private boolean end;

	public void start()
	{
		startTime = System.currentTimeMillis();
	}

	public void stop()
	{
		endTime = System.currentTimeMillis();
	}

	public int getSeconds()
	{
		return (int) (endTime - startTime) / 1000;
	}
}