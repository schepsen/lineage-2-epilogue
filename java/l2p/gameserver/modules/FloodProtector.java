package l2p.gameserver.modules;

import l2p.Config;
import l2p.gameserver.model.L2Player;

/**
 * @author hex1r0
 */
public final class FloodProtector
{
	public static enum Action
	{
		// TODO add more
		ATTACK(Config.ATTACK_PACKET_DELAY),
		DROP_ITEM(500),
		ENCHANT_ITEM(Config.ENCHANT_PACKET_DELAY),
		MOVE_TO(Config.MOVE_PACKET_DELAY),
		REQUEST_MULTISELL(500),
		DO_SOCIAL(2600),
		USE_ITEM(Config.USE_ITEM_DELAY),
		USE_MANA_POTION(Config.ManaPotionDelay),
		SUMMON_PET(6000),
		ROLL_DICE(4000);
		public static final int VALUES_LENGTH = Action.values().length;
		private final int _reuseDelay;

		private Action(int reuseDelay)
		{
			_reuseDelay = reuseDelay;
		}

		private int getReuseDelay()
		{
			return _reuseDelay;
		}
	}

	public static boolean tryPerformAction(L2Player player, Action action)
	{
		if(player.isGM())
		{
			return true;
		}
		long[] timers = player.getActionTimers();
		synchronized(timers)
		{
			if(timers[action.ordinal()] > System.currentTimeMillis())
			{
				return false;
			}
			timers[action.ordinal()] = System.currentTimeMillis() + action.getReuseDelay();
			return true;
		}
	}
}