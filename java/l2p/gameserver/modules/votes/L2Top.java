package l2p.gameserver.modules.votes;

import l2p.Config;
import l2p.common.ThreadPoolManager;
import l2p.database.DatabaseUtils;
import l2p.database.FiltredPreparedStatement;
import l2p.database.L2DatabaseFactory;
import l2p.database.ThreadConnection;
import l2p.gameserver.model.L2ObjectsStorage;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.modules.option.mOption;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.StringTokenizer;
import l2p.util.Rnd;

/**
 * User: Shaitan
 * Date: 09.02.11
 * Time: 16:16
 */
public class L2Top
{
	BufferedReader reader;
	private static L2Top ourInstance = new L2Top();

	public static L2Top getInstance()
	{
		return ourInstance;
	}

	private L2Top()
	{
		if(Config.L2TopEnable)
		{
			ThreadPoolManager.getInstance().scheduleGeneralAtFixedRate(new startParce(), 60000, Config.L2TopRefreshTime * 60 * 1000);
		}
	}

	private class startParce implements Runnable
	{
		@Override
		public void run()
		{
			try
			{
				getPage(Config.L2TopUrl);
				parse();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	private void getPage(String address)
	{
		try
		{
			URL url = new URL(address);
			reader = new BufferedReader(new InputStreamReader(url.openStream(), "windows-1251"));
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	private void parse()
	{
		String line;
		int i = 0;
		try
		{
			while((line = reader.readLine()) != null)
			{
				StringTokenizer st = new StringTokenizer(line, "- :\t");
				while(st.hasMoreTokens())
				{
					try
					{
						int year = Integer.parseInt(st.nextToken());
						int month = Integer.parseInt(st.nextToken());
						int day = Integer.parseInt(st.nextToken());
						int hour = Integer.parseInt(st.nextToken());
						int minute = Integer.parseInt(st.nextToken());
						int second = Integer.parseInt(st.nextToken());
						String name = st.nextToken();
						L2Player player = L2ObjectsStorage.getPlayer(name);
						if(player != null && player.isOnline())
						{
							Calendar calendar = Calendar.getInstance();
							calendar.set(Calendar.YEAR, year);
							calendar.set(Calendar.MONTH, month);
							calendar.set(Calendar.DAY_OF_MONTH, day);
							calendar.set(Calendar.HOUR_OF_DAY, hour);
							calendar.set(Calendar.MINUTE, minute);
							calendar.set(Calendar.SECOND, second);
							calendar.set(Calendar.MILLISECOND, 0);
							long time = calendar.getTimeInMillis() / 1000;
							checkAndSave(name, time);
						}
						i++;
					}
					catch(Exception e)
					{
					}
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	private void checkAndSave(String name, long time)
	{
		ThreadConnection tc = null;
		FiltredPreparedStatement fps = null;
		ResultSet rs = null;
		try
		{
			tc = L2DatabaseFactory.getInstance().getConnection();
			fps = tc.prepareStatement("SELECT * FROM `character_l2top_votes` WHERE `name`=? AND `time`=? LIMIT 1");
			fps.setString(1, name);
			fps.setLong(2, time);
			rs = fps.executeQuery();
			if(!rs.next())
			{
				L2Player player = L2ObjectsStorage.getPlayer(name);
				if(player != null && player.isOnline())
				{
					if(player.getLevel() >= Config.L2TopMinLvl)
					{
						int count = Config.L2TopRandomItemCount ? Rnd.get(1, Config.L2TopRewardItemCount) : Config.L2TopRewardItemCount;
						player.getInventory().addItem(Config.L2TopRewardItemId, count);
						player.sendMessage("Вам начислено " + count + " " + mOption.getItemName(Config.L2TopRewardItemId) + " за голосование в рейтинге L2Top.");
						fps = tc.prepareStatement("INSERT INTO `character_l2top_votes` (`name`, `time`) VALUES (?, ?)");
						fps.setString(1, name);
						fps.setLong(2, time);
						fps.execute();
					}
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DatabaseUtils.closeDatabaseCSR(tc, fps, rs);
		}
	}
}