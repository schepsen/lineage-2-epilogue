package l2p.gameserver.modules.instance;

import javolution.util.FastMap;
import l2p.extensions.scripts.Functions;
import l2p.gameserver.cache.Msg;
import l2p.gameserver.instancemanager.InstancedZoneManager;
import l2p.gameserver.model.L2Effect;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.model.Reflection;
import l2p.gameserver.model.instances.L2NpcInstance;
import l2p.gameserver.serverpackets.SystemMessage;

import java.util.ArrayList;

/**
 * User: Shaitan
 * Date: 19.01.11
 * Time: 13:21
 */
public class mInstance extends Functions
{
	public static boolean enterInstance(L2Player player, int id, boolean party, boolean time)
	{
		if(player == null)
		{
			return false;
		}
		if(party)
		{
			if(player.getParty() == null)
			{
				player.sendMessage("Вы не в группе.");
				return false;
			}
			else if(player.getParty().getPartyLeader() != player)
			{
				player.sendMessage("Вы не лидер группы.");
				return false;
			}
		}
		InstancedZoneManager izm = InstancedZoneManager.getInstance();
		FastMap<Integer, InstancedZoneManager.InstancedZone> izs = InstancedZoneManager.getInstance().getById(id);
		if(izs == null)
		{
			player.sendPacket(Msg.SYSTEM_ERROR);
			return false;
		}
		InstancedZoneManager.InstancedZone iz = izs.get(0);
		if(iz == null)
		{
			player.sendPacket(Msg.SYSTEM_ERROR);
			return false;
		}
		String name = iz.getName();
		if(izm.getTimeToNextEnterInstance(name, player) > 0)
		{
			player.sendPacket(new SystemMessage(SystemMessage.C1_MAY_NOT_RE_ENTER_YET).addName(player));
			return false;
		}
		int timelimit = iz.getTimelimit();
		if(party)
		{
			int minMembers = iz.getMinParty();
			int maxMembers = iz.getMaxParty();
			if(player.getParty().getMemberCount() < minMembers)
			{
				player.sendMessage("В группе должно быть не меньше " + minMembers + " человек.");
				return false;
			}
			if(player.getParty().getMemberCount() > maxMembers)
			{
				player.sendMessage("В группе должно быть не больше " + maxMembers + " человек.");
				return false;
			}
			for(L2Player member : player.getParty().getPartyMembers())
			{
				if(member.isCursedWeaponEquipped() || member.isInFlyingTransform() || member.isDead())
				{
					player.sendPacket(new SystemMessage(SystemMessage.C1S_QUEST_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(member));
					return false;
				}
				if(!player.isInRange(member, 500))
				{
					member.sendPacket(Msg.ITS_TOO_FAR_FROM_THE_NPC_TO_WORK);
					player.sendPacket(Msg.ITS_TOO_FAR_FROM_THE_NPC_TO_WORK);
					return false;
				}
				if(izm.getTimeToNextEnterInstance(name, member) > 0)
				{
					player.getParty().broadcastToPartyMembers(new SystemMessage(SystemMessage.C1_MAY_NOT_RE_ENTER_YET).addName(member));
					return false;
				}
			}
		}
		Reflection r = new Reflection(name);
		r.setInstancedZoneId(id);
		for(InstancedZoneManager.InstancedZone i : izs.values())
		{
			if(r.getTeleportLoc() == null)
			{
				r.setTeleportLoc(i.getTeleportCoords());
			}
			r.FillSpawns(i.getSpawnsInfo());
			r.FillDoors(i.getDoors());
		}
		r.setCoreLoc(r.getReturnLoc());
		r.setReturnLoc(player.getLoc());
		boolean dispellBuffs = iz.isDispellBuffs();
		if(party)
		{
			for(L2Player member : player.getParty().getPartyMembers())
			{
				if(time)
				{
					member.setVar(name, String.valueOf(System.currentTimeMillis()));
				}
				member.setVar("backCoords", r.getReturnLoc().toXYZString());
				member.teleToLocation(iz.getTeleportCoords(), r.getId());
				if(dispellBuffs)
				{
					for(L2Effect e : member.getEffectList().getAllEffects())
					{
						if(!e.getSkill().isOffensive() && !e.getSkill().getName().startsWith("Adventurer's "))
						{
							e.exit();
						}
					}
					if(member.getPet() != null)
					{
						for(L2Effect e : member.getPet().getEffectList().getAllEffects())
						{
							if(!e.getSkill().isOffensive() && !e.getSkill().getName().startsWith("Adventurer's "))
							{
								e.exit();
							}
						}
					}
				}
			}
			player.getParty().setReflection(r);
		}
		else
		{
			if(time)
			{
				player.setVar(name, String.valueOf(System.currentTimeMillis()));
			}
			player.setVar("backCoords", r.getReturnLoc().toXYZString());
			player.teleToLocation(iz.getTeleportCoords(), r.getId());
			for(L2Effect e : player.getEffectList().getAllEffects())
			{
				if(!e.getSkill().isOffensive() && !e.getSkill().getName().startsWith("Adventurer's "))
				{
					e.exit();
				}
			}
			if(player.getPet() != null)
			{
				for(L2Effect e : player.getPet().getEffectList().getAllEffects())
				{
					if(!e.getSkill().isOffensive() && !e.getSkill().getName().startsWith("Adventurer's "))
					{
						e.exit();
					}
				}
			}
			player.setReflection(r);
		}
		if(timelimit > 0)
		{
			r.startCollapseTimer(timelimit * 60 * 1000L);
		}
		return true;
	}

	private ArrayList<L2NpcInstance> monsters = new ArrayList<L2NpcInstance>();

	public void addMonster(L2NpcInstance npc)
	{
		monsters.add(npc);
	}

	public void clearMonster()
	{
		monsters.clear();
	}

	public ArrayList<L2NpcInstance> getMonsters()
	{
		return monsters;
	}

	private String name = "";

	public void setName(String name)
	{
		this.name = name;
	}

	public String getName()
	{
		return name;
	}
}