package l2p.gameserver.modules.instance.SoI;

import java.util.HashMap;

import l2p.gameserver.instancemanager.ServerVariables;
import l2p.util.Location;

/**
 * User: Shaitan
 * Date: 24.03.11
 * Time: 11:32
 */
public class mSoI
{
	public static HashMap<Long, BreakThroughTheHallOfSuffering> breakThroughTheHallOfSuffering = new HashMap<Long, BreakThroughTheHallOfSuffering>();
	public static HashMap<Long, DefendTheHallofSuffering> defendTheHallofSuffering = new HashMap<Long, DefendTheHallofSuffering>();
	private static int soiStage = ServerVariables.getInt("soiStage", 1);

	public static void setStage(int stage)
	{
		soiStage = stage;
		ServerVariables.set("soiStage", stage);
	}

	public static int getStage()
	{
		return soiStage;
	}

	public static int npcTepios = 32603;
	public static int npcTepios2 = 32530;
	public static int npcMouthOfEkimus = 32537;
	public static int idMonsterTumorOfDeath = 18704;
	public static int idMonsterYehanKlodekus = 25665;
	public static int idMonsterYehanKlanikus = 25666;
	public static Location Center = new Location(-173704, 218088, -9562);
	public static Location KlodekusLoc = new Location(-173752, 217880, -9582, 31046);
	public static Location KlanikusLoc = new Location(-173640, 218312, -9584, 35323);
}