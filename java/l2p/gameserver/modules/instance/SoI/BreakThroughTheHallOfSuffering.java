package l2p.gameserver.modules.instance.SoI;

import java.util.concurrent.ScheduledFuture;

import l2p.gameserver.model.instances.L2NpcInstance;
import l2p.gameserver.modules.mTimer;
import l2p.gameserver.modules.instance.mInstance;

/**
 * User: Shaitan
 * Date: 24.03.11
 * Time: 11:35
 */
public class BreakThroughTheHallOfSuffering
{
	public L2NpcInstance monsterTumorOfDeath;
	public L2NpcInstance monsterYehanKlodekus;
	public L2NpcInstance monsterYehanKlanikus;
	//
	public ScheduledFuture<?> RessTask;
	//
	public mInstance instance = new mInstance();
	//
	public int countMob = 0;
	public int rewardType = 10;
	//
	public mTimer timer = new mTimer();
}