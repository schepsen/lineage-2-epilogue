package l2p.gameserver.modules.instance.SoI;

import java.util.concurrent.ScheduledFuture;

import l2p.gameserver.model.instances.L2NpcInstance;
import l2p.gameserver.modules.mTimer;
import l2p.gameserver.modules.instance.mInstance;

/**
 * User: Shaitan
 * Date: 26.03.11
 * Time: 13:05
 */
public class DefendTheHallofSuffering
{
	public L2NpcInstance monsterTumorOfDeath;
	public L2NpcInstance monsterYehanKlodekus;
	public L2NpcInstance monsterYehanKlanikus;
	//
	public ScheduledFuture<?> RessTask;
	//
	public mInstance instance = new mInstance();
	//
	public int countMob = 0;
	public int rewardType = 10;
	public int waveId = 0;
	//
	public mTimer timer = new mTimer();
}