package l2p.gameserver.modules.event;

/**
 * User: Shaitan
 * Date: 15.02.11
 * Time: 9:35
 */
public interface mIEvent
{
	public void startEvent();

	public String getStatus();

	public String getName();

	public boolean isStat();

	public void getDescription();

	public void getStat();
}