package l2p.gameserver.modules.event;

/**
 * User: Shaitan
 * Date: 02.04.11
 * Time: 9:51
 */
public class mEventPvP
{
	private Status status = mEventPvP.Status.noactive;

	private enum Status
	{
		noactive,
		reg,
		active
	}

	public boolean isNoActive()
	{
		return status == Status.noactive;
	}

	public boolean isReg()
	{
		return status == Status.reg;
	}

	public boolean isActive()
	{
		return status == Status.active;
	}

	public void setNoActive()
	{
		status = Status.noactive;
	}

	public void setReg()
	{
		status = Status.reg;
	}

	public void setActive()
	{
		status = Status.active;
	}
}