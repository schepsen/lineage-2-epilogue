package l2p.gameserver.modules.event;

import l2p.Config;
import l2p.common.ThreadPoolManager;
import l2p.gameserver.model.L2Effect;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.model.entity.olympiad.Olympiad;
import l2p.gameserver.model.instances.L2DoorInstance;
import l2p.gameserver.modules.data.DoorTable;
import l2p.util.GArray;
import l2p.util.Location;

/**
 * User: Shaitan
 * Date: 15.02.11
 * Time: 9:38
 */
public class mEvent
{
	public static GArray<L2DoorInstance> doors = new GArray<L2DoorInstance>();
	private static int[] doorIds =
	{
		24190001, 24190002, 24190003, 24190004
	};
	private static mEvent ourInstance = new mEvent();

	public static mEvent getInstance()
	{
		return ourInstance;
	}

	private mEvent()
	{
		for(int doorId : doorIds)
		{
			L2DoorInstance door = DoorTable.getInstance().getDoor(doorId).clone();
			door.setOpen(false);
			door.setGeodata(false);
			door.setHPVisible(false);
			door.setIsInvul(true);
			doors.add(door);
		}
		if(Config.eventEngine)
		{
			ThreadPoolManager.getInstance().scheduleGeneralAtFixedRate(new runEvents(), 1000, Config.runEveryNMinutes * 60 * 1000);
		}
	}
	private static int number = 0;

	private static class runEvents implements Runnable
	{
		@Override
		public void run()
		{
			mEventList.getEvent(number).startEvent();
			number++;
			if(number == mEventList.getSize())
			{
				number = 0;
			}
		}
	}

	public static boolean checkPlayer(L2Player player)
	{
		if(player == null)
		{
			return false;
		}
		else if(player.isEvent())
		{
			player.sendMessage("Нельзя участвовать сразу в нескольких эвентах.");
			return false;
		}
		else if(player.isCursedWeaponEquipped())
		{
			player.sendMessage("Нельзя участвовать в эвенте с проклятым оружием.");
			return false;
		}
		else if(player.getOlympiadGameId() > 0 || player.isInZoneOlympiad() || Olympiad.isRegistered(player))
		{
			player.sendMessage("Нельзя участвовать в эвенте если вы на олимпиаде.");
			return false;
		}
		else if(player.getKarma() > 0)
		{
			player.sendMessage("Нельзя участвовать в эвенте если есть карма.");
			return false;
		}
		else if(player.isInParty() && player.getParty().isInDimensionalRift())
		{
			player.sendMessage("Закончите все свои дела в Dimensional Rift.");
			return false;
		}
		else if(player.isTeleporting())
		{
			player.sendMessage("Во время телепортации регистрироваться нельзя.");
			return false;
		}
		else if(player.getDuel() != null)
		{
			player.sendMessage("Нельзя участвовать в эвенте во время дуэли.");
			return false;
		}
		return true;
	}

	public static void paralyzePlayer(L2Player player)
	{
		player.setParalyzed(true);
		if(player.getPet() != null)
		{
			player.getPet().setParalyzed(true);
		}
	}

	public static void unParalyzePlayer(L2Player player)
	{
		player.setParalyzed(false);
		if(player.getPet() != null)
		{
			player.getPet().setParalyzed(false);
		}
	}

	public static void resurrectAndHeal(L2Player player)
	{
		if(player.isDead())
		{
			player.doRevive();
		}
		player.setCurrentHpMp(player.getMaxHp(), player.getMaxMp());
		player.setCurrentCp(player.getMaxCp());
	}

	public static void teleportToLocation(L2Player player, Location loc, boolean save, long ref)
	{
		if(save)
		{
			player.setVar("backCoords", player.getX() + " " + player.getY() + " " + player.getZ());
		}
		player.teleToLocation(loc, ref);
	}

	public static void unTeleportToLocation(L2Player player)
	{
		String var = player.getVar("backCoords");
		if(var == null || var.equals(""))
		{
			return;
		}
		String[] coords = var.split(" ");
		if(coords.length != 3)
		{
			return;
		}
		player.setReflection(0);
		player.teleToLocation(Integer.parseInt(coords[0]), Integer.parseInt(coords[1]), Integer.parseInt(coords[2]), 0);
		player.unsetVar("backCoords");
	}

	public static void deleteAllBuff(L2Player player)
	{
		for(L2Effect e : player.getEffectList().getAllEffects())
		{
			e.exit();
		}
		if(player.getPet() != null)
		{
			for(L2Effect e : player.getPet().getEffectList().getAllEffects())
			{
				e.exit();
			}
		}
	}
}