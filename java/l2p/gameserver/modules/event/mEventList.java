package l2p.gameserver.modules.event;

import java.util.ArrayList;

/**
 * User: Shaitan
 * Date: 15.02.11
 * Time: 10:11
 */
public class mEventList
{
	private static ArrayList<mIEvent> events = new ArrayList<mIEvent>();

	public static void addEvent(mIEvent event)
	{
		if(!contains(event.getName()))
		{
			events.add(event);
		}
	}

	public static ArrayList<mIEvent> getEvents()
	{
		return events;
	}

	public static mIEvent getEvent(int number)
	{
		return events.get(number);
	}

	public static int getSize()
	{
		return events.size();
	}

	public static boolean contains(String name)
	{
		for(mIEvent event : events)
		{
			if(event.getName().equalsIgnoreCase(name))
			{
				return true;
			}
		}
		return false;
	}
}