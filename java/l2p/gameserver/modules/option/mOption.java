package l2p.gameserver.modules.option;

import l2p.Server;
import l2p.database.DatabaseUtils;
import l2p.database.FiltredPreparedStatement;
import l2p.database.L2DatabaseFactory;
import l2p.database.ThreadConnection;
import l2p.gameserver.loginservercon.LSConnection;
import l2p.gameserver.loginservercon.gspackets.ChangeAccessLevel;
import l2p.gameserver.model.L2ObjectsStorage;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.serverpackets.SystemMessage;
import l2p.gameserver.tables.ItemTable;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * User: Shaitan
 * Date: 02.11.2010
 * Time: 10:47:43
 */
public class mOption
{
	private static final Logger _log = Logger.getLogger(mOption.class.getName());
	public static Properties loadFile(String fileOption)
	{
		Properties properties = new Properties();
		try
		{
			InputStream inputStream = new FileInputStream(new File(fileOption));
			properties.load(inputStream);
		}
		catch(Exception e)
		{
			Server.exit(0, "File " + fileOption + " not found");
		}
		return properties;
	}

	public static int getInt(Properties Option, String s)
	{
		return Integer.parseInt(Option.getProperty(s.trim()));
	}

	public static boolean getBoolean(Properties Option, String s)
	{
		return Boolean.parseBoolean(Option.getProperty(s.trim()));
	}

	public static String getString(Properties Option, String s)
	{
		return Option.getProperty(s.trim());
	}

	public static boolean cash(L2Player player, int cash, int price)
	{
		if(cash == 0)
		{
			player.sendMessage("Цена: " + price + " " + "рублей.");
			player.sendMessage("У вас вообще нету денег.");
			return false;
		}
		else if(cash < price)
		{
			player.sendMessage("Цена: " + price + " " + "рублей.");
			player.sendMessage("У вас нету " + price + " рублей.");
			return false;
		}
		else
		{
			ThreadConnection con = null;
			FiltredPreparedStatement statement = null;
			try
			{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE character_cash set cash=? where char_obj_id=? and char_name=?");
			statement.setInt(1, cash - price);
			statement.setInt(2, player.getObjectId());
			statement.setString(3, player.getName());
			statement.executeUpdate();
			statement.close();
			}
			catch(final Exception e)
			{
				_log.warning("mOption: error in price() " + e);
			}
			finally
			{
				DatabaseUtils.closeDatabaseCS(con, statement);
			}
			player.sendMessage("Вы потратили: " + price + " рублей.");
			return true;
		}
	}

	public static boolean price(L2Player player, int itemid, int count)
	{
		if(player.getInventory().getItemByItemId(itemid) == null)
		{
			player.sendMessage("Цена: " + count + " " + getItemName(itemid));
			player.sendMessage("У вас вообще нету " + getItemName(itemid));
			return false;
		}
		else if(getItemCount(player, itemid) < count)
		{
			player.sendMessage("Цена: " + count + " " + getItemName(itemid));
			player.sendMessage("У вас нету " + count + " " + getItemName(itemid));
			return false;
		}
		else
		{
			player.getInventory().destroyItemByItemId(itemid, count, true);
			player.sendPacket(SystemMessage.removeItems(itemid, count));
			return true;
		}
	}

	public static void addItem(L2Player player, int id, int count)
	{
		player.getInventory().addItem(id, count);
		player.sendMessage("Вы получили: " + count + " " + getItemName(id));
	}

	public static String getItemName(int ItemId)
	{
		return ItemTable.getInstance().getTemplate(ItemId).getName();
	}

	public static long getItemCount(L2Player player, int ItemId)
	{
		return player.getInventory().getItemByItemId(ItemId).getCount();
	}

	public static void banAcc(L2Player player, String reason)
	{
		LSConnection.getInstance().sendPacket(new ChangeAccessLevel(player.getAccountName(), -100, reason, -1));
		L2Player tokick = null;
		for(L2Player p : L2ObjectsStorage.getAllPlayersForIterate())
		{
			if(p.getAccountName().equalsIgnoreCase(player.getAccountName()))
			{
				tokick = p;
				break;
			}
		}
		if(tokick != null)
		{
			tokick.logout(false, false, true, true);
		}
	}
}