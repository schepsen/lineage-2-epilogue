package l2p.gameserver.model;

/**
 * Fake PCs <BR>
 * 
 * @author Fonduee aka FewG
 */

public class FakePc {
    /**
     * Server side npc id, all npc's with this id will apear as PC
     */
    public String name;
    /**
     * Color of name in RGB format
     */
    public int nameColor;
    /**
     * Title of the NPC (HP percent as title only)
     */
    public String title;
    /**
     * Color of title in RGB format
     */
    public int titleColor;
    /**
     * 0 = Human, 1 = Elf, 2 = Darf Elf, 3 = Orc, 4 = Dwarf, 5 = Kamael
     */
    public int race;
    /**
     * 0 = Male, 1 = Female
     */
    public int sex;
    /**
     * Class id
     */
    public int clazz;
    /**
     * 0 bis n
     */
    public int hairStyle;
    /**
     * 0 bis n
     */
    public int hairColor;
    /**
     * 0 bis n
     */
    public int face;
    /**
     * Equipment item id
     */
    public int pdUnder;
    public int pdHead;
    public int pdRHand;
    public int pdLHand;
    public int pdGloves;
    public int pdChest;
    public int pdLegs;
    public int pdFeet;
    public int pdCloak;
    public int pdLRHand;

    public int pdHair;
    public int pdHair2;
    public int pdBelt;
    public int pdRBracelet;
    public int pdLBracelet;

    public int pdDeco1;
    public int pdDeco2;
    public int pdDeco3;
    public int pdDeco4;
    public int pdDeco5;
    public int pdDeco6;
    /**
     * Equipment augmentation
     */
    public int pdUnderAug;
    public int pdHeadAug;
    public int pdRHandAug;
    public int pdLHandAug;
    public int pdGlovesAug;
    public int pdChestAug;
    public int pdLegsAug;
    public int pdFeetAug;
    public int pdCloakAug;
    public int pdLRHandAug;

    public int pdHairAug;
    public int pdHair2Aug;
    public int pdBeltAug;
    public int pdRBraceletAug;
    public int pdLBraceletAug;

    public int pdDeco1Aug;
    public int pdDeco2Aug;
    public int pdDeco3Aug;
    public int pdDeco4Aug;
    public int pdDeco5Aug;
    public int pdDeco6Aug;
    /**
     * 0 = No PvP Flag, 1 = PvP Flag
     */
    public boolean pvpFlag;
    /**
     * 0 = No Karma, >0 = Karma
     */
    public int karma;
    /**
     * 0 = Visible, 1 = Invisible
     */
    public boolean invisible;
    /**
     * 0 = No mount, 1 = Strider, 2 = Wyvern, 3 = Great Wolf
     */
    public byte mount;
    /**
     * 0 = No Circle, 1 = Blue Circle, 2 = Red Circle
     */
    public byte team;
    /**
     * Hero Status
     */
    public boolean hero;
    /**
     * Fishing Status
     */
    public boolean fishing;
    /**
     * Fishing location (x, y, z)
     */
    public int fishingX;
    public int fishingY;
    public int fishingZ;
}