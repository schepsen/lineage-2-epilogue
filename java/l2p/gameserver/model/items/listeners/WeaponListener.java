package l2p.gameserver.model.items.listeners;

import l2p.Config;
import l2p.gameserver.model.L2Effect;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.model.items.Inventory;
import l2p.gameserver.model.items.L2ItemInstance;
import l2p.gameserver.templates.L2Weapon;

/**
 * User: Shaitan
 * Date: 16.02.11
 * Time: 15:39
 */
public class WeaponListener implements PaperdollListener
{
	private Inventory _inv;

	public WeaponListener(Inventory inv)
	{
		_inv = inv;
	}

	public void notifyUnequipped(int slot, L2ItemInstance item)
	{
		if(_inv.getOwner() == null || !_inv.getOwner().isPlayer() || !item.isEquipable() || !Config.DeleteBuffOnWeapon)
		{
			return;
		}
		L2Player owner = (L2Player) _inv.getOwner();
		if(item.getItem() instanceof L2Weapon)
		{
			for(L2Effect effect : owner.getEffectList().getAllEffects())
			{
				if(effect.getSkill().isMusic())
				{
					continue;
				}
				else if(effect.getSkill().getWeaponsAllowed() == ((L2Weapon) item.getItem()).getItemType().mask())
				{
					effect.exit();
				}
			}
		}
	}

	public void notifyEquipped(int slot, L2ItemInstance item)
	{
		if(_inv.getOwner() == null || !_inv.getOwner().isPlayer() || !item.isEquipable())
		{
			return;
		}
	}
}