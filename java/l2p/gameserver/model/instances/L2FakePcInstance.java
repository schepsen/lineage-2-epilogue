package l2p.gameserver.model.instances;

import l2p.Config;
import l2p.gameserver.model.instances.L2MonsterInstance;
import l2p.gameserver.serverpackets.NickNameChanged;
import l2p.gameserver.templates.L2NpcTemplate;

/**
 * @author Fonduee aka FewG
 * 
 */

public class L2FakePcInstance extends L2MonsterInstance
{
    public L2FakePcInstance(int objectId, L2NpcTemplate template)
    {
        super(objectId, template);
    }

    @Override
    public void checkHpMessages(double currentHp, double newHp)
    {
        if (Config.SHOW_HP_IN_TITLE)
        {
            if (Config.SHOW_HP_AS_PERCENTAGE)
            {
                this.setTitle("HP " + (int) this.getCurrentHpPercents());
            } else
            {
                this.setTitle("HP " + (int) newHp);
            }
            this.broadcastPacket(new NickNameChanged(this));
        }
        super.checkHpMessages(currentHp, newHp);
    }

}