package l2p.gameserver.serverpackets;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import l2p.extensions.scripts.ScriptManager;
import l2p.extensions.scripts.ScriptManager.ScriptClassAndMethod;
import l2p.gameserver.model.L2ObjectsStorage;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.model.entity.SevenSignsFestival.SevenSignsFestival;
import l2p.gameserver.model.instances.L2NpcInstance;
import l2p.util.Files;
import l2p.util.GArray;
import l2p.util.Strings;
import l2p.util.Util;

public class NpcHtmlMessage extends L2GameServerPacket
{
	private int _npcObjId, _questId;
	private String _html;
	private String _file = null;
	private GArray<String> _replaces = new GArray<String>();
	private int item_id = 0;
	private boolean have_appends = false;
	private final StackTraceElement[] ceatedFrom; //TODO убрать отладку

	public NpcHtmlMessage(L2Player player, L2NpcInstance npc, String filename, int val)
	{
		_npcObjId = npc.getObjectId();
		player.setLastNpc(npc);
		GArray<ScriptClassAndMethod> appends = ScriptManager.dialogAppends.get(npc.getNpcId());
		if(appends != null && appends.size() > 0)
		{
			have_appends = true;
			if(filename != null && filename.equalsIgnoreCase("data/html/npcdefault.htm"))
			{
				setHtml(""); // контент задается скриптами через DialogAppend_
			}
			else
			{
				setFile(filename);
			}
			String replaces = "";
			// Добавить в конец странички текст, определенный в скриптах.
			Object[] script_args = new Object[] {new Integer(val)};
			for(ScriptClassAndMethod append : appends)
			{
				Object obj = player.callScripts(append.scriptClass, append.method, script_args);
				if(obj != null)
				{
					replaces += obj;
				}
			}
			if(!replaces.equals(""))
			{
				replace("</body>", "\n" + Strings.bbParse(replaces) + "</body>");
			}
		}
		else
		{
			setFile(filename);
		}
		replace("%npcId%", String.valueOf(npc.getNpcId()));
		replace("%npcname%", npc.getName());
		replace("%festivalMins%", SevenSignsFestival.getInstance().getTimeToNextFestivalStr());
		ceatedFrom = Thread.currentThread().getStackTrace();
	}

	public NpcHtmlMessage(L2Player player, L2NpcInstance npc)
	{
		if(npc == null)
		{
			_npcObjId = 5;
			player.setLastNpc(null);
		}
		else
		{
			_npcObjId = npc.getObjectId();
			player.setLastNpc(npc);
		}
		ceatedFrom = Thread.currentThread().getStackTrace();
	}

	public NpcHtmlMessage(int npcObjId)
	{
		_npcObjId = npcObjId;
		// TODO player.setLastNpc(null);
		ceatedFrom = Thread.currentThread().getStackTrace();
	}

	public final NpcHtmlMessage setHtml(String text)
	{
		if(!text.contains("<html>"))
		{
			text = "<html><body>" + text + "</body></html>";
		} //<title>Message:</title> <br><br><br>
		_html = text;
		return this;
	}

	public final NpcHtmlMessage setFile(String file)
	{
		_file = file;
		return this;
	}

	/**
	 * WTF is this? Never used.
	 */
	public final NpcHtmlMessage setItemId(int _item_id)
	{
		item_id = _item_id;
		return this;
	}

	public void setQuest(int quest)
	{
		_questId = quest;
	}

	protected String html_load(String name, String lang)
	{
		String content = Files.read(name, lang);
		if(content == null)
		{
			content = "Can't find file'" + name + "'";
		}
		return content;
	}

	public NpcHtmlMessage replace(String pattern, String value)
	{
		_replaces.add(pattern);
		_replaces.add(value);
		return this;
	}

	private static final Pattern objectId = Pattern.compile("%objectId%");
	private static final Pattern playername = Pattern.compile("%playername%");

	@Override
	protected final void writeImpl()
	{
		L2Player player = getClient().getActiveChar();
		if(player == null)
		{
			return;
		}
		if(_file != null) //TODO может быть не очень хорошо сдесь это делать...
		{
			String content = Files.read(_file, player);
			if(content == null)
			{
				setHtml(have_appends && _file.endsWith(".htm") ? "" : _file);
			}
			else
			{
				setHtml(content);
			}
		}
		for(int i = 0; i < _replaces.size(); i += 2)
		{
			_html = _html.replaceAll(_replaces.get(i), _replaces.get(i + 1));
		}
		if(objectId == null)
		{
			System.out.println("objectId == null");
			Thread.dumpStack();
		}
		if(_html == null)
		{
			L2NpcInstance npc = L2ObjectsStorage.getNpc(_npcObjId);
			System.out.println("[WARNING] NpcHtmlMessage, _html == null, npc: " + npc.toString());
			for(StackTraceElement e : ceatedFrom)
			{
				System.out.println("\t" + e);
			}
			System.out.println(Util.dumpObject(this, true, false, true));
			return;
		}
		Matcher m = objectId.matcher(_html);
		if(m != null)
		{
			_html = m.replaceAll(String.valueOf(_npcObjId));
		}
		_html = playername.matcher(_html).replaceAll(player.getName());
		player.cleanBypasses(false);
		if(_questId > 0)
		{
			writeC(EXTENDED_PACKET);
			writeH(0x8d);
			writeD(_npcObjId);
			writeS(_html);
			writeD(_questId);
		}
		else
		{
			writeC(0x19);
			writeD(_npcObjId);
			writeS(_html);
			writeD(item_id);
		}
	}
}