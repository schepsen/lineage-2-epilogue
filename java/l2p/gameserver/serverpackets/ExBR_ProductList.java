package l2p.gameserver.serverpackets;

import java.util.Collection;

import l2p.gameserver.modules.CashShop;

public class ExBR_ProductList extends L2GameServerPacket
{
	public static final int DEFAULT_START_SALE_HOUR = 0;
	public static final int DEFAULT_START_SALE_MINUTE = 0;
	public static final int DEFAULT_END_SALE_HOUR = 23;
	public static final int DEFAULT_END_SALE_MINUTE = 59;
	public Collection<CashShop.CashShopItem> collection;

	@Override
	protected void writeImpl()
	{
		writeC(0xfe);
		writeH(0xb9);
		writeD(collection.size());
		for(CashShop.CashShopItem cs : collection)
		{
			writeD(cs.template.brId);
			writeH(cs.template.category);
			writeD(cs.price);
			// Todo конфиг
			int cat = 0;
			if(cs.iSale >= 2)
			{
				switch(cs.iCategory2)
				{
					case 0:
					case 2:
						cat = 2;
						break;
					case 1:
						cat = 3;
						break;
				}
			}
			writeD(cat);
			writeD(315561600); //writeD(cs.iStartSale);
			writeD(2127456000);
			writeC(127); //writeC(0);
			writeC(0); //writeC(cs.iStartHour);
			writeC(0); //writeC(cs.iStartMin);
			writeC(23); //writeC(cs.iEndHour);
			writeC(59); //writeC(cs.iEndMin);
			writeD(0); //writeD(cs.iStock);
			writeD(-1); //writeD(cs.iMaxStock);
		}
	}
}