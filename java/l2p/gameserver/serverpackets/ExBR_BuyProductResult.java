package l2p.gameserver.serverpackets;

/**
 * User: Shaitan
 * Date: 13.01.11
 * Time: 7:48
 */
public class ExBR_BuyProductResult extends L2GameServerPacket
{
	private int _code;

	public ExBR_BuyProductResult(int code)
	{
		_code = code;
	}

	@Override
	protected void writeImpl()
	{
		writeC(0xfe);
		writeH(0xbb);
		writeD(_code);
	}
}