package l2p.gameserver.serverpackets;

import l2p.gameserver.modules.CashShop;

public class ExBR_ProductInfo extends L2GameServerPacket
{
	private CashShop.CashShopItem _item;

	public ExBR_ProductInfo(CashShop.CashShopItem item)
	{
		_item = item;
	}

	@Override
	protected void writeImpl()
	{
		writeC(0xfe);
		writeH(0xba);
		writeD(_item.template.brId);
		writeD(_item.price);
		writeD(1);
		writeD(_item.template.itemId);
		writeD(_item.count);
		writeD(_item.iWeight);
		writeD(_item.iDropable ? 1 : 0);
	}
}