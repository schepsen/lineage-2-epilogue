package l2p.gameserver.serverpackets;

public class ExBR_GamePoint extends L2GameServerPacket
{
	private int _objectId;
	private int _points;

	public ExBR_GamePoint(int objId, int score)
	{
		_objectId = objId;
		_points = score;
	}

	@Override
	protected void writeImpl()
	{
		writeC(0xfe);
		writeH(0xb8);
		writeD(_objectId);
		writeD(_points);
		writeD(0x00);
	}
}