package l2p.gameserver.clientpackets;

import l2p.Config;
import l2p.gameserver.model.L2Player;
import l2p.util.Util;

/**
 * @author hex1r0
 */
public class RequestChangeNicknameColor extends L2GameClientPacket
{
	private int				_color;
	private String			_title;
	
	private static int[]	_titleColors	= new int[]
	{
		0x8e7ae4,
		0x7d41ea,
		0xacfef9,
		0xcfaac4,
		0xca5271,
		0x7effb5,
		0x8ea800,
		0x7e8a96,
		0x1e3063,
		0x939596
	};
	
	@Override
	protected void readImpl()
	{
		_color = readD();
		_title = readS();
		readD();
	}
	
	@Override
	protected void runImpl()
	{
		L2Player activeChar = getClient().getActiveChar();
		if (activeChar != null)
		{
			if(!_title.equals("") && !Util.isMatchingRegexp(_title, Config.CLAN_TITLE_TEMPLATE))
			{
				activeChar.sendMessage("Incorrect title.");
				return;
			}
			else
			{
				if (_color >= 0 && _color <= _titleColors.length)
				{
					activeChar.setTitleColor(_titleColors[_color]);
					activeChar.setTitle(_title);
					activeChar.broadcastUserInfo(true);
				}
			}
		}
	}
}