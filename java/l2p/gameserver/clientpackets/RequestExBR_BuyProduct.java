package l2p.gameserver.clientpackets;

import l2p.Config;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.modules.CashShop;

public class RequestExBR_BuyProduct extends L2GameClientPacket
{
	private int iProductID, iAmount;

	@Override
	public void readImpl()
	{
		iProductID = readD();
		iAmount = readD();
	}

	@Override
	public void runImpl()
	{
		if(!Config.itemmallEnable)
		{
			return;
		}
		L2Player player = getClient().getActiveChar();
		if(player == null)
		{
			return;
		}
		CashShop.getInstance().requestBuyItem(player, iProductID, iAmount);
	}
}