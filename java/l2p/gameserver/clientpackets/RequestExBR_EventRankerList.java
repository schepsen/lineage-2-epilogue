package l2p.gameserver.clientpackets;

import l2p.gameserver.serverpackets.ExBR_LoadEventTopRankers;

public class RequestExBR_EventRankerList extends L2GameClientPacket
{
	private int _eventId;
	private int _day;

	protected void readImpl()
	{
		_eventId = readD();
		_day = readD();
		readD();
	}

	@Override
	protected void runImpl()
	{
		int count = 0;
		int bestScore = 0;
		int myScore = 0;
		getClient().sendPacket(new ExBR_LoadEventTopRankers(_eventId, _day, count, bestScore, myScore));
	}
}