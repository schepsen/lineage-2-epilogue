package l2p.gameserver.clientpackets;

import l2p.gameserver.cache.Msg;
import l2p.gameserver.model.L2Macro;
import l2p.gameserver.model.L2Macro.L2MacroCmd;
import l2p.gameserver.model.L2Player;

/**
 * packet type id 0xcd
 * <p/>
 * sample
 * <p/>
 * cd
 * d // id
 * S // macro name
 * S // unknown  desc
 * S // unknown  acronym
 * c // icon
 * c // count
 * <p/>
 * c // entry
 * c // type
 * d // skill id
 * c // shortcut id
 * S // command name
 * <p/>
 * format:		cdSSScc (ccdcS)
 */
public class RequestMakeMacro extends L2GameClientPacket
{
	private L2Macro _macro;

	@Override
	public void readImpl()
	{
		int id = readD();
		String name = readS(32);
		String desc = readS(64);
		String acronym = readS(4);
		int icon = readC();
		int count = readC();
		if(count > 12)
		{
			count = 12;
		}
		L2MacroCmd[] commands = new L2MacroCmd[count];
		for(int i = 0; i < count; i++)
		{
			int entry = readC();
			int type = readC(); // 1 = skill, 3 = action, 4 = shortcut
			int d1 = readD(); // skill or page number for shortcuts
			int d2 = readC();
			String command = readS().replace(";", "").replace(",", "");
			commands[i] = new L2MacroCmd(entry, type, d1, d2, command);
		}
		_macro = new L2Macro(id, icon, name, desc, acronym, commands);
	}

	@Override
	public void runImpl()
	{
		L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;
		
		// this is max/min index in the db 
		if (_macro.id > 65535 || _macro.id < 0)
			return;
		
		if(activeChar.getMacroses().getAllMacroses().length > 48)
		{
			activeChar.sendPacket(Msg.YOU_MAY_CREATE_UP_TO_48_MACROS);
			return;
		}
		if(_macro.name.length() == 0)
		{
			activeChar.sendPacket(Msg.ENTER_THE_NAME_OF_THE_MACRO);
			return;
		}
		if(_macro.descr.length() > 32)
		{
			activeChar.sendPacket(Msg.MACRO_DESCRIPTIONS_MAY_CONTAIN_UP_TO_32_CHARACTERS);
			return;
		}
		activeChar.registerMacro(_macro);
	}
}