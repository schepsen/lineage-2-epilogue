package l2p.gameserver.clientpackets;

import l2p.Config;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.modules.CashShop;

public class RequestExBR_ProductInfo extends L2GameClientPacket
{
	private int iProductID;

	@Override
	public void readImpl()
	{
		iProductID = readD();
	}

	@Override
	public void runImpl()
	{
		if(!Config.itemmallEnable)
		{
			return;
		}
		L2Player player = getClient().getActiveChar();
		if(player == null)
		{
			return;
		}
		CashShop.getInstance().showItemInfo(player, iProductID);
	}
}