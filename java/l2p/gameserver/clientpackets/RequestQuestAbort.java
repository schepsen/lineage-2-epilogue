package l2p.gameserver.clientpackets;

import l2p.gameserver.instancemanager.QuestManager;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.model.quest.QuestState;
import l2p.gameserver.serverpackets.QuestList;
import l2p.gameserver.serverpackets.SystemMessage;

public class RequestQuestAbort extends L2GameClientPacket
{
	private int _QuestID;
	private int[] noAbortIds = {734, 735, 736, 737, 738};

	@Override
	public void readImpl()
	{
		_QuestID = readD();
	}

	@Override
	public void runImpl()
	{
		L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null || QuestManager.getQuest(_QuestID) == null)
		{
			return;
		}
		for(int id : noAbortIds)
		{
			if(_QuestID == id)
			{
				activeChar.sendMessage("Этот квест нельзя отменить.");
				activeChar.sendPacket(new QuestList(activeChar));
				return;
			}
		}
		QuestState qs = activeChar.getQuestState(QuestManager.getQuest(_QuestID).getName());
		if(qs != null && !qs.isCompleted())
		{
			qs.abortQuest();
			activeChar.sendPacket(new SystemMessage(SystemMessage.S1_IS_ABORTED).addString(QuestManager.getQuest(_QuestID).getDescr(activeChar)));
		}
	}
}