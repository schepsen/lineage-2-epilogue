package l2p.gameserver.clientpackets;

import l2p.Config;
import l2p.gameserver.cache.Msg;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.model.items.L2ItemInstance;
import l2p.gameserver.model.items.PcInventory;
import l2p.gameserver.modules.FloodProtector;
import l2p.gameserver.modules.FloodProtector.Action;
import l2p.gameserver.serverpackets.InventoryUpdate;
import l2p.gameserver.serverpackets.SystemMessage;
import l2p.gameserver.templates.L2Armor.ArmorType;
import l2p.gameserver.templates.L2Item;
import l2p.gameserver.templates.L2Weapon.WeaponType;
import l2p.util.Log;
import l2p.util.Rnd;

public class RequestExEnchantItemAttribute extends L2GameClientPacket
{
	private int _objectId;

	@Override
	public void readImpl()
	{
		_objectId = readD();
	}

	@Override
	public void runImpl()
	{
		L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
		{
			return;
		}
		if(_objectId == -1)
		{
			activeChar.setEnchantScroll(null);
			activeChar.sendPacket(Msg.ELEMENTAL_POWER_ENCHANCER_USAGE_HAS_BEEN_CANCELLED);
			return;
		}
		if(!FloodProtector.tryPerformAction(activeChar, Action.ENCHANT_ITEM))
		{
			activeChar.sendPacket(Msg.INAPPROPRIATE_ENCHANT_CONDITIONS, Msg.ActionFail);
			return;
		}
		if(activeChar.isOutOfControl() || activeChar.isActionsDisabled() || activeChar.isCastingNow() || activeChar.isAttackingNow())
		{
			activeChar.sendPacket(Msg.INAPPROPRIATE_ENCHANT_CONDITIONS, Msg.ActionFail);
			return;
		}
		PcInventory inventory = activeChar.getInventory();
		L2ItemInstance itemToEnchant = inventory.getItemByObjectId(_objectId);
		L2ItemInstance stone = activeChar.getEnchantScroll();
		activeChar.setEnchantScroll(null);
		if(itemToEnchant == null || stone == null)
		{
			activeChar.sendActionFailed();
			return;
		}
		if(itemToEnchant.getOwnerId() != activeChar.getObjectId())
		{
			activeChar.sendPacket(Msg.INAPPROPRIATE_ENCHANT_CONDITIONS, Msg.ActionFail);
			return;
		}
		L2Item item = itemToEnchant.getItem();
		// only weapon & armor
		if(!item.isWeapon() && !item.isArmor())
		{
			activeChar.sendPacket(Msg.INAPPROPRIATE_ENCHANT_CONDITIONS, Msg.ActionFail);
			return;
		}
		// only S80 & S84
		if(item.getCrystalType().cry != L2Item.CRYSTAL_S)
		{
			activeChar.sendPacket(Msg.INAPPROPRIATE_ENCHANT_CONDITIONS, Msg.ActionFail);
			return;
		}
		// only normal weapons & armors
		@SuppressWarnings("rawtypes")
		Enum type = item.getItemType();
		if(type == WeaponType.ETC || type == WeaponType.PET || type == ArmorType.PET || type == ArmorType.SIGIL)
		{
			activeChar.sendPacket(Msg.INAPPROPRIATE_ENCHANT_CONDITIONS, Msg.ActionFail);
			return;
		}
		if((!Config.AttributToPvpItem && item.isPvP()) || item.isUnderwear() || item.isBelt())
		{
			activeChar.sendPacket(Msg.INAPPROPRIATE_ENCHANT_CONDITIONS, Msg.ActionFail);
			return;
		}
		if(!itemToEnchant.canBeEnchanted() && !(item.isCloak() && Config.ALLOW_ATTRIBUTE_CLOAKS))
		{
			activeChar.sendPacket(Msg.INAPPROPRIATE_ENCHANT_CONDITIONS, Msg.ActionFail);
			return;
		}
		if(itemToEnchant.getLocation() != L2ItemInstance.ItemLocation.INVENTORY && itemToEnchant.getLocation() != L2ItemInstance.ItemLocation.PAPERDOLL)
		{
			activeChar.sendPacket(Msg.INAPPROPRIATE_ENCHANT_CONDITIONS, Msg.ActionFail);
			return;
		}
		if(activeChar.getPrivateStoreType() != L2Player.STORE_PRIVATE_NONE)
		{
			activeChar.sendPacket(Msg.YOU_CANNOT_ADD_ELEMENTAL_POWER_WHILE_OPERATING_A_PRIVATE_STORE_OR_PRIVATE_WORKSHOP, Msg.ActionFail);
			return;
		}
		if(itemToEnchant.isStackable() || (stone = inventory.getItemByObjectId(stone.getObjectId())) == null)
		{
			activeChar.sendActionFailed();
			return;
		}
		int itemType2 = item.getType2();
		if(itemToEnchant.getAttributeElement() != L2Item.ATTRIBUTE_NONE && itemToEnchant.getAttributeElement() != stone.getEnchantAttributeStoneElement(itemType2 == L2Item.TYPE2_SHIELD_ARMOR))
		{
			activeChar.sendPacket(Msg.ANOTHER_ELEMENTAL_POWER_HAS_ALREADY_BEEN_ADDED_THIS_ELEMENTAL_POWER_CANNOT_BE_ADDED, Msg.ActionFail);
			return;
		}
		int maxValue = itemType2 == L2Item.TYPE2_WEAPON ? 150 : 60;
		if(stone.isAttributeCrystal())
		{
			maxValue += itemType2 == L2Item.TYPE2_WEAPON ? 150 : 60;
		}
		if(itemToEnchant.getAttributeElementValue() >= maxValue)
		{
			activeChar.sendPacket(Msg.ELEMENTAL_POWER_ENCHANCER_USAGE_HAS_BEEN_CANCELLED, Msg.ActionFail);
			return;
		}
		Log.add(activeChar.getName() + "|Trying to attribute enchant|" + itemToEnchant.getItemId() + "|attribute:" + stone.getEnchantAttributeStoneElement(itemType2 == L2Item.TYPE2_SHIELD_ARMOR) + "|" + itemToEnchant.getObjectId(), "enchants");
		L2ItemInstance removedStone;
		synchronized(inventory)
		{
			removedStone = inventory.destroyItem(stone.getObjectId(), 1, true);
		}
		if(removedStone == null)
		{
			activeChar.sendActionFailed();
			return;
		}
		if(Rnd.chance(stone.isAttributeCrystal() ? Config.ENCHANT_ATTRIBUTE_CRYSTAL_CHANCE : Config.ENCHANT_ATTRIBUTE_STONE_CHANCE))
		{
			if(itemToEnchant.getEnchantLevel() == 0)
			{
				SystemMessage sm = new SystemMessage(SystemMessage.S2_ELEMENTAL_POWER_HAS_BEEN_ADDED_SUCCESSFULLY_TO_S1);
				sm.addItemName(itemToEnchant.getItemId());
				sm.addItemName(stone.getItemId());
				activeChar.sendPacket(sm);
			}
			else
			{
				SystemMessage sm = new SystemMessage(SystemMessage.S3_ELEMENTAL_POWER_HAS_BEEN_ADDED_SUCCESSFULLY_TO__S1S2);
				sm.addNumber(itemToEnchant.getEnchantLevel());
				sm.addItemName(itemToEnchant.getItemId());
				sm.addItemName(stone.getItemId());
				activeChar.sendPacket(sm);
			}
			int value = itemType2 == L2Item.TYPE2_WEAPON ? 5 : 6;
			// Для оружия 1й камень дает +20 атрибута
			if(itemToEnchant.getAttributeElementValue() == 0 && itemType2 == L2Item.TYPE2_WEAPON)
			{
				value = 20;
			}
			itemToEnchant.setAttributeElement(stone.getEnchantAttributeStoneElement(itemType2 == L2Item.TYPE2_SHIELD_ARMOR), itemToEnchant.getAttributeElementValue() + value, true);
			inventory.refreshListeners();
			activeChar.sendPacket(new InventoryUpdate().addModifiedItem(itemToEnchant));
			Log.add(activeChar.getName() + "|Successfully enchanted by attribute|" + itemToEnchant.getItemId() + "|to+" + itemToEnchant.getAttributeElementValue() + "|" + (stone.isAttributeCrystal() ? Config.ENCHANT_ATTRIBUTE_CRYSTAL_CHANCE : Config.ENCHANT_ATTRIBUTE_STONE_CHANCE), "enchants");
			Log.LogItem(activeChar, Log.EnchantItem, itemToEnchant);
		}
		else
		{
			activeChar.sendPacket(Msg.YOU_HAVE_FAILED_TO_ADD_ELEMENTAL_POWER);
			Log.add(activeChar.getName() + "|Failed to enchant attribute|" + itemToEnchant.getItemId() + "|+" + itemToEnchant.getAttributeElementValue() + "|" + (stone.isAttributeCrystal() ? Config.ENCHANT_ATTRIBUTE_CRYSTAL_CHANCE : Config.ENCHANT_ATTRIBUTE_STONE_CHANCE), "enchants");
		}
		activeChar.setEnchantScroll(null);
		activeChar.sendChanges();
	}
}