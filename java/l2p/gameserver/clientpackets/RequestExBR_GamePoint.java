package l2p.gameserver.clientpackets;

import l2p.Config;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.modules.CashShop;

public class RequestExBR_GamePoint extends L2GameClientPacket
{
	@Override
	public void runImpl()
	{
		if(!Config.itemmallEnable)
		{
			return;
		}
		L2Player player = getClient().getActiveChar();
		if(player == null)
		{
			return;
		}
		CashShop.getInstance().validateMyPoints(player);
	}

	@Override
	public void readImpl()
	{
	}
}