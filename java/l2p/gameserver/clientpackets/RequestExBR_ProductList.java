package l2p.gameserver.clientpackets;

import l2p.Config;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.modules.CashShop;

public class RequestExBR_ProductList extends L2GameClientPacket
{
	@Override
	public void readImpl()
	{
	}

	@Override
	public void runImpl()
	{
		if(!Config.itemmallEnable)
		{
			return;
		}
		L2Player player = getClient().getActiveChar();
		if(player == null)
		{
			return;
		}
		CashShop.getInstance().showList(player);
	}
}