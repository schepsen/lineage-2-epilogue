package l2p.gameserver.clientpackets;

import l2p.gameserver.model.L2Player;
import l2p.gameserver.templates.L2Item;

public class RequestUnEquipItem extends L2GameClientPacket
{
	private int _slot;
	
	/**
	 * packet type id 0x16
	 * format: cd
	 */
	@Override
	public void readImpl()
	{
		_slot = readD();
	}
	
	@Override
	public void runImpl()
	{
		L2Player activeChar = getClient().getActiveChar();
		if (activeChar == null)
		{
			return;
		}
		// Нельзя снимать проклятое оружие и флаги
		if ((_slot == L2Item.SLOT_R_HAND || _slot == L2Item.SLOT_L_HAND || _slot == L2Item.SLOT_LR_HAND) && (activeChar.isCursedWeaponEquipped() || activeChar.isCombatFlagEquipped() || activeChar.isTerritoryFlagEquipped()))
		{
			return;
		}
		
		switch (_slot)
		{
			case L2Item.SLOT_NECK:
			case L2Item.SLOT_L_EAR:
			case L2Item.SLOT_R_EAR:
			case L2Item.SLOT_L_FINGER:
			case L2Item.SLOT_R_FINGER:
			case L2Item.SLOT_HAIR:
			case L2Item.SLOT_DHAIR:
			case L2Item.SLOT_HAIRALL:
			case L2Item.SLOT_HEAD:
			case L2Item.SLOT_R_HAND:
			case L2Item.SLOT_L_HAND:
			case L2Item.SLOT_GLOVES:
			case L2Item.SLOT_LEGS:
			case L2Item.SLOT_CHEST:
			case L2Item.SLOT_FULL_ARMOR:
			case L2Item.SLOT_FORMAL_WEAR:
			case L2Item.SLOT_BACK:
			case L2Item.SLOT_FEET:
			case L2Item.SLOT_UNDERWEAR:
			case L2Item.SLOT_BELT:
			case L2Item.SLOT_LR_HAND:
			case L2Item.SLOT_L_BRACELET:
			case L2Item.SLOT_R_BRACELET:
			case L2Item.SLOT_DECO:
				activeChar.getInventory().unEquipItemInBodySlotAndNotify(_slot, null);
		}
	}
}