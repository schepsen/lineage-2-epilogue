package l2p.gameserver.ai;

import java.util.concurrent.ScheduledFuture;

import l2p.common.ThreadPoolManager;
import l2p.gameserver.model.L2Character;
import l2p.gameserver.tables.SkillTable;

/**
 * Healers of Lematan in _129_PailakaDevilsLegacy.java
 * - can't walk
 * 
 * @author hex1r0
 */
public class LematansFollower extends Fighter
{
	private static int LEMATAN = 18633;
	private L2Character _lematanInstance = null;
	private ScheduledFuture<?> _healTask;
	
	public LematansFollower(L2Character actor)
	{
		super(actor);
	}
	
	private void findLematan()
	{
		if (getActor().isCastingNow())
			return;
		
		if (_lematanInstance == null)
		{
			for (L2Character c : getActor().getAroundCharacters(1000, 100))
			{
				if (c.getNpcId() == LEMATAN)
				{
					_lematanInstance = c;
					healLematan();
				}
			}
		}
		else
		{
			if (getActor().isInRange(_lematanInstance, 1000))
			{
				healLematan();
			}
			else
			{
				_lematanInstance = null;
			}
		}
	}
	
	private void healLematan()
	{
		if (_lematanInstance != null && !_lematanInstance.isDead())
		{
			if (_lematanInstance.getCurrentHpPercents() < 100.0)
			{
				getActor().setTarget(_lematanInstance);
				getActor().doCast(SkillTable.getInstance().getInfo(5712, 1), _lematanInstance, true);
				SkillTable.getInstance().getInfo(5713, 5).getEffects((L2Character)getActor(), _lematanInstance, false, false);
			}
		}
	}
	@Override
	protected void onEvtDead(L2Character killer)
	{
		if (_healTask != null)
		{
			_healTask.cancel(false);
			_healTask = null;
		}
		
		super.onEvtDead(killer);
	}
	
	@Override
	protected void onEvtSpawn()
	{
		if (_healTask == null)
			_healTask = ThreadPoolManager.getInstance().scheduleAiAtFixedRate(new HealTask(this), 1000, 10000, false);
		
		super.onEvtSpawn();
	}
	
	@Override
	protected boolean randomWalk()
	{
		return false;
	}
	
	private class HealTask implements Runnable
	{
		private LematansFollower _caller = null;
		
		public HealTask(LematansFollower follower)
		{
			_caller = follower;
		}
		
		public void run()
		{
			_caller.findLematan();
		}
	}
}