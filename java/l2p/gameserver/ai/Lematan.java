package l2p.gameserver.ai;

import l2p.extensions.listeners.L2ZoneEnterLeaveListener;
import l2p.gameserver.geodata.GeoEngine;
import l2p.gameserver.model.L2Character;
import l2p.gameserver.model.L2Object;
import l2p.gameserver.model.L2Spawn;
import l2p.gameserver.model.L2Zone;
import l2p.gameserver.model.instances.L2NpcInstance;
import l2p.gameserver.tables.NpcTable;
import l2p.util.Location;

/**
 * Lamatan - boos in _129_PailakaDevilsLegacy.java
 * Lematan teleports to ship when HP is below 50%
 * 
 * @author hex1r0
 */
public class Lematan extends Fighter
{
	private static int LEMATANS_FOLLOWER = 18634;
	
	private static Location SPAWN_POINTS[] =
	{
		new Location(84704, -208726, -3336),
		new Location(85136, -208430, -3336),
		new Location(85136, -208430, -3336),
		new Location(85271, -208726, -3336),
		new Location(84851, -209024, -3336),
		new Location(85144, -208995, -3336)
	};
	
	private boolean _teleported;
	
	public Lematan(L2Character actor)
	{
		super(actor);
		_teleported = false;
	}
	
	@Override
	protected boolean maybeMoveToHome()
	{
		L2NpcInstance actor = getActor();
		Location loc = actor.getSpawnedLoc();
		if (actor.isInRange(loc, 10000))
		{
			return true;
		}
		return true;
	}
	
	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{
		L2NpcInstance actor = getActor();
		if (actor.getCurrentHpPercents() < 50 && !_teleported)
		{
			Location loc = (new Location(84968, -208728, -3367));
			actor.setSpawnedLoc(loc);
			actor.getAI().setIntention(CtrlIntention.AI_INTENTION_ACTIVE, null, null);
			actor.teleToLocation(loc);
			_teleported = true;
			try
			{
				for (int i = 0; i < SPAWN_POINTS.length; i++) 
				{
					Location oldPos = SPAWN_POINTS[i];
					L2Spawn s = new L2Spawn(NpcTable.getTemplate(LEMATANS_FOLLOWER));
					Location newPos = GeoEngine.findPointToStay(oldPos.x, oldPos.y, oldPos.z, 0, 0, actor.getReflection().getGeoIndex());
					s.setReflection(actor.getReflection().getId());
					s.setLoc(newPos);
					s.doSpawn(true);
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		
		super.onEvtAttacked(attacker, damage);
	}
	
	@Override
	protected void onEvtDead(L2Character killer) 
	{
		_teleported = false;
		
		super.onEvtDead(killer);
	}

	public static class ZoneListener extends L2ZoneEnterLeaveListener
	{
		@Override
		public void objectEntered(L2Zone zone, L2Object object)
		{
		}
		
		@Override
		public void objectLeaved(L2Zone zone, L2Object object)
		{
		}
	}
}