package l2p.gameserver.ai;

import l2p.common.ThreadPoolManager;
import l2p.gameserver.instancemanager.TowerOfNaiaManager;
import l2p.gameserver.model.L2Character;
import l2p.gameserver.model.instances.L2MonsterInstance;
import l2p.gameserver.taskmanager.SpawnTaskManager;

/**
 * @author hex1r0
 */
public class NaiaSpore extends Fighter
{
	public NaiaSpore(L2Character actor)
	{
		super(actor);
	}
	
	public void notifyEpidosIndexReached()
	{
		L2MonsterInstance actor = (L2MonsterInstance)getActor();
		if (actor.isDead())
			SpawnTaskManager.getInstance().cancelSpawnTask(actor);
		
		getActor().moveToLocation(TowerOfNaiaManager.CENTRAL_COLUMN, 0, false);
		ThreadPoolManager.getInstance().scheduleGeneral(new DespawnTask(), 3000);
	}
	
	@Override
	protected void onEvtDead(L2Character killer)
	{
		TowerOfNaiaManager.handleEpidosIndex((L2MonsterInstance) getActor());
		
		super.onEvtDead(killer);
	}

	@Override
	protected void onEvtSpawn()
	{
		super.onEvtSpawn();
		
		L2MonsterInstance actor = (L2MonsterInstance)getActor();
		if (TowerOfNaiaManager.isEpidosSpawned())
			actor.decayMe();
		else
			TowerOfNaiaManager.addSpore(actor);
	}
	
	private class DespawnTask implements Runnable
	{
		@Override
		public void run()
		{
			getActor().decayMe();
		}
	}
}
