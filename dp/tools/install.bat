@echo off

REM ###############################################
REM ## ������ ���� ��ࠬ���� ��襩 ���� ������  ##
REM ###############################################
REM ���� � 䠩�� MYSQL.exe �ய��� ��易⥫쭮.
REM �᫨ ���� ⠪�� ��, � ⮣�� ��⠢�� ��� ����.
set mysqlBinPath=../sql/

set DateT=%date%

REM ����� ������
set lsuser=root
set lspass=
set lsdb=l2jmmocore
set lshost=localhost

REM ���� ������
set gsuser=root
set gspass=
set gsdb=l2jmmocore
set gshost=localhost
REM ############################################

set mysqldumpPath="%mysqlBinPath%\mysqldump"
set mysqlPath="%mysqlBinPath%\mysql"

set MYL=%mysqlPath% -h %lshost% -u %lsuser% --password=%lspass% -D %lsdb%
set MYG=%mysqlPath% -h %gshost% -u %gsuser% --password=%gspass% -D %gsdb%

:Welcome
cls
title MMOCORE DEVELOPMENT TEAM
echo.
echo. ************************************************************
echo.
echo.            ��஢� ��ࢥ� MMOCORE Epilogue CT2.4           
echo.
echo. ************************************************************
echo.
echo.   (1) LS Datenbank installieren
echo.
echo.   (2) GS Datenbank installieren
echo.
echo. ************************************************************
echo. 

set Welcomeprompt=x
set /p Welcomeprompt= Ihre Auswahl: 
if /i %Welcomeprompt%==1 goto LoginInstall
if /i %Welcomeprompt%==2 goto GamesInstall
if /i %Welcomeprompt%==3 goto Quit
goto Welcome

:LoginInstall

@cls
echo.
echo. ************************************************************
echo.
echo.            ��஢� ��ࢥ� MMOCORE Epilogue CT2.4           
echo.
echo. ************************************************************
echo.  
echo.   ���⪠ �� [ %lsdb% ]
%MYL% < loginserver.sql
echo.     
echo.   ��⠭���� ���� ������ ����� ��ࢥ�
echo.
%MYL% < ../sql/login/accounts.sql
echo.   [MMOCORE] accounts.sql
%MYL% < ../sql/login/banned_ips.sql
echo.   [MMOCORE] banned_ips.sql
%MYL% < ../sql/login/gameservers.sql
echo.   [MMOCORE] gameservers.sql
%MYL% < ../sql/login/lock.sql
echo.   [MMOCORE] lock.sql
%MYL% < ../sql/login/loginserv_log.sql
echo.   [MMOCORE] loginserv_log.sql
echo. 
echo    Die LS Datenbank wurde erfolgreich installiert!
echo.                                                                       
echo. ************************************************************
echo.
pause
goto :Welcome

:GamesInstall
@cls
echo.
echo. ************************************************************
echo.
echo.            ��஢� ��ࢥ� MMOCORE Epilogue CT2.4           
echo.
echo. ************************************************************
echo.  
echo.   ���⪠ �� [ %gsdb% ]
echo. 
%MYG% < gameserver.sql
echo.   ���⪠ �� ��諠 �ᯥ譮
echo.   
echo.   ��⠭���� ���� ������ ��஢��� ��ࢥ�
echo.
%MYG% < ../sql/game/ai_params.sql
echo.   [MMOCORE] ai_params.sql
%MYG% < ../sql/game/ally_data.sql
echo.   [MMOCORE] ally_data.sql
%MYG% < ../sql/game/armor.sql
echo.   [MMOCORE] armor.sql
%MYG% < ../sql/game/armorsets.sql
echo.   [MMOCORE] armorsets.sql
%MYG% < ../sql/game/armor_ex.sql
echo.   [MMOCORE] armor_ex.sql
%MYG% < ../sql/game/auction.sql
echo.   [MMOCORE] auction.sql
%MYG% < ../sql/game/auction_bid.sql
echo.   [MMOCORE] auction_bid.sql
%MYG% < ../sql/game/auto_chat.sql
echo.   [MMOCORE] auto_chat.sql
%MYG% < ../sql/game/auto_chat_text.sql
echo.   [MMOCORE] auto_chat_text.sql
%MYG% < ../sql/game/bans.sql
echo.   [MMOCORE] bans.sql
%MYG% < ../sql/game/bonus.sql
echo.   [MMOCORE] bonus.sql
%MYG% < ../sql/game/castle.sql
echo.   [MMOCORE] castle.sql
%MYG% < ../sql/game/castle_manor_procure.sql
echo.   [MMOCORE] castle_manor_procure.sql
%MYG% < ../sql/game/castle_manor_production.sql
echo.   [MMOCORE] castle_manor_production.sql
%MYG% < ../sql/game/characters.sql
echo.   [MMOCORE] characters.sql
%MYG% < ../sql/game/character_blocklist.sql
echo.   [MMOCORE] character_blocklist.sql
%MYG% < ../sql/game/character_bookmarks.sql
echo.   [MMOCORE] character_bookmarks.sql
%MYG% < ../sql/game/character_cash.sql
echo.   [MMOCORE] character_cash.sql
%MYG% < ../sql/game/character_effects_save.sql
echo.   [MMOCORE] character_effects_save.sql
%MYG% < ../sql/game/character_friends.sql
echo.   [MMOCORE] character_friends.sql
%MYG% < ../sql/game/character_hennas.sql
echo.   [MMOCORE] character_hennas.sql
%MYG% < ../sql/game/character_l2top_votes.sql
echo.   [MMOCORE] character_l2top_votes.sql
%MYG% < ../sql/game/character_macroses.sql
echo.   [MMOCORE] character_macroses.sql
%MYG% < ../sql/game/character_mmotop_votes.sql
echo.   [MMOCORE] character_mmotop_votes.sql
%MYG% < ../sql/game/character_quests.sql
echo.   [MMOCORE] character_quests.sql
%MYG% < ../sql/game/character_recipebook.sql
echo.   [MMOCORE] character_recipebook.sql
%MYG% < ../sql/game/character_shortcuts.sql
echo.   [MMOCORE] character_shortcuts.sql
%MYG% < ../sql/game/character_skills.sql
echo.   [MMOCORE] character_skills.sql
%MYG% < ../sql/game/character_skills_save.sql
echo.   [MMOCORE] character_skills_save.sql
%MYG% < ../sql/game/character_subclasses.sql
echo.   [MMOCORE] character_subclasses.sql
%MYG% < ../sql/game/character_variables.sql
echo.   [MMOCORE] character_variables.sql
%MYG% < ../sql/game/char_templates.sql
echo.   [MMOCORE] char_templates.sql
%MYG% < ../sql/game/clanhall.sql
echo.   [MMOCORE] clanhall.sql
%MYG% < ../sql/game/clan_data.sql
echo.   [MMOCORE] clan_data.sql
%MYG% < ../sql/game/clan_notices.sql
echo.   [MMOCORE] clan_notices.sql
%MYG% < ../sql/game/clan_privs.sql
echo.   [MMOCORE] clan_privs.sql
%MYG% < ../sql/game/clan_skills.sql
echo.   [MMOCORE] clan_skills.sql
%MYG% < ../sql/game/clan_subpledges.sql
echo.   [MMOCORE] clan_subpledges.sql
%MYG% < ../sql/game/clan_wars.sql
echo.   [MMOCORE] clan_wars.sql
%MYG% < ../sql/game/class_list.sql
echo.   [MMOCORE] class_list.sql
%MYG% < ../sql/game/couples.sql
echo.   [MMOCORE] couples.sql
%MYG% < ../sql/game/craftcount.sql
echo.   [MMOCORE] craftcount.sql
%MYG% < ../sql/game/cursed_weapons.sql
echo.   [MMOCORE] cursed_weapons.sql
%MYG% < ../sql/game/dropcount.sql
echo.   [MMOCORE] dropcount.sql
%MYG% < ../sql/game/droplist.sql
echo.   [MMOCORE] droplist.sql
%MYG% < ../sql/game/epic_boss_spawn.sql
echo.   [MMOCORE] epic_boss_spawn.sql
%MYG% < ../sql/game/etcitem.sql
echo.   [MMOCORE] etcitem.sql
%MYG% < ../sql/game/fake_pcs.sql
echo.   [MMOCORE] fake_pcs.sql
%MYG% < ../sql/game/fish.sql
echo.   [MMOCORE] fish.sql
%MYG% < ../sql/game/fishreward.sql
echo.   [MMOCORE] fishreward.sql
%MYG% < ../sql/game/forts.sql
echo.   [MMOCORE] forts.sql
%MYG% < ../sql/game/four_sepulchers_spawnlist.sql
echo.   [MMOCORE] four_sepulchers_spawnlist.sql
%MYG% < ../sql/game/games.sql
echo.   [MMOCORE] games.sql
%MYG% < ../sql/game/game_log.sql
echo.   [MMOCORE] game_log.sql
%MYG% < ../sql/game/global_tasks.sql
echo.   [MMOCORE] global_tasks.sql
%MYG% < ../sql/game/henna.sql
echo.   [MMOCORE] henna.sql
%MYG% < ../sql/game/henna_trees.sql
echo.   [MMOCORE] henna_trees.sql
%MYG% < ../sql/game/heroes.sql
echo.   [MMOCORE] heroes.sql
%MYG% < ../sql/game/items.sql
echo.   [MMOCORE] items.sql
%MYG% < ../sql/game/items_delayed.sql
echo.   [MMOCORE] items_delayed.sql
%MYG% < ../sql/game/item_attributes.sql
echo.   [MMOCORE] item_attributes.sql
%MYG% < ../sql/game/item_mall.sql
echo.   [MMOCORE] item_mall.sql
%MYG% < ../sql/game/killcount.sql
echo.   [MMOCORE] killcount.sql
%MYG% < ../sql/game/lastimperialtomb_spawnlist.sql
echo.   [MMOCORE] lastimperialtomb_spawnlist.sql
%MYG% < ../sql/game/locations.sql
echo.   [MMOCORE] locations.sql
%MYG% < ../sql/game/lvlupgain.sql
echo.   [MMOCORE] lvlupgain.sql
%MYG% < ../sql/game/mail.sql
echo.   [MMOCORE] mail.sql
%MYG% < ../sql/game/mapregion.sql
echo.   [MMOCORE] mapregion.sql
%MYG% < ../sql/game/merchant_areas_list.sql
echo.   [MMOCORE] merchant_areas_list.sql
%MYG% < ../sql/game/minions.sql
echo.   [MMOCORE] minions.sql
%MYG% < ../sql/game/npc.sql
echo.   [MMOCORE] npc.sql
%MYG% < ../sql/game/npcskills.sql
echo.   [MMOCORE] npcskills.sql
%MYG% < ../sql/game/olympiad_nobles.sql
echo.   [MMOCORE] olympiad_nobles.sql
%MYG% < ../sql/game/petitions.sql
echo.   [MMOCORE] petitions.sql
%MYG% < ../sql/game/pets.sql
echo.   [MMOCORE] pets.sql
%MYG% < ../sql/game/pets_skills.sql
echo.   [MMOCORE] pets_skills.sql
%MYG% < ../sql/game/pet_data.sql
echo.   [MMOCORE] pet_data.sql
%MYG% < ../sql/game/raidboss_points.sql
echo.   [MMOCORE] raidboss_points.sql
%MYG% < ../sql/game/raidboss_status.sql
echo.   [MMOCORE] raidboss_status.sql
%MYG% < ../sql/game/random_spawn.sql
echo.   [MMOCORE] random_spawn.sql
%MYG% < ../sql/game/random_spawn_loc.sql
echo.   [MMOCORE] random_spawn_loc.sql
%MYG% < ../sql/game/recipes.sql
echo.   [MMOCORE] recipes.sql
%MYG% < ../sql/game/recitems.sql
echo.   [MMOCORE] recitems.sql
%MYG% < ../sql/game/residence_functions.sql
echo.   [MMOCORE] residence_functions.sql
%MYG% < ../sql/game/server_variables.sql
echo.   [MMOCORE] server_variables.sql
%MYG% < ../sql/game/seven_signs.sql
echo.   [MMOCORE] seven_signs.sql
%MYG% < ../sql/game/seven_signs_festival.sql
echo.   [MMOCORE] seven_signs_festival.sql
%MYG% < ../sql/game/seven_signs_status.sql
echo.   [MMOCORE] seven_signs_status.sql
%MYG% < ../sql/game/siege_clans.sql
echo.   [MMOCORE] siege_clans.sql
%MYG% < ../sql/game/siege_door.sql
echo.   [MMOCORE] siege_door.sql
%MYG% < ../sql/game/siege_doorupgrade.sql
echo.   [MMOCORE] siege_doorupgrade.sql
%MYG% < ../sql/game/siege_guards.sql
echo.   [MMOCORE] siege_guards.sql
%MYG% < ../sql/game/siege_territory_members.sql
echo.   [MMOCORE] siege_territory_members.sql
%MYG% < ../sql/game/skills.sql
echo.   [MMOCORE] skills.sql
%MYG% < ../sql/game/skill_learn.sql
echo.   [MMOCORE] skill_learn.sql
%MYG% < ../sql/game/skill_spellbooks.sql
echo.   [MMOCORE] skill_spellbooks.sql
%MYG% < ../sql/game/skill_trees.sql
echo.   [MMOCORE] skill_trees.sql
%MYG% < ../sql/game/spawnlist.sql
echo.   [MMOCORE] spawnlist.sql
%MYG% < ../sql/game/tournament_class_list.sql
echo.   [MMOCORE] tournament_class_list.sql
%MYG% < ../sql/game/tournament_table.sql
echo.   [MMOCORE] tournament_table.sql
%MYG% < ../sql/game/tournament_teams.sql
echo.   [MMOCORE] tournament_teams.sql
%MYG% < ../sql/game/tournament_variables.sql
echo.   [MMOCORE] tournament_variables.sql
%MYG% < ../sql/game/weapon.sql
echo.   [MMOCORE] weapon.sql
%MYG% < ../sql/game/weapon_ex.sql
echo.   [MMOCORE] weapon_ex.sql
echo. 
echo    Die GS-Datenbank wurde erfolgreich installiert!
echo.                                                                       
echo. ************************************************************
echo.
pause
goto :Welcome

:Quit
