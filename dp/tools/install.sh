#!/bin/bash
trap finish 2

configure() {
echo "**************************************************"
echo ""
echo "    MMOCORE DEVELOPMENT TEAM DATABASE INSTALLER   "
echo ""
echo "**************************************************"
echo "" 
MYSQLDUMPPATH=`which -a mysqldump 2>/dev/null`
MYSQLPATH=`which -a mysql 2>/dev/null`
if [ $? -ne 0 ]; then
echo "We were unable to find MySQL binaries on your path"
while :
 do
  echo -ne "\nPlease enter MySQL binaries directory (no trailing slash): "
  read MYSQLBINPATH
    if [ -e "$MYSQLBINPATH" ] && [ -d "$MYSQLBINPATH" ] && \
       [ -e "$MYSQLBINPATH/mysqldump" ] && [ -e "$MYSQLBINPATH/mysql" ]; then
       MYSQLDUMPPATH="$MYSQLBINPATH/mysqldump"
       MYSQLPATH="$MYSQLBINPATH/mysql"
       break
    else
       echo "The data you entered is invalid. Please verify and try again."
       exit 1
    fi
 done
fi
#LS
echo -ne "\nPlease enter MySQL Login Server hostname (default localhost): "
read LSDBHOST
if [ -z "$LSDBHOST" ]; then
  LSDBHOST="localhost"
fi
echo -ne "\nPlease enter MySQL Login Server database name (default l2jmmocore): "
read LSDB
if [ -z "$LSDB" ]; then
  LSDB="l2jmmocore"
fi
echo -ne "\nPlease enter MySQL Login Server user (default root): "
read LSUSER
if [ -z "$LSUSER" ]; then
  LSUSER="root"
fi
echo -ne "\nPlease enter MySQL Login Server $LSUSER's password (won't be displayed) :"
stty -echo
read LSPASS
stty echo
echo ""
if [ -z "$LSPASS" ]; then
  echo "Hum.. I'll let it be but don't be stupid and avoid empty passwords"
elif [ "$LSUSER" == "$LSPASS" ]; then
  echo "You're not too brilliant choosing passwords huh?"
fi
#GS
echo -ne "\nPlease enter MySQL Game Server hostname (default $LSDBHOST): "
read GSDBHOST
if [ -z "$GSDBHOST" ]; then
  GSDBHOST="$LSDBHOST"
fi
echo -ne "\nPlease enter MySQL Game Server database name (default $LSDB): "
read GSDB
if [ -z "$GSDB" ]; then
  GSDB="$LSDB"
fi
echo -ne "\nPlease enter MySQL Game Server user (default $LSUSER): "
read GSUSER
if [ -z "$GSUSER" ]; then
  GSUSER="$LSUSER"
fi
echo -ne "\nPlease enter MySQL Game Server $GSUSER's password (won't be displayed): "
stty -echo
read GSPASS
stty echo
echo ""
if [ -z "$GSPASS" ]; then
  echo "Hum.. I'll let it be but don't be stupid and avoid empty passwords"
elif [ "$GSUSER" == "$GSPASS" ]; then
  echo "You're not too brilliant choosing passwords huh?"
fi
save_config $1
}

save_config() {
if [ -n "$1" ]; then
CONF="$1"
else 
CONF="datenbank.ini"
fi
echo ""
echo -ne "Shall I generate config file? (Y/N):"
read SAVE
if [ "$SAVE" == "y" -o "$SAVE" == "Y" -o "$SAVE" == "" ];then 
cat <<EOF>$CONF
MYSQLDUMPPATH=$MYSQLDUMPPATH
MYSQLPATH=$MYSQLPATH
LSDBHOST=$LSDBHOST
LSDB=$LSDB
LSUSER=$LSUSER
LSPASS=$LSPASS
GSDBHOST=$GSDBHOST
GSDB=$GSDB
GSUSER=$GSUSER
GSPASS=$GSPASS
EOF
chmod 600 $CONF
echo "[MMOCORE] Saved"
echo ""
elif [ "$SAVE" != "n" -a "$SAVE" != "N" ]; then
  save_config
fi
}
load_config() {
if [ -n "$1" ]; then
CONF="$1"
else 
CONF="datenbank.ini"
fi
echo ""
if [ -e "$CONF" ] && [ -f "$CONF" ]; then
. $CONF
else
configure $CONF
fi
}
welcome(){
clear
echo ""
echo "**************************************************"
echo ""
echo "    MMOCORE DEVELOPMENT TEAM DATABASE INSTALLER   "
echo ""
echo "**************************************************"
echo ""
echo "(1) LS Datenbank installieren"
echo ""
echo "(2) GS Datenbank installieren"
echo ""
echo ""
echo ""
echo "(3) Diese Anwendung verlassen"
echo ""
echo "**************************************************"
read VALUE
case "$VALUE" in
  "1") logininstall; welcome;;
  "2") gameinstall; welcome;;
  "3") finish;;
esac
}
logininstall(){
clear
echo ""
echo "**************************************************"
echo ""
echo "    MMOCORE DEVELOPMENT TEAM DATABASE INSTALLER   "
echo ""
echo "**************************************************"
echo ""
$MYL  < loginserver.sql &> /dev/null
$MYL  < ../sql/login/accounts.sql &> /dev/null
echo "[MMOCORE] accounts.sql"
$MYL  < ../sql/login/banned_ips.sql &> /dev/null
echo "[MMOCORE] banned_ips.sql"
$MYL  < ../sql/login/gameservers.sql &> /dev/null
echo "[MMOCORE] gameservers.sql"
$MYL  < ../sql/login/lock.sql &> /dev/null
echo "[MMOCORE] lock.sql"
$MYL  < ../sql/login/loginserv_log.sql &> /dev/null
echo "[MMOCORE] loginserv_log.sql"
echo ""
echo "**************************************************"
}
gameinstall(){
clear
echo ""
echo "**************************************************"
echo ""
echo "    MMOCORE DEVELOPMENT TEAM DATABASE INSTALLER   "
echo ""
echo "**************************************************"
echo ""
$MYL  < gameserver.sql &> /dev/null
$MYG < ../sql/game/ai_params.sql &> /dev/null
echo "[MMOCORE] ai_params.sql"
$MYG < ../sql/game/ally_data.sql &> /dev/null
echo "[MMOCORE] ally_data.sql"
$MYG < ../sql/game/armor.sql &> /dev/null
echo "[MMOCORE] armor.sql"
$MYG < ../sql/game/armorsets.sql &> /dev/null
echo "[MMOCORE] armorsets.sql"
$MYG < ../sql/game/armor_ex.sql &> /dev/null
echo "[MMOCORE] armor_ex.sql"
$MYG < ../sql/game/auction.sql &> /dev/null
echo "[MMOCORE] auction.sql"
$MYG < ../sql/game/auction_bid.sql &> /dev/null
echo "[MMOCORE] auction_bid.sql"
$MYG < ../sql/game/auto_chat.sql &> /dev/null
echo "[MMOCORE] auto_chat.sql"
$MYG < ../sql/game/auto_chat_text.sql &> /dev/null
echo "[MMOCORE] auto_chat_text.sql"
$MYG < ../sql/game/bans.sql &> /dev/null
echo "[MMOCORE] bans.sql"
$MYG < ../sql/game/bonus.sql &> /dev/null
echo "[MMOCORE] bonus.sql"
$MYG < ../sql/game/castle.sql &> /dev/null
echo "[MMOCORE] castle.sql"
$MYG < ../sql/game/castle_manor_procure.sql &> /dev/null
echo "[MMOCORE] castle_manor_procure.sql"
$MYG < ../sql/game/castle_manor_production.sql &> /dev/null
echo "[MMOCORE] castle_manor_production.sql"
$MYG < ../sql/game/characters.sql &> /dev/null
echo "[MMOCORE] characters.sql"
$MYG < ../sql/game/character_blocklist.sql &> /dev/null
echo "[MMOCORE] character_blocklist.sql"
$MYG < ../sql/game/character_bookmarks.sql &> /dev/null
echo "[MMOCORE] character_bookmarks.sql"
$MYG < ../sql/game/character_cash.sql &> /dev/null
echo "[MMOCORE] character_cash.sql"
$MYG < ../sql/game/character_effects_save.sql &> /dev/null
echo "[MMOCORE] character_effects_save.sql"
$MYG < ../sql/game/character_friends.sql &> /dev/null
echo "[MMOCORE] character_friends.sql"
$MYG < ../sql/game/character_hennas.sql &> /dev/null
echo "[MMOCORE] character_hennas.sql"
$MYG < ../sql/game/character_l2top_votes.sql &> /dev/null
echo "[MMOCORE] character_l2top_votes.sql"
$MYG < ../sql/game/character_macroses.sql &> /dev/null
echo "[MMOCORE] character_macroses.sql"
$MYG < ../sql/game/character_mmotop_votes.sql &> /dev/null
echo "[MMOCORE] character_mmotop_votes.sql"
$MYG < ../sql/game/character_quests.sql &> /dev/null
echo "[MMOCORE] character_quests.sql"
$MYG < ../sql/game/character_recipebook.sql &> /dev/null
echo "[MMOCORE] character_recipebook.sql"
$MYG < ../sql/game/character_shortcuts.sql &> /dev/null
echo "[MMOCORE] character_shortcuts.sql"
$MYG < ../sql/game/character_skills.sql &> /dev/null
echo "[MMOCORE] character_skills.sql"
$MYG < ../sql/game/character_skills_save.sql &> /dev/null
echo "[MMOCORE] character_skills_save.sql"
$MYG < ../sql/game/character_subclasses.sql &> /dev/null
echo "[MMOCORE] character_subclasses.sql"
$MYG < ../sql/game/character_variables.sql &> /dev/null
echo "[MMOCORE] character_variables.sql"
$MYG < ../sql/game/char_templates.sql &> /dev/null
echo "[MMOCORE] char_templates.sql"
$MYG < ../sql/game/clanhall.sql &> /dev/null
echo "[MMOCORE] clanhall.sql"
$MYG < ../sql/game/clan_data.sql &> /dev/null
echo "[MMOCORE] clan_data.sql"
$MYG < ../sql/game/clan_notices.sql &> /dev/null
echo "[MMOCORE] clan_notices.sql"
$MYG < ../sql/game/clan_privs.sql &> /dev/null
echo "[MMOCORE] clan_privs.sql"
$MYG < ../sql/game/clan_skills.sql &> /dev/null
echo "[MMOCORE] clan_skills.sql"
$MYG < ../sql/game/clan_subpledges.sql &> /dev/null
echo "[MMOCORE] clan_subpledges.sql"
$MYG < ../sql/game/clan_wars.sql &> /dev/null
echo "[MMOCORE] clan_wars.sql"
$MYG < ../sql/game/class_list.sql &> /dev/null
echo "[MMOCORE] class_list.sql"
$MYG < ../sql/game/couples.sql &> /dev/null
echo "[MMOCORE] couples.sql"
$MYG < ../sql/game/craftcount.sql &> /dev/null
echo "[MMOCORE] craftcount.sql"
$MYG < ../sql/game/cursed_weapons.sql &> /dev/null
echo "[MMOCORE] cursed_weapons.sql"
$MYG < ../sql/game/dropcount.sql &> /dev/null
echo "[MMOCORE] dropcount.sql"
$MYG < ../sql/game/droplist.sql &> /dev/null
echo "[MMOCORE] droplist.sql"
$MYG < ../sql/game/epic_boss_spawn.sql &> /dev/null
echo "[MMOCORE] epic_boss_spawn.sql"
$MYG < ../sql/game/etcitem.sql &> /dev/null
echo "[MMOCORE] etcitem.sql"
$MYG < ../sql/game/fake_pcs.sql &> /dev/null
echo "[MMOCORE] fake_pcs.sql"
$MYG < ../sql/game/fish.sql &> /dev/null
echo "[MMOCORE] fish.sql"
$MYG < ../sql/game/fishreward.sql &> /dev/null
echo "[MMOCORE] fishreward.sql"
$MYG < ../sql/game/forts.sql &> /dev/null
echo "[MMOCORE] forts.sql"
$MYG < ../sql/game/four_sepulchers_spawnlist.sql &> /dev/null
echo "[MMOCORE] four_sepulchers_spawnlist.sql"
$MYG < ../sql/game/games.sql &> /dev/null
echo "[MMOCORE] games.sql"
$MYG < ../sql/game/game_log.sql &> /dev/null
echo "[MMOCORE] game_log.sql"
$MYG < ../sql/game/global_tasks.sql &> /dev/null
echo "[MMOCORE] global_tasks.sql"
$MYG < ../sql/game/henna.sql &> /dev/null
echo "[MMOCORE] henna.sql"
$MYG < ../sql/game/henna_trees.sql &> /dev/null
echo "[MMOCORE] henna_trees.sql"
$MYG < ../sql/game/heroes.sql &> /dev/null
echo "[MMOCORE] heroes.sql"
$MYG < ../sql/game/items.sql &> /dev/null
echo "[MMOCORE] items.sql"
$MYG < ../sql/game/items_delayed.sql &> /dev/null
echo "[MMOCORE] items_delayed.sql"
$MYG < ../sql/game/item_attributes.sql &> /dev/null
echo "[MMOCORE] item_attributes.sql"
$MYG < ../sql/game/item_mall.sql &> /dev/null
echo "[MMOCORE] item_mall.sql"
$MYG < ../sql/game/killcount.sql &> /dev/null
echo "[MMOCORE] killcount.sql"
$MYG < ../sql/game/lastimperialtomb_spawnlist.sql &> /dev/null
echo "[MMOCORE] lastimperialtomb_spawnlist.sql"
$MYG < ../sql/game/locations.sql &> /dev/null
echo "[MMOCORE] locations.sql"
$MYG < ../sql/game/lvlupgain.sql &> /dev/null
echo "[MMOCORE] lvlupgain.sql"
$MYG < ../sql/game/mail.sql &> /dev/null
echo "[MMOCORE] mail.sql"
$MYG < ../sql/game/mapregion.sql &> /dev/null
echo "[MMOCORE] mapregion.sql"
$MYG < ../sql/game/merchant_areas_list.sql &> /dev/null
echo "[MMOCORE] merchant_areas_list.sql"
$MYG < ../sql/game/minions.sql &> /dev/null
echo "[MMOCORE] minions.sql"
$MYG < ../sql/game/npc.sql &> /dev/null
echo "[MMOCORE] npc.sql"
$MYG < ../sql/game/npcskills.sql &> /dev/null
echo "[MMOCORE] npcskills.sql"
$MYG < ../sql/game/olympiad_nobles.sql &> /dev/null
echo "[MMOCORE] olympiad_nobles.sql"
$MYG < ../sql/game/petitions.sql &> /dev/null
echo "[MMOCORE] petitions.sql"
$MYG < ../sql/game/pets.sql &> /dev/null
echo "[MMOCORE] pets.sql"
$MYG < ../sql/game/pets_skills.sql &> /dev/null
echo "[MMOCORE] pets_skills.sql"
$MYG < ../sql/game/pet_data.sql &> /dev/null
echo "[MMOCORE] pet_data.sql"
$MYG < ../sql/game/raidboss_points.sql &> /dev/null
echo "[MMOCORE] raidboss_points.sql"
$MYG < ../sql/game/raidboss_status.sql &> /dev/null
echo "[MMOCORE] raidboss_status.sql"
$MYG < ../sql/game/random_spawn.sql &> /dev/null
echo "[MMOCORE] random_spawn.sql"
$MYG < ../sql/game/random_spawn_loc.sql &> /dev/null
echo "[MMOCORE] random_spawn_loc.sql"
$MYG < ../sql/game/recipes.sql &> /dev/null
echo "[MMOCORE] recipes.sql"
$MYG < ../sql/game/recitems.sql &> /dev/null
echo "[MMOCORE] recitems.sql"
$MYG < ../sql/game/residence_functions.sql &> /dev/null
echo "[MMOCORE] residence_functions.sql"
$MYG < ../sql/game/server_variables.sql &> /dev/null
echo "[MMOCORE] server_variables.sql"
$MYG < ../sql/game/seven_signs.sql &> /dev/null
echo "[MMOCORE] seven_signs.sql"
$MYG < ../sql/game/seven_signs_festival.sql &> /dev/null
echo "[MMOCORE] seven_signs_festival.sql"
$MYG < ../sql/game/seven_signs_status.sql &> /dev/null
echo "[MMOCORE] seven_signs_status.sql"
$MYG < ../sql/game/siege_clans.sql &> /dev/null
echo "[MMOCORE] siege_clans.sql"
$MYG < ../sql/game/siege_door.sql &> /dev/null
echo "[MMOCORE] siege_door.sql"
$MYG < ../sql/game/siege_doorupgrade.sql &> /dev/null
echo "[MMOCORE] siege_doorupgrade.sql"
$MYG < ../sql/game/siege_guards.sql &> /dev/null
echo "[MMOCORE] siege_guards.sql"
$MYG < ../sql/game/siege_territory_members.sql &> /dev/null
echo "[MMOCORE] siege_territory_members.sql"
$MYG < ../sql/game/skills.sql &> /dev/null
echo "[MMOCORE] skills.sql"
$MYG < ../sql/game/skill_learn.sql &> /dev/null
echo "[MMOCORE] skill_learn.sql"
$MYG < ../sql/game/skill_spellbooks.sql &> /dev/null
echo "[MMOCORE] skill_spellbooks.sql"
$MYG < ../sql/game/skill_trees.sql &> /dev/null
echo "[MMOCORE] skill_trees.sql"
$MYG < ../sql/game/spawnlist.sql &> /dev/null
echo "[MMOCORE] spawnlist.sql"
$MYG < ../sql/game/tournament_class_list.sql &> /dev/null
echo "[MMOCORE] tournament_class_list.sql"
$MYG < ../sql/game/tournament_table.sql &> /dev/null
echo "[MMOCORE] tournament_table.sql"
$MYG < ../sql/game/tournament_teams.sql &> /dev/null
echo "[MMOCORE] tournament_teams.sql"
$MYG < ../sql/game/tournament_variables.sql &> /dev/null
echo "[MMOCORE] tournament_variables.sql"
$MYG < ../sql/game/weapon.sql &> /dev/null
echo "[MMOCORE] weapon.sql"
$MYG < ../sql/game/weapon_ex.sql &> /dev/null
echo "[MMOCORE] weapon_ex.sql"
echo ""
echo "**************************************************"
}
finish(){
exit 0
}
clear
load_config $1
MYL="$MYSQLPATH -h $LSDBHOST -u $LSUSER --password=$LSPASS -D $LSDB"
MYG="$MYSQLPATH -h $GSDBHOST -u $GSUSER --password=$GSPASS -D $GSDB"
welcome
