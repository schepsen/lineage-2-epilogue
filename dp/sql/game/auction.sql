CREATE TABLE `auction` (
  `id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `sellerId` int(11) NOT NULL DEFAULT '0',
  `sellerName` varchar(50) NOT NULL DEFAULT 'NPC',
  `sellerClanName` varchar(50) NOT NULL DEFAULT '',
  `itemName` varchar(40) NOT NULL DEFAULT '',
  `startingBid` bigint(20) NOT NULL DEFAULT '0',
  `currentBid` bigint(20) NOT NULL DEFAULT '0',
  `endDate` decimal(20,0) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

REPLACE INTO `auction` VALUES ('22', '0', 'NPC', 'NPC Clan', 'Moonstone Hall', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('23', '0', 'NPC', 'NPC Clan', 'Onyx Hall', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('24', '0', 'NPC', 'NPC Clan', 'Topaz Hall', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('25', '0', 'NPC', 'NPC Clan', 'Ruby Hall', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('26', '0', 'NPC', 'NPC Clan', 'Crystal Hall', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('27', '0', 'NPC', 'NPC Clan', 'Onyx Hall', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('28', '0', 'NPC', 'NPC Clan', 'Sapphire Hall', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('29', '0', 'NPC', 'NPC Clan', 'Moonstone Hall', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('30', '0', 'NPC', 'NPC Clan', 'Emerald Hall', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('31', '0', 'NPC', 'NPC Clan', 'The Atramental Barracks', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('32', '0', 'NPC', 'NPC Clan', 'The Scarlet Barracks', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('33', '0', 'NPC', 'NPC Clan', 'The Viridian Barracks', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('36', '0', 'NPC', 'NPC Clan', 'The Golden Chamber', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('37', '0', 'NPC', 'NPC Clan', 'The Silver Chamber', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('38', '0', 'NPC', 'NPC Clan', 'The Mithril Chamber', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('39', '0', 'NPC', 'NPC Clan', 'Silver Manor', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('40', '0', 'NPC', 'NPC Clan', 'Gold Manor', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('41', '0', 'NPC', 'NPC Clan', 'The Bronze Chamber', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('42', '0', 'NPC', 'NPC Clan', 'The Golden Chamber', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('43', '0', 'NPC', 'NPC Clan', 'The Silver Chamber', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('44', '0', 'NPC', 'NPC Clan', 'The Mithril Chamber', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('45', '0', 'NPC', 'NPC Clan', 'The Bronze Chamber', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('46', '0', 'NPC', 'NPC Clan', 'Silver Manor', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('47', '0', 'NPC', 'NPC Clan', 'Moonstone Hall', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('48', '0', 'NPC', 'NPC Clan', 'Onyx Hall', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('49', '0', 'NPC', 'NPC Clan', 'Emerald Hall', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('50', '0', 'NPC', 'NPC Clan', 'Sapphire Hall', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('51', '0', 'NPC', 'NPC Clan', 'Mont Chamber', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('52', '0', 'NPC', 'NPC Clan', 'Astaire Chamber', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('53', '0', 'NPC', 'NPC Clan', 'Aria Chamber', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('54', '0', 'NPC', 'NPC Clan', 'Yiana Chamber', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('55', '0', 'NPC', 'NPC Clan', 'Roien Chamber', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('56', '0', 'NPC', 'NPC Clan', 'Luna Chamber', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('57', '0', 'NPC', 'NPC Clan', 'Traban Chamber', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('58', '0', 'NPC', 'NPC Clan', 'Eisen Hall', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('59', '0', 'NPC', 'NPC Clan', 'Heavy Metal Hall', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('60', '0', 'NPC', 'NPC Clan', 'Molten Ore Hall', '2000000', '0', '0');
REPLACE INTO `auction` VALUES ('61', '0', 'NPC', 'NPC Clan', 'Titan Hall', '2000000', '0', '0');