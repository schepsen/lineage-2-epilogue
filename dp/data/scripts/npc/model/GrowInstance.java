package npc.model;

import l2p.gameserver.model.instances.L2NpcInstance;
import l2p.gameserver.templates.L2NpcTemplate;

/**
 * Данный инстанс используется NPC 13193 в локации Seed of Destruction
 *
 * @author z3r0ff
 */
public class GrowInstance extends L2NpcInstance
{
	public GrowInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}
}