package events_new;

import community.mGeneratePage;
import l2p.Config;
import l2p.extensions.scripts.Functions;
import l2p.gameserver.Announcements;
import l2p.gameserver.model.L2Character;
import l2p.gameserver.model.L2ObjectsStorage;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.model.Reflection;
import l2p.gameserver.model.entity.Hero;
import l2p.gameserver.model.instances.L2PetInstance;
import l2p.gameserver.modules.event.mEvent;
import l2p.gameserver.modules.event.mEventList;
import l2p.gameserver.modules.event.mEventPvP;
import l2p.gameserver.modules.event.mIEvent;
import l2p.gameserver.modules.option.mOption;
import l2p.util.Location;
import l2p.util.Rnd;

import java.util.ArrayList;
import java.util.concurrent.ScheduledFuture;

/**
 * User: Shaitan
 * Date: 15.02.11
 * Time: 9:37
 */
public class LastHero extends Functions implements mIEvent
{
	//--------------------------------------------------------------------------------------------------------------------------------------------
	private static boolean isRun = false;
	private static int time;
	private static int playersDie;
	private static ArrayList<L2Player> players;
	private static ArrayList<L2Player> playersWin;
	private static Location playerLoc;
	private static mEventPvP eventPvP = new mEventPvP();
	private static Reflection r;
	//--------------------------------------------------------------------------------------------------------------------------------------------
	private static ScheduledFuture stopBattle;
	//--------------------------------------------------------------------------------------------------------------------------------------------

	public void onLoad()
	{
		if(Config.eventLastHero)
		{
			mEventList.addEvent(this);
		}
	}

	//--------------------------------------------------------------------------------------------------------------------------------------------
	public void playerStartEvent()
	{
		L2Player player = getSelfPlayer();
		if(player == null)
		{
			return;
		}
		if(Config.eventEngine)
		{
			player.sendMessage("Для запуска эвентов вручную необходимо отключить эвентовый движек");
			return;
		}
		if(!Config.eventLastHero)
		{
			player.sendMessage("Эвент " + getName() + " выключен в конфигах");
		}
		if(isRun)
		{
			player.sendMessage("Эвент " + getName() + " уже запущен");
			return;
		}
		else
		{
			player.sendMessage("Вы запустили эвент " + getName() + ". Эвент начнется через пару минут.");
		}
		isRun = true;
		time = 0;
		executeTask("events_new.LastHero", "startReg", new Object[0], time += 3 * 60 * 1000);
		executeTask("events_new.LastHero", "stopReg", new Object[0], time += 5 * 60 * 1000);
	}

	@Override
	public void startEvent()
	{
		isRun = true;
		time = 0;
		executeTask("events_new.LastHero", "startReg", new Object[0], time += 3 * 60 * 1000);
		executeTask("events_new.LastHero", "stopReg", new Object[0], time += 5 * 60 * 1000);
	}

	@Override
	public String getStatus()
	{
		String s;
		if(eventPvP.isReg())
		{
			s = "<font color=00FF00>Регистрация активна</font>";
		}
		else
		{
			s = "<font color=FF0000>Регистрация не активна</font>";
		}
		return s;
	}

	@Override
	public String getName()
	{
		return LastHero.class.getSimpleName();
	}

	@Override
	public boolean isStat()
	{
		return false;
	}

	@Override
	public void getDescription()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
		{
			return;
		}
		StringBuilder sb = new StringBuilder();
		sb.append("<font color=\"LEVEL\">Last Hero</font><br>");
		sb.append("Суть эвента убить как можно больше противников и остаться в живых");
		show(sb.toString(), player, null);
	}

	@Override
	public void getStat()
	{
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------

	public static void startReg()
	{
		players = new ArrayList<L2Player>();
		eventPvP.setReg();
		mGeneratePage.generateEvent();
		Announcements.getInstance().announceToAll("Начался период регистрации на эвент " + LastHero.class.getSimpleName() + ". Зарегистрироваться на эвент можно в течении " + 5 + "ти минут в Community(Alt+B).");
		playersDie = 0;
	}

	public static void stopReg()
	{
		eventPvP.setNoActive();
		mGeneratePage.generateEvent();
		Announcements.getInstance().announceToAll("Закончился период регистрации на эвент Last Hero.");
		if(players.size() < Config.eventLastHeroMinPlayer)
		{
			Announcements.getInstance().announceToAll("Эвент Last Hero отменен т.к. зарегистрировалось " + players.size() + " человек. А нужно минимум " + Config.eventLastHeroMinPlayer + " человек.");
			isRun = false;
			return;
		}
		Announcements.getInstance().announceToAll("Через 30 секунд участники будут телепортированы на место проведения эвента.");
		time = 0;
		executeTask("events_new.LastHero", "teleport", new Object[0], time += 30 * 1000);
		executeTask("events_new.LastHero", "startBattle", new Object[0], time += 30 * 1000);
		stopBattle = executeTask("events_new.LastHero", "stopBattle", new Object[0], time += Config.eventLastHeroStopBattle * 60 * 1000);
	}

	public void reg()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
		{
			return;
		}
		else if(!eventPvP.isReg())
		{
			player.sendMessage("В данный момент регистрация не активна.");
			return;
		}
		else if(players.contains(player))
		{
			player.sendMessage("Вы уже зарегестрированы.");
			return;
		}
		else if(players.size() == Config.eventLastHeroMaxPlayer)
		{
			player.sendMessage("Эвент рассчитан на " + Config.eventLastHeroMaxPlayer + " человек. Свободных мест больше нету.");
			return;
		}
		else if(player.getLevel() < Config.eventLastHeroMinLvl || player.getLevel() > Config.eventLastHeroMaxLvl)
		{
			player.sendMessage("Не подходящий уровень, в эвенте могут участвовать только с " + Config.eventLastHeroMinLvl + " по " + Config.eventLastHeroMaxLvl + " уровни.");
			return;
		}
		else if(!mEvent.checkPlayer(player))
		{
			return;
		}
		players.add(player);
		player.sendMessage("Регистрация на эвент Last Hero прошла успешно.");
	}

	public static void teleport()
	{
		r = new Reflection(LastHero.class.getSimpleName());
		r.FillDoors(mEvent.doors);
		for(L2Player player : players)
		{
			mEvent.resurrectAndHeal(player);
			playerLoc = Rnd.coordsRandomize(149448, 46744, -3437, 0, 0, 1100);
			mEvent.teleportToLocation(player, playerLoc, true, r.getId());
			mEvent.paralyzePlayer(player);
			player.setTeam(2, false);
			player.sendMessage("Старт через 30 секунд !!!");
		}
	}

	public static void startBattle()
	{
		eventPvP.setActive();
		for(L2Player player : players)
		{
			player.sendMessage("Старт !!!");
			mEvent.unParalyzePlayer(player);
			if(Config.eventLastHeroDeleteAllBuff)
			{
				mEvent.deleteAllBuff(player);
			}
			player.setEvent(true);
		}
	}

	public static void stopBattle()
	{
		eventPvP.setNoActive();
		for(L2Player player : players)
		{
			if(!player.isDead() && player.isEvent())
			{
				playersWin.add(player);
			}
			mEvent.resurrectAndHeal(player);
			player.setTeam(0, false);
			player.setEvent(false);
			mEvent.unTeleportToLocation(player);
		}
		if(playersWin.size() == 1)
		{
			for(L2Player player : playersWin)
			{
				Announcements.getInstance().announceToAll("Последний герой: " + player.getName());
				mOption.addItem(player, Config.eventLastHeroMainRewardId, Config.eventLastHeroMainRewardCount);
				if(Config.eventLastHeroSetHero)
				{
					player.setHero(true);
					player.updatePledgeClass();
					Hero.addSkills(player);
				}
			}
		}
		isRun = false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------

	public static void OnDie(L2Character self, L2Character killer)
	{
		if(eventPvP.isActive() && self != null && self.getReflectionId() == r.getId() && self.isPlayer())
		{
			L2Player player = (L2Player) self;
			if(players.contains(player))
			{
				playersDie++;
				if(killer != null && killer.getReflectionId() == r.getId())
				{
					if(killer.isPet())
					{
						L2PetInstance pet = (L2PetInstance) killer;
						mOption.addItem(pet.getPlayer(), Config.eventLastHeroRewardId, Config.eventLastHeroRewardCount);
					}
					else if(killer.isPlayer())
					{
						L2Player playerKiller = (L2Player) killer;
						mOption.addItem(playerKiller, Config.eventLastHeroRewardId, Config.eventLastHeroRewardCount);
					}
				}
				checkEvent();
			}
		}
	}

	public static void OnPlayerExit(int objectId)
	{
		if(players == null)
		{
			return;
		}
		L2Player player = L2ObjectsStorage.getPlayer(objectId);
		if(player != null && players.contains(player))
		{
			players.remove(player);
		}
		else
		{
			return;
		}
		if(eventPvP.isActive())
		{
			mEvent.resurrectAndHeal(player);
			player.setTeam(0, false);
			player.setEvent(false);
			mEvent.unTeleportToLocation(player);
			checkEvent();
		}
	}

	public static Location OnEscape(L2Player player)
	{
		OnPlayerExit(player.getObjectId());
		return null;
	}

	private static void checkEvent()
	{
		if(playersDie == players.size() - 1)
		{
			if(stopBattle != null)
			{
				stopBattle.cancel(true);
				stopBattle = null;
			}
			stopBattle = executeTask("events_new.LastHero", "stopBattle", new Object[0], time = 10 * 1000);
		}
	}
}