package events_new;

import community.mGeneratePage;
import l2p.Config;
import l2p.extensions.scripts.Functions;
import l2p.gameserver.Announcements;
import l2p.gameserver.model.L2Character;
import l2p.gameserver.model.L2ObjectsStorage;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.model.Reflection;
import l2p.gameserver.model.instances.L2PetInstance;
import l2p.gameserver.modules.event.mEvent;
import l2p.gameserver.modules.event.mEventList;
import l2p.gameserver.modules.event.mEventPvP;
import l2p.gameserver.modules.event.mIEvent;
import l2p.gameserver.modules.option.mOption;
import l2p.util.Location;
import l2p.util.Rnd;

import java.util.ArrayList;
import java.util.concurrent.ScheduledFuture;

/**
 * User: Shaitan
 * Date: 15.02.11
 * Time: 9:42
 */
public class TvT extends Functions implements mIEvent
{
	//--------------------------------------------------------------------------------------------------------------------------------------------
	private static boolean isRun = false;
	private static int time;
	private static int teamBluePoints;
	private static int teamRedPoints;
	private static int teamBlueDie;
	private static int teamRedDie;
	private static ArrayList<L2Player> teamBlue;
	private static ArrayList<L2Player> teamRed;
	private static Location teamBlueLoc;
	private static Location teamRedLoc;
	private static mEventPvP eventPvP = new mEventPvP();
	private static Reflection r;
	//--------------------------------------------------------------------------------------------------------------------------------------------
	private static ScheduledFuture stopBattle;
	//--------------------------------------------------------------------------------------------------------------------------------------------

	public void onLoad()
	{
		if(Config.eventTvT)
		{
			mEventList.addEvent(this);
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------

	public void playerStartEvent()
	{
		L2Player player = getSelfPlayer();
		if(player == null)
		{
			return;
		}
		if(Config.eventEngine)
		{
			player.sendMessage("Для запуска эвентов вручную необходимо отключить эвентовый движек");
			return;
		}
		if(!Config.eventTvT)
		{
			player.sendMessage("Эвент " + getName() + " выключен в конфигах");
		}
		if(isRun)
		{
			player.sendMessage("Эвент " + getName() + " уже запущен");
			return;
		}
		else
		{
			player.sendMessage("Вы запустили эвент " + getName() + ". Эвент начнется через пару минут.");
		}
		isRun = true;
		time = 0;
		executeTask("events_new.TvT", "startReg", new Object[0], time += 3 * 60 * 1000);
		executeTask("events_new.TvT", "stopReg", new Object[0], time += 5 * 60 * 1000);
	}

	@Override
	public void startEvent()
	{
		isRun = true;
		time = 0;
		executeTask("events_new.TvT", "startReg", new Object[0], time += 3 * 60 * 1000);
		executeTask("events_new.TvT", "stopReg", new Object[0], time += 5 * 60 * 1000);
	}

	@Override
	public String getStatus()
	{
		String s;
		if(eventPvP.isReg())
		{
			s = "<font color=00FF00>Регистрация активна</font>";
		}
		else
		{
			s = "<font color=FF0000>Регистрация не активна</font>";
		}
		return s;
	}

	@Override
	public String getName()
	{
		return TvT.class.getSimpleName();
	}

	@Override
	public boolean isStat()
	{
		return false;
	}

	@Override
	public void getDescription()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
		{
			return;
		}
		StringBuilder sb = new StringBuilder();
		sb.append("<font color=\"LEVEL\">TvT</font><br>");
		sb.append("Суть эвента заработать как можно больше очков, эвент разделен на так называемые раунды т.е. две команды начинают сражаться, до тех пор пока игроки\n"
				  + " одной из команд полностью не умрут, как только это случается эвент не заканчивается, все игроки воскрешаются и начинают сражаются и так до того\n"
				  + " момента пока время эвента не истечет");
		show(sb.toString(), player, null);
	}

	@Override
	public void getStat()
	{
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------

	public static void startReg()
	{
		teamBlue = new ArrayList<L2Player>();
		teamRed = new ArrayList<L2Player>();
		eventPvP.setReg();
		mGeneratePage.generateEvent();
		Announcements.getInstance().announceToAll("Начался период регистрации на эвент " + TvT.class.getSimpleName() + ". Зарегистрироваться на эвент можно в течении " + 5 + "ти минут в Community(Alt+B).");
		executeTask("events_new.TvT", "announceReg", new Object[0], 1000);
		teamBluePoints = 0;
		teamRedPoints = 0;
		teamBlueDie = 0;
		teamRedDie = 0;
	}

	public static void stopReg()
	{
		eventPvP.setNoActive();
		mGeneratePage.generateEvent();
		Announcements.getInstance().announceToAll("Закончился период регистрации на эвент TvT.");
		if((teamBlue.size() + teamRed.size()) < Config.eventTvTMinPlayer)
		{
			Announcements.getInstance().announceToAll("Эвент TvT отменен т.к. зарегистрировалось " + (teamBlue.size() + teamRed.size()) + " человек. А нужно минимум " + Config.eventTvTMinPlayer + " человек.");
			isRun = false;
			return;
		}
		Announcements.getInstance().announceToAll("Через 30 секунд участники будут телепортированы на место проведения эвента.");
		time = 0;
		executeTask("events_new.TvT", "teleport", new Object[0], time += 30 * 1000);
		executeTask("events_new.TvT", "startBattle", new Object[0], time += 30 * 1000);
		stopBattle = executeTask("events_new.TvT", "stopBattle", new Object[0], time += Config.eventTvTStopBattle * 60 * 1000);
	}

	public void reg()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
		{
			return;
		}
		else if(!eventPvP.isReg())
		{
			player.sendMessage("В данный момент регистрация не активна.");
			return;
		}
		else if(teamBlue.contains(player) || teamRed.contains(player))
		{
			player.sendMessage("Вы уже зарегестрированы.");
			return;
		}
		else if(teamBlue.size() + teamRed.size() == Config.eventTvTMaxPlayer)
		{
			player.sendMessage("Эвент рассчитан на " + Config.eventTvTMaxPlayer + " человек. Свободных мест больше нету.");
			return;
		}
		else if(player.getLevel() < Config.eventTvTMinLvl || player.getLevel() > Config.eventTvTMaxLvl)
		{
			player.sendMessage("Не подходящий уровень, в эвенте могут участвовать только с " + Config.eventTvTMinLvl + " по " + Config.eventTvTMaxLvl + " уровни.");
			return;
		}
		else if(!mEvent.checkPlayer(player))
		{
			return;
		}
		int team;
		if(teamBlue.size() > teamRed.size())
		{
			team = 2;
		}
		else if(teamBlue.size() < teamRed.size())
		{
			team = 1;
		}
		else
		{
			team = Rnd.get(1, 2);
		}
		switch(team)
		{
			case 1:
				teamBlue.add(player);
				break;
			case 2:
				teamRed.add(player);
				break;
		}
		player.sendMessage("Регистрация на эвент TvT прошла успешно.");
	}

	public static void teleport()
	{
		r = new Reflection(TvT.class.getSimpleName());
		r.FillDoors(mEvent.doors);
		for(L2Player player : teamBlue)
		{
			mEvent.resurrectAndHeal(player);
			teamBlueLoc = Rnd.coordsRandomize(148552, 46179, -3412, 0, 0, 350);
			mEvent.teleportToLocation(player, teamBlueLoc, true, r.getId());
			mEvent.paralyzePlayer(player);
			player.setTeam(1, false);
			player.sendMessage("Старт через 30 секунд !!!");
		}
		for(L2Player player : teamRed)
		{
			mEvent.resurrectAndHeal(player);
			teamRedLoc = Rnd.coordsRandomize(150412, 47392, -3412, 0, 0, 280);
			mEvent.teleportToLocation(player, teamRedLoc, true, r.getId());
			mEvent.paralyzePlayer(player);
			player.setTeam(2, false);
			player.sendMessage("Старт через 30 секунд !!!");
		}
	}

	public static void startBattle()
	{
		eventPvP.setActive();
		for(L2Player player : teamBlue)
		{
			player.sendMessage("Старт !!!");
			mEvent.unParalyzePlayer(player);
			if(Config.eventTvTDeleteAllBuff)
			{
				mEvent.deleteAllBuff(player);
			}
			player.setEvent(true);
		}
		for(L2Player player : teamRed)
		{
			player.sendMessage("Старт !!!");
			mEvent.unParalyzePlayer(player);
			if(Config.eventTvTDeleteAllBuff)
			{
				mEvent.deleteAllBuff(player);
			}
			player.setEvent(true);
		}
	}

	public static void stopBattle()
	{
		eventPvP.setNoActive();
		Announcements.getInstance().announceToAll("Кол-во очков у синих: " + teamBluePoints);
		Announcements.getInstance().announceToAll("Кол-во очков у красных: " + teamRedPoints);
		if(teamBluePoints > teamRedPoints)
		{
			Announcements.getInstance().announceToAll("Побеждает команда синих");
			for(L2Player player : teamBlue)
			{
				mOption.addItem(player, Config.eventTvTRewardId, Config.eventTvTRewardCount);
			}
		}
		else if(teamBluePoints < teamRedPoints)
		{
			Announcements.getInstance().announceToAll("Побеждает команда красных");
			for(L2Player player : teamRed)
			{
				mOption.addItem(player, Config.eventTvTRewardId, Config.eventTvTRewardCount);
			}
		}
		else if(teamBluePoints == teamRedPoints)
		{
			Announcements.getInstance().announceToAll("Ничья");
			for(L2Player player : teamBlue)
			{
				mOption.addItem(player, Config.eventTvTRewardId, Config.eventTvTRewardCount);
			}
			for(L2Player player : teamRed)
			{
				mOption.addItem(player, Config.eventTvTRewardId, Config.eventTvTRewardCount);
			}
		}
		for(L2Player player : teamBlue)
		{
			mEvent.resurrectAndHeal(player);
			player.setTeam(0, false);
			player.setEvent(false);
			mEvent.unTeleportToLocation(player);
		}
		for(L2Player player : teamRed)
		{
			mEvent.resurrectAndHeal(player);
			player.setTeam(0, false);
			player.setEvent(false);
			mEvent.unTeleportToLocation(player);
		}
		isRun = false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------

	public static void OnDie(L2Character self, L2Character killer)
	{
		if(eventPvP.isActive() && self != null && self.getReflectionId() == r.getId() && self.isPlayer())
		{
			L2Player player = (L2Player) self;
			if(teamBlue.contains(player) || teamRed.contains(player))
			{
				switch(player.getTeam())
				{
					case 1:
						teamBlueDie++;
						break;
					case 2:
						teamRedDie++;
						break;
				}
				int killerTeam = 0;
				if(killer.isPet())
				{
					L2PetInstance pet = (L2PetInstance) killer;
					killerTeam = pet.getPlayer().getTeam();
				}
				else if(killer.isPlayer())
				{
					L2Player playerKiller = (L2Player) killer;
					killerTeam = playerKiller.getTeam();
				}
				if(player.getTeam() == 1 && killerTeam == 2)
				{
					teamRedPoints++;
				}
				else if(player.getTeam() == 2 && killerTeam == 1)
				{
					teamBluePoints++;
				}
				checkEvent();
			}
		}
	}

	public static void OnPlayerExit(int objectId)
	{
		if(teamBlue == null || teamRed == null)
		{
			return;
		}
		L2Player player = L2ObjectsStorage.getPlayer(objectId);
		if(player == null)
		{
			return;
		}
		else if(teamBlue.contains(player))
		{
			teamBlue.remove(player);
		}
		else if(teamRed.contains(player))
		{
			teamRed.remove(player);
		}
		else
		{
			return;
		}
		if(eventPvP.isActive())
		{
			mEvent.resurrectAndHeal(player);
			player.setTeam(0, false);
			player.setEvent(false);
			mEvent.unTeleportToLocation(player);
			checkEvent();
		}
	}

	public static Location OnEscape(L2Player player)
	{
		OnPlayerExit(player.getObjectId());
		return null;
	}

	private static void checkEvent()
	{
		if(teamBlue.isEmpty() || teamRed.isEmpty())
		{
			if(stopBattle != null)
			{
				stopBattle.cancel(true);
				stopBattle = null;
			}
			stopBattle = executeTask("events_new.TvT", "stopBattle", new Object[0], time = 10 * 1000);
		}
		else if(teamBlue.size() == teamBlueDie || teamRed.size() == teamRedDie)
		{
			teamBlueDie = 0;
			teamRedDie = 0;
			for(L2Player player : teamBlue)
			{
				mEvent.resurrectAndHeal(player);
				if(Config.eventTvTDeleteAllBuff)
				{
					mEvent.deleteAllBuff(player);
				}
				teamBlueLoc = Rnd.coordsRandomize(148552, 46179, -3412, 0, 0, 350);
				mEvent.teleportToLocation(player, teamBlueLoc, false, r.getId());
			}
			for(L2Player player : teamRed)
			{
				mEvent.resurrectAndHeal(player);
				if(Config.eventTvTDeleteAllBuff)
				{
					mEvent.deleteAllBuff(player);
				}
				teamRedLoc = Rnd.coordsRandomize(150412, 47392, -3412, 0, 0, 280);
				mEvent.teleportToLocation(player, teamRedLoc, false, r.getId());
			}
		}
	}
}