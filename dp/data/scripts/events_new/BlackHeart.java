package events_new;

import community.mGeneratePage;
import javolution.util.FastMap;
import l2p.Config;
import l2p.database.DatabaseUtils;
import l2p.database.FiltredPreparedStatement;
import l2p.database.L2DatabaseFactory;
import l2p.database.ThreadConnection;
import l2p.database.mysql;
import l2p.extensions.scripts.Functions;
import l2p.gameserver.Announcements;
import l2p.gameserver.model.L2Character;
import l2p.gameserver.model.L2ObjectsStorage;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.model.Reflection;
import l2p.gameserver.model.entity.Hero;
import l2p.gameserver.model.entity.olympiad.Olympiad;
import l2p.gameserver.model.instances.L2PetInstance;
import l2p.gameserver.modules.event.mEvent;
import l2p.gameserver.modules.event.mEventList;
import l2p.gameserver.modules.event.mEventPvP;
import l2p.gameserver.modules.event.mIEvent;
import l2p.gameserver.modules.option.mOption;
import l2p.gameserver.serverpackets.ExHeroList;
import l2p.gameserver.templates.StatsSet;
import l2p.util.Location;
import l2p.util.Rnd;
import l2p.util.Util;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;

/**
 * User: Shaitan
 * Date: 15.02.11
 * Time: 9:37
 */
public class BlackHeart extends Functions implements mIEvent
{
	//--------------------------------------------------------------------------------------------------------------------------------------------
	private static boolean isRun = false;
	private static int time;
	private static int playersDie;
	private static ArrayList<L2Player> players;
	private static Location playerLoc;
	private static mEventPvP eventPvP = new mEventPvP();
	private static Reflection r;
	//--------------------------------------------------------------------------------------------------------------------------------------------
	private static ScheduledFuture stopBattle;
	//--------------------------------------------------------------------------------------------------------------------------------------------

	public void onLoad()
	{
		if(Config.eventBlackHeart)
		{
			mEventList.addEvent(this);
		}
	}

	//--------------------------------------------------------------------------------------------------------------------------------------------
	public void playerStartEvent()
	{
		L2Player player = getSelfPlayer();
		if(player == null)
		{
			return;
		}
		if(Config.eventEngine)
		{
			player.sendMessage("Для запуска эвентов вручную необходимо отключить эвентовый движек");
			return;
		}
		if(!Config.eventBlackHeart)
		{
			player.sendMessage("Эвент " + getName() + " выключен в конфигах");
		}
		if(isRun)
		{
			player.sendMessage("Эвент " + getName() + " уже запущен");
			return;
		}
		else
		{
			player.sendMessage("Вы запустили эвент " + getName() + ". Эвент начнется через пару минут.");
		}
		isRun = true;
		time = 0;
		executeTask("events_new.BlackHeart", "startReg", new Object[0], time += 3 * 60 * 1000);
		executeTask("events_new.BlackHeart", "stopReg", new Object[0], time += 5 * 60 * 1000);
	}

	@Override
	public void startEvent()
	{
		isRun = true;
		time = 0;
		executeTask("events_new.BlackHeart", "startReg", new Object[0], time += 3 * 60 * 1000);
		executeTask("events_new.BlackHeart", "stopReg", new Object[0], time += 5 * 60 * 1000);
	}

	@Override
	public String getStatus()
	{
		String s;
		if(eventPvP.isReg())
		{
			s = "<font color=00FF00>Регистрация активна</font>";
		}
		else
		{
			s = "<font color=FF0000>Регистрация не активна</font>";
		}
		return s;
	}

	@Override
	public String getName()
	{
		return BlackHeart.class.getSimpleName();
	}

	@Override
	public boolean isStat()
	{
		return true;
	}

	@Override
	public void getDescription()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
		{
			return;
		}
		StringBuilder sb = new StringBuilder();
		sb.append("<font color=\"LEVEL\">Black Heart</font><br>");
		sb.append("Суть эвента...");
		show(sb.toString(), player, null);
	}

	@Override
	public void getStat()
	{
		Map<Integer, StatsSet> _heroes = new FastMap<Integer, StatsSet>();
		ThreadConnection tc = null;
		FiltredPreparedStatement fps = null;
		ResultSet rs = null;
		try
		{
			tc = L2DatabaseFactory.getInstance().getConnection();
			fps = tc.prepareStatement("SELECT obj_id, value FROM character_variables WHERE `name` = 'BlackHeart_Count' ORDER BY CAST(`value` AS DECIMAL) DESC LIMIT 15");
			rs = fps.executeQuery();
			while(rs.next())
			{
				int id = rs.getInt("obj_id");
				StatsSet hero = new StatsSet();
				hero.set(Olympiad.CHAR_NAME, Util.getCharNameByIdD(id));
				hero.set(Olympiad.CLASS_ID, mysql.get("SELECT class_id FROM character_subclasses WHERE char_obj_id = " + id));
				hero.set(Hero.COUNT, rs.getInt("value"));
				Hero.HeroSetClanAndAlly(id, hero);
				_heroes.put(id, hero);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DatabaseUtils.closeDatabaseCSR(tc, fps, rs);
		}
		L2Player player = (L2Player) getSelf();
		if(player == null)
		{
			return;
		}
		player.sendPacket(new ExHeroList(_heroes));
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------

	public static void startReg()
	{
		players = new ArrayList<L2Player>();
		eventPvP.setReg();
		mGeneratePage.generateEvent();
		Announcements.getInstance().announceToAll("Начался период регистрации на эвент " + BlackHeart.class.getSimpleName() + ". Зарегистрироваться на эвент можно в течении " + 5 + "ти минут в Community(Alt+B).");
		playersDie = 0;
	}

	public static void stopReg()
	{
		eventPvP.setNoActive();
		mGeneratePage.generateEvent();
		Announcements.getInstance().announceToAll("Закончился период регистрации на эвент Last Hero.");
		if(players.size() < Config.eventBlackHeartMinPlayer)
		{
			Announcements.getInstance().announceToAll("Эвент Last Hero отменен т.к. зарегистрировалось " + players.size() + " человек. А нужно минимум " + Config.eventBlackHeartMinPlayer + " человек.");
			isRun = false;
			return;
		}
		Announcements.getInstance().announceToAll("Через 30 секунд участники будут телепортированы на место проведения эвента.");
		time = 0;
		executeTask("events_new.BlackHeart", "teleport", new Object[0], time += 30 * 1000);
		executeTask("events_new.BlackHeart", "startBattle", new Object[0], time += 30 * 1000);
		stopBattle = executeTask("events_new.BlackHeart", "stopBattle", new Object[0], time += Config.eventBlackHeartStopBattle * 60 * 1000);
	}

	public void reg()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
		{
			return;
		}
		else if(!eventPvP.isReg())
		{
			player.sendMessage("В данный момент регистрация не активна.");
			return;
		}
		else if(players.contains(player))
		{
			player.sendMessage("Вы уже зарегестрированы.");
			return;
		}
		else if(players.size() == Config.eventBlackHeartMaxPlayer)
		{
			player.sendMessage("Эвент рассчитан на " + Config.eventBlackHeartMaxPlayer + " человек. Свободных мест больше нету.");
			return;
		}
		else if(player.getLevel() < Config.eventBlackHeartMinLvl || player.getLevel() > Config.eventBlackHeartMaxLvl)
		{
			player.sendMessage("Не подходящий уровень, в эвенте могут участвовать только с " + Config.eventBlackHeartMinLvl + " по " + Config.eventBlackHeartMaxLvl + " уровни.");
			return;
		}
		else if(!mEvent.checkPlayer(player))
		{
			return;
		}
		players.add(player);
		player.sendMessage("Регистрация на эвент Last Hero прошла успешно.");
	}

	public static void teleport()
	{
		r = new Reflection(BlackHeart.class.getSimpleName());
		r.FillDoors(mEvent.doors);
		for(L2Player player : players)
		{
			mEvent.resurrectAndHeal(player);
			playerLoc = Rnd.coordsRandomize(149448, 46744, -3437, 0, 0, 1100);
			mEvent.teleportToLocation(player, playerLoc, true, r.getId());
			mEvent.paralyzePlayer(player);
			player.setTeam(2, false);
			player.sendMessage("Старт через 30 секунд !!!");
		}
	}

	public static void startBattle()
	{
		eventPvP.setActive();
		for(L2Player player : players)
		{
			player.sendMessage("Старт !!!");
			if(player.getVar("BlackHeart_Count") == null)
			{
				player.setVar("BlackHeart_Count", "10");
			}
			mEvent.unParalyzePlayer(player);
			if(Config.eventBlackHeartDeleteAllBuff)
			{
				mEvent.deleteAllBuff(player);
			}
			player.setEvent(true);
		}
	}

	public static void stopBattle()
	{
		eventPvP.setNoActive();
		for(L2Player player : players)
		{
			if(playersDie == players.size() - 1 && !player.isDead())
			{
				Announcements.getInstance().announceToAll("Последний герой: " + player.getName());
				mOption.addItem(player, Config.eventBlackHeartMainRewardId, Config.eventBlackHeartMainRewardCount);
				if(Config.eventBlackHeartSetHero)
				{
					player.setHero(true);
					player.updatePledgeClass();
					Hero.addSkills(player);
				}
			}
			mEvent.resurrectAndHeal(player);
			player.setTeam(0, false);
			player.setEvent(false);
			mEvent.unTeleportToLocation(player);
		}
		isRun = false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------

	public static void OnDie(L2Character self, L2Character killer)
	{
		if(eventPvP.isActive() && self != null && self.getReflectionId() == r.getId() && self.isPlayer())
		{
			L2Player player = (L2Player) self;
			L2Player pk = null;
			if(killer != null)
			{
				if(killer.isPet())
				{
					L2PetInstance pet = (L2PetInstance) killer;
					pk = pet.getPlayer();
				}
				else if(killer.isPlayer())
				{
					pk = (L2Player) killer;
				}
			}
			if(players.contains(player))
			{
				playersDie++;
				if(killer != null && killer.getReflectionId() == r.getId())
				{
					mOption.addItem(pk, Config.eventBlackHeartRewardId, Config.eventBlackHeartRewardCount);
				}
				if(pk.getVar("BlackHeart_Count") != null && player.getVar("BlackHeart_Count") != null)
				{
					int CountPK;
					CountPK = Integer.parseInt(pk.getVar("BlackHeart_Count")) + 1;
					String sPK = "" + CountPK;
					pk.setVar("BlackHeart_Count", sPK);
					//----------------------------------------------------------------------
					int Count;
					int DelCount;
					Count = Integer.parseInt(player.getVar("BlackHeart_Count"));
					DelCount = Count >= 10 ? Math.round(Count / 10) : 1;
					Count = Integer.parseInt(player.getVar("BlackHeart_Count")) - DelCount;
					player.setVar("BlackHeart_Count", Count + "");
				}
				checkEvent();
			}
		}
	}

	public static void OnPlayerExit(int objectId)
	{
		if(players == null)
		{
			return;
		}
		L2Player player = L2ObjectsStorage.getPlayer(objectId);
		if(player != null && players.contains(player))
		{
			players.remove(player);
		}
		else
		{
			return;
		}
		if(eventPvP.isActive())
		{
			mEvent.resurrectAndHeal(player);
			player.setTeam(0, false);
			player.setEvent(false);
			mEvent.unTeleportToLocation(player);
			checkEvent();
		}
	}

	public static Location OnEscape(L2Player player)
	{
		OnPlayerExit(player.getObjectId());
		return null;
	}

	private static void checkEvent()
	{
		if(playersDie == players.size() - 1)
		{
			if(stopBattle != null)
			{
				stopBattle.cancel(true);
				stopBattle = null;
			}
			stopBattle = executeTask("events_new.BlackHeart", "stopBattle", new Object[0], time = 10 * 1000);
		}
	}
}