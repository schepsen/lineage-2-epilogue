import community.mGeneratePage;
import l2p.Config;

/**
 * User: Shaitan
 * Date: 20.04.11
 * Time: 13:08
 */
public class EndStart
{
	public void onLoad()
	{
		if(Config.communityEnabled)
		{
			mGeneratePage.getInstance();
		}
	}
}