package services;

import l2p.Config;
import l2p.extensions.scripts.Functions;
import l2p.extensions.scripts.ScriptFile;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.model.entity.Hero;
import l2p.gameserver.modules.community.mGenerateElement;
import l2p.gameserver.modules.option.mOption;

/**
 * User: Shaitan
 * Date: 20.03.11
 * Time: 10:17
 */
public class HeroService extends Functions implements ScriptFile
{
	public void hero_list()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
		{
			return;
		}
		String append = "Сервис продажи геройства:";
		append += "<br>";
		append += "<font color=\"LEVEL\">Купить статус героя за " + Config.SellHeroPriceCount + " " + mOption.getItemName(Config.SellHeroPriceId) + "</font>";
		append += mGenerateElement.button("Купить", "scripts_services.HeroService:sethero", 200, 25);
		show(append, player);
	}

	public void sethero()
	{
		L2Player player = (L2Player) getSelf();
		if(!player.isHero() && mOption.price(player, Config.SellHeroPriceId, Config.SellHeroPriceCount))
		{
			Hero.addHeroes(player.getObjectId());
			Hero.getInstance().activateHero(player);
		}
	}

	@Override
	public void onLoad()
	{
	}

	@Override
	public void onReload()
	{
	}

	@Override
	public void onShutdown()
	{
	}
}