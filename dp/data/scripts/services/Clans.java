package services;

import l2p.Config;
import l2p.extensions.scripts.Functions;
import l2p.extensions.scripts.ScriptFile;
import l2p.gameserver.cache.Msg;
import l2p.gameserver.instancemanager.CastleSiegeManager;
import l2p.gameserver.instancemanager.SiegeManager;
import l2p.gameserver.model.L2Clan;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.model.L2World;
import l2p.gameserver.modules.community.mGenerateElement;
import l2p.gameserver.modules.option.mOption;
import l2p.gameserver.serverpackets.PledgeInfo;
import l2p.gameserver.serverpackets.PledgeShowInfoUpdate;
import l2p.gameserver.serverpackets.PledgeStatusChanged;
import l2p.gameserver.tables.ClanTable;
import l2p.util.Util;

import java.util.HashMap;

/**
 * User: Shaitan
 * Date: 22.02.11
 * Time: 8:54
 */
public class Clans extends Functions implements ScriptFile
{
	private class AllowedLvl
	{
		int lvl;
		int id;
		int count;
	}

	private static HashMap<Integer, AllowedLvl> allowedLvls = new HashMap<Integer, AllowedLvl>();

	public void list()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
		{
			return;
		}
		String s = "";
		if(Config.ClanChangeName)
		{
			s += "<font color=\"LEVEL\">Сервис смены имени клана.</font><br1>";
			s += "Цена: " + Config.ClanChangeNameCount + " " + mOption.getItemName(Config.ClanChangeNameId) + "<br1>";
			s += "Введите желаемой имя клана: <edit var=\"name\" width=270 height=15 /><br1>";
			s += mGenerateElement.button("Сменить имя клана", "scripts_services.Clans:changeClanName $name", 270, 20) + "<br1>";
		}
		if(Config.ClanPointSell)
		{
			s += "<font color=\"LEVEL\">Сервис покупки клановых очей.</font><br1>";
			s += mGenerateElement.button("Купить " + Config.ClanPointCount + " клановых очей за " + Config.ClanPointPriceCount + " " + mOption.getItemName(Config.ClanPointPriceId), "scripts_services.Clans:buyClanPoints", 270, 20) + "<br1>";
		}
		if(Config.ClanLevelSell)
		{
			s += "<font color=\"LEVEL\">Сервис покупки уровня клана.</font><br1>";
			s += "Доступные для продажи лвла:<br1>";
			for(AllowedLvl allowedLvl : allowedLvls.values())
			{
				if(allowedLvl.lvl > 0 || allowedLvl.lvl < 12)
				{
					s += mGenerateElement.button("Купить " + allowedLvl.lvl + "й уровень клана за " + allowedLvl.count + " " + mOption.getItemName(allowedLvl.id), "scripts_services.Clans:buyClanLevel " + allowedLvl.lvl, 270, 20) + "<br1>";
				}
			}
		}
		show(s, player, null);
	}

	public void changeClanName(String[] args)
	{
		L2Player player = (L2Player) getSelf();
		if(player.getClan() == null)
		{
			player.sendMessage("У вас нет клана.");
			return;
		}
		if(!Util.isMatchingRegexp(args[0], Config.CLAN_NAME_TEMPLATE))
		{
			player.sendPacket(Msg.CLAN_NAME_IS_INCORRECT);
			list();
			return;
		}
		if(ClanTable.getInstance().getClanByName(args[0]) != null)
		{
			player.sendMessage("Такое имя клана уже занято.");
			list();
			return;
		}
		if(mOption.price(player, Config.ClanChangeNameId, Config.ClanChangeNameCount))
		{
			L2Clan clan = player.getClan();
			clan.setName(args[0]);
			clan.updateClanInDB();
			clan.broadcastClanStatus(true, true, true);
			clan.broadcastToOnlineMembers(new PledgeInfo(clan));
			for(L2Player ch : L2World.getAroundPlayers(player))
			{
				if(ch != null && ch.getClan() != clan)
				{
					ch.sendPacket(new PledgeInfo(clan));
				}
			}
		}
		list();
	}

	public void buyClanPoints()
	{
		L2Player player = (L2Player) getSelf();
		if(player.getClan() == null || player.getClan().getLevel() < 5)
		{
			player.sendMessage("У вас нет клана или ваш клан ниже 5го уровня.");
			list();
			return;
		}
		else if(mOption.price(player, Config.ClanPointPriceId, Config.ClanPointPriceCount))
		{
			player.getClan().incReputation(Config.ClanPointCount, false, "ClanPoint");
		}
		list();
	}

	public void buyClanLevel(String[] args)
	{
		L2Player player = (L2Player) getSelf();
		if(args[0].length() > 2)
		{
			player.sendMessage("Не верно указанный уровень.");
			list();
			return;
		}
		int level;
		try
		{
			level = Integer.parseInt(args[0]);
			if(level < 1 || level > 11)
			{
				player.sendMessage("Не верно указанный уровень.");
				list();
				return;
			}
		}
		catch(Exception e)
		{
			player.sendMessage("Не верно указанный уровень.");
			list();
			return;
		}
		if(player.getClan() == null)
		{
			player.sendMessage("У вас нет клана.");
			return;
		}
		else if(player.getClan().getLevel() != level - 1)
		{
			player.sendMessage("Для покупки " + level + " уровня клана ваш клан должен быть " + (level - 1) + " уровня.");
			list();
			return;
		}
		AllowedLvl allowedLvl = allowedLvls.get(level);
		if(allowedLvl != null)
		{
			if(mOption.price(player, allowedLvl.id, allowedLvl.count))
			{
				L2Clan clan = player.getClan();
				clan.setLevel(Byte.parseByte(allowedLvl.lvl + ""));
				clan.updateClanInDB();
				if(level < CastleSiegeManager.getSiegeClanMinLevel())
				{
					SiegeManager.removeSiegeSkills(player);
				}
				else
				{
					SiegeManager.addSiegeSkills(player);
				}
				if(level == 5)
				{
					player.sendPacket(Msg.NOW_THAT_YOUR_CLAN_LEVEL_IS_ABOVE_LEVEL_5_IT_CAN_ACCUMULATE_CLAN_REPUTATION_POINTS);
				}
				PledgeShowInfoUpdate pu = new PledgeShowInfoUpdate(clan);
				PledgeStatusChanged ps = new PledgeStatusChanged(clan);
				for(L2Player member : clan.getOnlineMembers(0))
				{
					member.updatePledgeClass();
					member.sendPacket(Msg.CLANS_SKILL_LEVEL_HAS_INCREASED, pu, ps);
					member.broadcastUserInfo(true);
				}
			}
		}
		else
		{
			player.sendMessage(level + "й уровень клана не продается.");
		}
		list();
	}

	public void onLoad()
	{
		if(Config.ClanLevelSell)
		{
			for(String lvls : Config.ClanLevelPrice)
			{
				String[] lvl = lvls.split(",");
				AllowedLvl allowedLvl = new AllowedLvl();
				allowedLvl.lvl = Integer.parseInt(lvl[0]);
				allowedLvl.id = Integer.parseInt(lvl[1]);
				allowedLvl.count = Integer.parseInt(lvl[2]);
				allowedLvls.put(allowedLvl.lvl, allowedLvl);
			}
		}
	}

	public void onReload()
	{
		allowedLvls = new HashMap<Integer, AllowedLvl>();
		onLoad();
	}

	public void onShutdown()
	{
	}
}