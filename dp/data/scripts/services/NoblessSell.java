package services;

import l2p.Config;
import l2p.extensions.multilang.CustomMessage;
import l2p.extensions.scripts.Functions;
import l2p.extensions.scripts.ScriptFile;
import l2p.gameserver.cache.Msg;
import l2p.gameserver.instancemanager.QuestManager;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.model.base.Race;
import l2p.gameserver.model.entity.olympiad.Olympiad;
import l2p.gameserver.model.instances.L2NpcInstance;
import l2p.gameserver.model.instances.L2TerritoryManagerInstance;
import l2p.gameserver.model.items.L2ItemInstance;
import l2p.gameserver.model.quest.Quest;
import l2p.gameserver.model.quest.QuestState;
import l2p.gameserver.modules.community.mGenerateElement;
import l2p.gameserver.modules.option.mOption;
import l2p.gameserver.serverpackets.SkillList;
import l2p.gameserver.serverpackets.SocialAction;
import l2p.util.Files;

public class NoblessSell extends Functions implements ScriptFile
{
	public void list()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
		{
			return;
		}
		StringBuilder sb = new StringBuilder();
		sb.append("<font color=\"LEVEL\">Сервис продажи дворянства</font><br>");
		sb.append("Цена: " + Config.SellNoblePriceCount + " " + mOption.getItemName(Config.SellNoblePriceId) + "<br1>");
		sb.append(mGenerateElement.button("Стать дворянином", "scripts_services.NoblessSell:buyNoble", 270, 20) + "<br1>");
		show(sb.toString(), player, null);
	}

	public void buyNoble()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
		{
			return;
		}
		if(player.isNoble())
		{
			player.sendMessage("Вы уже дворянин");
			return;
		}
		if(mOption.price(player, Config.SellNoblePriceId, Config.SellNoblePriceCount))
		{
			makeSubQuests();
			becomeNoble();
		}
		list();
	}

	public void getTW()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null || player.isNoble())
		{
			return;
		}
		L2NpcInstance npc = this.getNpc();
		if(npc == null || !(npc instanceof L2TerritoryManagerInstance))
		{
			return;
		}
		int terr = npc.getNpcId() - 36489;
		if(terr > 9 || terr < 1)
		{
			return;
		}
		int territoryBadgeId = 13756 + terr;
		L2ItemInstance pay = player.getInventory().getItemByItemId(territoryBadgeId);
		if(pay != null && pay.getCount() >= 100)
		{
			player.getInventory().destroyItem(pay, 100, true);
			makeSubQuests();
			becomeNoble();
		}
		else
		{
			player.sendPacket(Msg.INCORRECT_ITEM_COUNT);
		}
	}

	public void makeSubQuests()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
		{
			return;
		}
		Quest q = QuestManager.getQuest("_234_FatesWhisper");
		QuestState qs = player.getQuestState(q.getClass());
		if(qs != null)
		{
			qs.exitCurrentQuest(true);
		}
		q.newQuestState(player, Quest.COMPLETED);
		if(player.getRace() == Race.kamael)
		{
			q = QuestManager.getQuest("_236_SeedsOfChaos");
			qs = player.getQuestState(q.getClass());
			if(qs != null)
			{
				qs.exitCurrentQuest(true);
			}
			q.newQuestState(player, Quest.COMPLETED);
		}
		else
		{
			q = QuestManager.getQuest("_235_MimirsElixir");
			qs = player.getQuestState(q.getClass());
			if(qs != null)
			{
				qs.exitCurrentQuest(true);
			}
			q.newQuestState(player, Quest.COMPLETED);
		}
	}

	public void becomeNoble()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null || player.isNoble())
		{
			return;
		}
		Olympiad.addNoble(player);
		player.setNoble(true);
		player.updatePledgeClass();
		player.updateNobleSkills();
		player.sendPacket(new SkillList(player));
		player.broadcastPacket(new SocialAction(player.getObjectId(), SocialAction.VICTORY));
		player.broadcastUserInfo(true);
		player.sendMessage("Поздравляем, вы стали дворянином");
	}

	public void dialogTW()
	{
		L2Player player = (L2Player) getSelf();
		if(Config.SERVICES_NOBLESS_TW_ENABLED)
		{
			show(Files.read("data/html/TerritoryManager/TerritoryManager-2.htm", player), player);
		}
		else
		{
			show(new CustomMessage("common.Disabled", player), player);
		}
	}

	public void onLoad()
	{
		System.out.println("Loaded Service: Nobless sell");
	}

	public void onReload()
	{
	}

	public void onShutdown()
	{
	}
}