package services;

import l2p.Config;
import l2p.extensions.scripts.Functions;
import l2p.extensions.scripts.ScriptFile;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.modules.community.mGenerateElement;
import l2p.gameserver.modules.option.mOption;

/**
 * User: Shaitan
 * Date: 20.03.11
 * Time: 8:50
 */
public class Pk extends Functions implements ScriptFile
{
	public void pk_page()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
		{
			return;
		}
		String append = "Сервисы отмытия пк:";
		append += "<br>";
		append += "<font color=\"LEVEL\">Отмыть 1 очко пк за " + Config.PlayerKillerPriceCount + " " + mOption.getItemName(Config.PlayerKillerPriceId) + "</font>";
		append += mGenerateElement.button("Отмыть", "scripts_services.Pk:pkkill", 200, 25);
		append += "<font color=\"LEVEL\">Отмыть " + Config.PlayerKillerKarma + " кармы за " + Config.PlayerKillerKarmaPriceCount + " " + mOption.getItemName(Config.PlayerKillerKarmaPriceId) + "</font>";
		append += mGenerateElement.button("Отмыть", "scripts_services.Pk:pkkarma", 200, 25);
		show(append, player);
	}

	public void pkkill()
	{
		L2Player player = (L2Player) getSelf();
		if(player.getPkKills() > 0 && mOption.price(player, Config.PlayerKillerPriceId, Config.PlayerKillerPriceCount))
		{
			player.setPkKills(player.getPkKills() - 1);
		}
		player.broadcastUserInfo(true);
		pk_page();
	}

	public void pkkarma()
	{
		L2Player player = (L2Player) getSelf();
		if(player.getKarma() > 0 && mOption.price(player, Config.PlayerKillerKarmaPriceId, Config.PlayerKillerKarmaPriceCount))
		{
			player.setKarma(Config.PlayerKillerKarma > player.getKarma() ? 0 : player.getKarma() - Config.PlayerKillerKarma);
		}
		player.broadcastUserInfo(true);
		pk_page();
	}

	@Override
	public void onLoad()
	{
	}

	@Override
	public void onReload()
	{
	}

	@Override
	public void onShutdown()
	{
	}
}