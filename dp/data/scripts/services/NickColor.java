package services;

import l2p.Config;
import l2p.extensions.scripts.Functions;
import l2p.extensions.scripts.ScriptFile;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.modules.option.mOption;
import l2p.gameserver.tables.ItemTable;

public class NickColor extends Functions implements ScriptFile
{
	public void list()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
		{
			return;
		}
		String s = "";
		s += "You can change nick color for small price " + Config.SERVICES_CHANGE_NICK_COLOR_PRICE + " " + ItemTable.getInstance().getTemplate(Config.SERVICES_CHANGE_NICK_COLOR_ITEM).getName() + ".";
		s += "<br>Possible colors:<br>";
		for(String color : Config.SERVICES_CHANGE_NICK_COLOR_LIST)
		{
			s += "<br><a action=\"bypass -h scripts_services.NickColor:change " + color + "\"><font color=\"" + color + "\">" + color + "</font></a>";
		}
		s += "<br><a action=\"bypass -h scripts_services.NickColor:change FFFFFF\"><font color=\"FFFFFF\">Revert to default (free)</font></a>";
		show(s, player, null);
	}

	public void change(String[] param)
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
		{
			return;
		}
		if(param[0].equalsIgnoreCase("FFFFFF"))
		{
			player.setNameColor(Integer.decode("0xFFFFFF"));
			player.broadcastUserInfo(true);
			return;
		}
		char c[];
		String s;
		c = param[0].toCharArray();
		s = "" + c[4] + c[5] + c[2] + c[3] + c[0] + c[1];
		if(mOption.price(player, Config.SERVICES_CHANGE_NICK_COLOR_ITEM, Config.SERVICES_CHANGE_NICK_COLOR_PRICE))
		{
			player.setNameColor(Integer.decode("0x" + s));
			player.broadcastUserInfo(true);
		}
	}

	public void onLoad()
	{
	}

	public void onReload()
	{
	}

	public void onShutdown()
	{
	}
}