package instances;

import l2p.extensions.scripts.Functions;
import l2p.extensions.scripts.ScriptFile;
import l2p.gameserver.GameTimeController;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.modules.instance.mInstance;

/**
 * User: Shaitan
 * Date: 23.12.10
 * Time: 10:40
 */
public class Zaken extends Functions implements ScriptFile
{
	public void onLoad()
	{
	}

	public void onReload()
	{
	}

	public void onShutdown()
	{
	}

	public void enterInstance(String[] strings)
	{
		int instancedZoneId;
		if(strings[0].equalsIgnoreCase("Night") && GameTimeController.getInstance().isNowNight())
		{
			instancedZoneId = 114;
		}
		else if(strings[0].equalsIgnoreCase("Day") && !GameTimeController.getInstance().isNowNight())
		{
			instancedZoneId = 133;
		}
		else
		{
			return;
		}
		L2Player player = (L2Player) getSelf();
		mInstance.enterInstance(player, instancedZoneId, true, true);
	}
}