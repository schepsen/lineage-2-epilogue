package instances;

/**
 * User: Shaitan
 * Date: 19.03.11
 * Time: 9:34
 */
public class SoI
{
	private static SoI ourInstance = new SoI();

	public static SoI getInstance()
	{
		return ourInstance;
	}

	private boolean active;

	public void setActive(boolean active)
	{
		this.active = active;
	}

	public boolean isActive()
	{
		return active;
	}
}