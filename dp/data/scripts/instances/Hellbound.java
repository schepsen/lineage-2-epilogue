package instances;

import l2p.extensions.scripts.Functions;
import l2p.extensions.scripts.ScriptFile;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.modules.instance.mInstance;

/**
 * User: Shaitan
 * Date: 04.05.11
 * Time: 11:07
 */
public class Hellbound extends Functions implements ScriptFile
{
	public void onLoad()
	{
	}

	public void onReload()
	{
	}

	public void onShutdown()
	{
	}

	public void enterToUrbanArea()
	{
		L2Player player = getSelfPlayer();
		mInstance.enterInstance(player, 300, true, false);
	}
}