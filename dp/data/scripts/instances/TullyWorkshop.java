package instances;

import l2p.common.ThreadPoolManager;
import l2p.extensions.scripts.Functions;
import l2p.extensions.scripts.ScriptFile;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.model.instances.L2DoorInstance;
import l2p.gameserver.model.instances.L2NpcInstance;
import l2p.gameserver.modules.data.DoorTable;

/**
 * User: Shaitan
 * Date: 23.01.11
 * Time: 7:37
 */
public class TullyWorkshop extends Functions implements ScriptFile
{
	public static boolean Tully = false;
	static int[][][] doorManager =
		{
			{{18445}, {19260002, 19260001}},
			{{18446}, {19260003}},
			{{18447}, {19260004, 19260005}},
			{{18448}, {19260006, 19260007}},
			{{18449}, {19260008}},
			{{18450}, {19260010}},
			{{18451}, {19260012}},
			{{18452}, {19260009, 19260011}},
			{{18453}, {19260014, 19260013}},
			{{18454}, {19260023, 19260015}},
			{{18455}, {19260016, 19260019}},
			{{18456}, {19260017}},
			{{18457}, {19260021, 19260020}},
			{{18458}, {19260022}},
			{{18459}, {19260018}},
			{{18460}, {19260051}},
			{{18461}, {19260052}}
		};

	public void openDoor()
	{
		L2NpcInstance npc = getNpc();
		for(int[][] index : doorManager)
		{
			if(index[0][0] == npc.getNpcId())
			{
				//if(player.getClassId().getId() == 118 || Rnd.chance(25))
				{
					for(int door : index[1])
					{
						L2DoorInstance doorObj = DoorTable.getInstance().getDoor(door);
						if(door != 19260051 && door != 19260052)
						{
							ThreadPoolManager.getInstance().scheduleAi(new CloseDoor(door), 60000, false);
						}
						doorObj.openMe();
					}
				}
				//saveGlobalQuestVar("OpenDoor - " + npc.getNpcId(), String.valueOf(Calendar.getInstance().getTimeInMillis() + 120000));
			}
		}
	}

	public void teleportToStage6()
	{
		L2Player player = getSelfPlayer();
		if(Tully)
		{
			player.sendMessage("Убейте сначала Tully.");
		}
		else
		{
			player.teleToLocation(-12220, 279696, -13592);
		}
	}

	class CloseDoor implements Runnable
	{
		private int doorId;

		public CloseDoor(int doorId)
		{
			this.doorId = doorId;
		}

		public void run()
		{
			DoorTable.getInstance().getDoor(doorId).closeMe();
		}
	}

	public void onLoad()
	{
	}

	public void onReload()
	{
	}

	public void onShutdown()
	{
	}
}