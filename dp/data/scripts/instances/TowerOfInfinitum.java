package instances;

import l2p.extensions.scripts.Functions;
import l2p.extensions.scripts.ScriptFile;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.modules.instance.mInstance;

/**
 * User: Shaitan
 * Date: 19.01.11
 * Time: 12:55
 */
public class TowerOfInfinitum extends Functions implements ScriptFile
{
	public void onLoad()
	{
	}

	public void onReload()
	{
	}

	public void onShutdown()
	{
	}

	public void enterToStage5()
	{
		L2Player player = getSelfPlayer();
		mInstance.enterInstance(player, 517, true, false);
	}

	public void enterToStage10()
	{
		L2Player player = getSelfPlayer();
		mInstance.enterInstance(player, 518, true, false);
	}
}