package commands.admin;

import java.util.StringTokenizer;
import l2p.extensions.scripts.ScriptFile;
import l2p.gameserver.handler.AdminCommandHandler;
import l2p.gameserver.handler.IAdminCommandHandler;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.model.items.L2ItemInstance;
import l2p.gameserver.serverpackets.ItemList;
import l2p.gameserver.serverpackets.SystemMessage;
import l2p.gameserver.tables.ItemTable;
import l2p.util.Log;

/**
 *
 * @author voron.dev
 */
public class AdminSummon implements IAdminCommandHandler, ScriptFile
{
	@Override
	public boolean useAdminCommand(Enum comm, String[] wordList, String fullString, L2Player activeChar)
	{
		if(activeChar.getPlayerAccess().UseGMShop)
		{
			Commands command = (Commands) comm;
			switch(command)
			{
				case admin_summon:
					StringTokenizer st = new StringTokenizer(fullString, " ");
					st.nextToken();
					int id = Integer.parseInt(st.nextToken());
					int count = Integer.parseInt(st.nextToken());
					L2ItemInstance itemInstance = ItemTable.getInstance().createItem(id);
					itemInstance.setCount(count);
					activeChar.getInventory().addItem(itemInstance);
					Log.LogItem(activeChar, Log.Adm_AddItem, itemInstance);
					if(!itemInstance.isStackable())
					{
						for(long i = 0; i < count - 1; i++)
						{
							itemInstance = ItemTable.getInstance().createItem(id);
							activeChar.getInventory().addItem(itemInstance);
							Log.LogItem(activeChar, Log.Adm_AddItem, itemInstance);
						}
					}
					activeChar.sendPacket(new ItemList(activeChar, true), SystemMessage.obtainItems(id, count, 0));
					break;
			}
			return true;
		}
		return false;
	}

	private static enum Commands
	{
		admin_summon
	}

	@Override
	public Enum[] getAdminCommandEnum()
	{
		return Commands.values();
	}

	@Override
	public void onLoad()
	{
		AdminCommandHandler.getInstance().registerAdminCommandHandler(this);
	}

	@Override
	public void onReload()
	{
	}

	@Override
	public void onShutdown()
	{
	}
}