package quests._643_RiseAndFallOfTheElrokiTribe;

import l2p.Config;
import l2p.extensions.scripts.ScriptFile;
import l2p.gameserver.model.L2Multisell;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.model.instances.L2NpcInstance;
import l2p.gameserver.model.quest.Quest;
import l2p.gameserver.model.quest.QuestState;
import l2p.util.Rnd;

import java.io.File;

public class _643_RiseAndFallOfTheElrokiTribe extends Quest implements ScriptFile
{
	private static int BONES_OF_A_PLAINS_DINOSAUR = 8776;
	private static int[] PLAIN_DINOSAURS = {
		22208, 22209, 22210, 22211, 22212, 22213, 22221, 22222, 22226, 22227, 22742, 22743, 22744, 22745
	};
	private static int[] REWARDS = {8712, 8713, 8714, 8715, 8716, 8717, 8718, 8719, 8720, 8721, 8722, 8723};

	public void onLoad()
	{
		loadMultiSell();
	}

	public void onReload()
	{
	}

	public void onShutdown()
	{
	}

	private static void loadMultiSell()
	{
		L2Multisell.getInstance().parseFile(new File(Config.DATAPACK_ROOT, "data/scripts/quests/_643_RiseAndFallOfTheElrokiTribe/32117.xml"));
	}

	public static void OnReloadMultiSell()
	{
		loadMultiSell();
	}

	public _643_RiseAndFallOfTheElrokiTribe()
	{
		super(false);
		addStartNpc(32106);
		addTalkId(32106);
		addTalkId(32117);
		for(int mob : PLAIN_DINOSAURS)
		{
			addKillId(mob);
		}
		addQuestItem(BONES_OF_A_PLAINS_DINOSAUR);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(st == null)
		{
			return null;
		}
		if(event.equalsIgnoreCase("None"))
		{
			htmltext = null;
		}
		else if(event.equalsIgnoreCase("32106-03.htm"))
		{
			st.setState(STARTED);
			st.set("cond", "1");
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("32117-03.htm"))
		{
			if(st.getQuestItemsCount(BONES_OF_A_PLAINS_DINOSAUR) >= 300)
			{
				st.takeItems(BONES_OF_A_PLAINS_DINOSAUR, 300);
				st.giveItems(REWARDS[Rnd.get(REWARDS.length)], 5, false);
			}
			else
			{
				htmltext = "32117-04.htm";
			}
		}
		else if(event.equalsIgnoreCase("Quit"))
		{
			htmltext = null;
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(true);
		}
		else if(event.equalsIgnoreCase("multisell"))
		{
			L2Multisell.getInstance().SeparateAndSend(32117, st.getPlayer(), 0);
			return null;
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getCond();
		long count = st.getQuestItemsCount(BONES_OF_A_PLAINS_DINOSAUR);
		if((cond == 0) && (npcId == 32106))
		{
			if(st.getPlayer().getLevel() >= 75)
			{
				htmltext = "32106-01.htm";
			}
			else
			{
				htmltext = "32106-00.htm";
				st.exitCurrentQuest(true);
			}
		}
		else if(st.getState() == STARTED)
		{
			if(npcId == 32106)
			{
				if(count == 0)
				{
					htmltext = "32106-05.htm";
				}
				else
				{
					htmltext = "32106-06.htm";
					st.takeItems(BONES_OF_A_PLAINS_DINOSAUR, -1);
					st.giveItems(ADENA_ID, count * 1374, false);
				}
			}
			else if(npcId == 32117)
			{
				htmltext = "32117-01.htm";
			}
		}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState qs)
	{
		int npcId = npc.getNpcId();
		L2Player player = qs.getRandomPartyMember(STARTED, Config.ALT_PARTY_DISTRIBUTION_RANGE);
		if(player == null || qs == null)
		{
			return null;
		}
		QuestState st = player.getQuestState(qs.getQuest().getName());
		if(contains(PLAIN_DINOSAURS, npcId) && st.getCond() == 1 && st.getState() == STARTED && Rnd.chance(75))
		{
			st.giveItems(BONES_OF_A_PLAINS_DINOSAUR, 1);
			st.playSound(SOUND_ITEMGET);
		}
		return null;
	}
}