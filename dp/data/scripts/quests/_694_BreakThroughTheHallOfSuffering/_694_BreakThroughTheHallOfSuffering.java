package quests._694_BreakThroughTheHallOfSuffering;

import l2p.Config;
import l2p.common.ThreadPoolManager;
import l2p.extensions.scripts.ScriptFile;
import l2p.gameserver.model.L2Party;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.model.instances.L2NpcInstance;
import l2p.gameserver.model.quest.Quest;
import l2p.gameserver.model.quest.QuestState;
import l2p.gameserver.modules.instance.SoI.BreakThroughTheHallOfSuffering;
import l2p.gameserver.modules.instance.SoI.mSoI;
import l2p.gameserver.modules.instance.mInstance;
import l2p.gameserver.serverpackets.SystemMessage;
import l2p.gameserver.skills.Stats;
import l2p.gameserver.skills.funcs.FuncMul;

/**
 * User: Shaitan
 * Date: 23.03.11
 * Time: 11:41
 */
public class _694_BreakThroughTheHallOfSuffering extends Quest implements ScriptFile
{
	public _694_BreakThroughTheHallOfSuffering()
	{
		super(PARTY_NONE);
		addStartNpc(mSoI.npcTepios);
		addTalkId(mSoI.npcMouthOfEkimus, mSoI.npcTepios2);
		addKillId(mSoI.idMonsterTumorOfDeath);
		addKillId(mSoI.idMonsterYehanKlodekus);
		addKillId(mSoI.idMonsterYehanKlanikus);
	}

	@Override
	public String onEvent(String event, QuestState qs, L2NpcInstance npc)
	{
		String htmltext = event;
		//
		L2Player player = qs.getPlayer();
		//
		long refId = player.getReflectionId();
		BreakThroughTheHallOfSuffering inst = mSoI.breakThroughTheHallOfSuffering.get(refId);
		//
		if(event.equalsIgnoreCase("32603-05.htm"))
		{
			qs.setCond(1);
			qs.setState(STARTED);
			qs.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("ai_is_time_attack_reward_q0695_12.htm"))
		{
			L2Party party = player.getParty();
			if(party != null)
			{
				if(party.isLeader(player))
				{
					for(L2Player member : player.getParty().getPartyMembers())
					{
						QuestState qsp = member.getQuestState(this._name);
						if(qsp != null && qsp.getCond() == 2)
						{
							qsp.giveItems(13776 + inst.rewardType, 1);
							qsp.exitCurrentQuest(true);
						}
					}
					player.getReflection().startCollapseTimer(60 * 1000);
					mSoI.breakThroughTheHallOfSuffering.remove(refId);
					htmltext = "ai_is_time_attack_reward_q0695_13.htm";
				}
				else
				{
					htmltext = "ai_is_time_attack_reward_q0695_12.htm";
				}
			}
			else
			{
				htmltext = "ai_is_time_attack_reward001.htm";
			}
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState qs)
	{
		String htmltext = "noquest";
		//
		int npcId = npc.getNpcId();
		int cond = qs.getCond();
		//
		L2Player player = qs.getPlayer();
		L2Party party = player.getParty();
		//
		long refId = player.getReflectionId();
		BreakThroughTheHallOfSuffering inst = mSoI.breakThroughTheHallOfSuffering.get(refId);
		//
		if(npcId == mSoI.npcTepios)
		{
			if(player.getLevel() < 75)
			{
				htmltext = "32603-02.htm";
				qs.exitCurrentQuest(true);
			}
			else if(player.getLevel() > 82)
			{
				htmltext = "32603-02a.htm";
				qs.exitCurrentQuest(true);
			}
			else if(cond == 0)
			{
				htmltext = "32603-01.htm";
			}
			else if(cond == 1)
			{
				htmltext = "32603-07.htm";
			}
		}
		else if(npcId == mSoI.npcMouthOfEkimus)
		{
			if(mSoI.getStage() != 1)
			{
				player.sendMessage("В данный момент прохождение Break Through The Hall Of Suffering не доступно.");
				return null;
			}
			if(party != null)
			{
				if(mSoI.breakThroughTheHallOfSuffering.size() >= Config.BreakThroughTheHallOfSufferingCount)
				{
					player.sendMessage("Проходить Break Through The Hall Of Suffering могут параллельно не более" + Config.BreakThroughTheHallOfSufferingCount + " групп.");
					return null;
				}
				for(L2Player member : party.getPartyMembers())
				{
					QuestState qsm = member.getQuestState(this.getName());
					if(qsm == null || qsm.getCond() != 1)
					{
						party.broadcastToPartyMembers(new SystemMessage("У персонажа " + member.getName() + ", не взят квест для входа."));
						return null;
					}
				}
				mInstance.enterInstance(player, 115, true, true);
				inst = new BreakThroughTheHallOfSuffering();
				inst.instance.setName(this.getName());
				mSoI.breakThroughTheHallOfSuffering.put(player.getReflectionId(), inst);
			}
			htmltext = null;
		}
		else if(npcId == mSoI.npcTepios2)
		{
			if(inst == null)
			{
				player.sendMessage("В данный момент прохождение Break Through The Hall Of Suffering не доступно.");
				return null;
			}
			int time = inst.timer.getSeconds();
			// 0 мин - 20 мин
			if(time < 20 * 60)
			{
				inst.rewardType = 1;
			}
			// 20 мин - 23 мин
			else if(time >= 20 * 60 && time <= 21 * 60)
			{
				inst.rewardType = 2;
			}
			// 21 мин - 24 мин
			else if(time > 21 * 60 && time <= 22 * 60)
			{
				inst.rewardType = 3;
			}
			// 22 мин - 25 мин
			else if(time > 22 * 60 && time <= 23 * 60)
			{
				inst.rewardType = 4;
			}
			// 23 мин - 26 мин
			else if(time > 23 * 60 && time <= 24 * 60)
			{
				inst.rewardType = 5;
			}
			// 24 мин - 60 мин
			else if(time > 24 * 60 && time <= 25 * 60)
			{
				inst.rewardType = 6;
			}
			// 25 мин - 60 мин
			else if(time > 25 * 60 && time <= 26 * 60)
			{
				inst.rewardType = 7;
			}
			// 26 мин - 60 мин
			else if(time > 26 * 60 && time <= 27 * 60)
			{
				inst.rewardType = 8;
			}
			// 27 мин - 60 мин
			else if(time > 27 * 60 && time <= 38 * 60)
			{
				inst.rewardType = 9;
			}
			// 28 мин - 60 мин
			else if(time > 28 * 60)
			{
				inst.rewardType = 10;
			}
			htmltext = "ai_is_time_attack_reward_q0695_0" + inst.rewardType + ".htm";
		}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState qs)
	{
		int npcId = npc.getNpcId();
		//
		L2Player player = qs.getPlayer();
		//
		final long refId = player.getReflectionId();
		final BreakThroughTheHallOfSuffering inst = mSoI.breakThroughTheHallOfSuffering.get(refId);
		//
		if(inst == null)
		{
			return null;
		}
		//
		if(npcId == mSoI.idMonsterTumorOfDeath)
		{
			inst.countMob++;
			for(L2NpcInstance monster : player.getAroundNpc(1000, 500))
			{
				monster.addStatFunc(new FuncMul(Stats.POWER_DEFENCE, 0x30, this, 0.7));
				monster.addStatFunc(new FuncMul(Stats.MAGIC_DEFENCE, 0x30, this, 0.7));
			}
			if(inst.countMob == 5)
			{
				inst.monsterYehanKlodekus = addSpawnToInstance(mSoI.idMonsterYehanKlodekus, mSoI.KlodekusLoc, 0, refId);
				inst.monsterYehanKlanikus = addSpawnToInstance(mSoI.idMonsterYehanKlanikus, mSoI.KlanikusLoc, 0, refId);
			}
		}
		else if((npcId == mSoI.idMonsterYehanKlodekus || npcId == mSoI.idMonsterYehanKlanikus) && (inst.monsterYehanKlodekus.isDead() && inst.monsterYehanKlanikus.isDead()))
		{
			if(inst.RessTask != null)
			{
				inst.RessTask.cancel(true);
			}
			inst.timer.stop();
			for(L2Player member : player.getParty().getPartyMembers())
			{
				QuestState qsp = member.getQuestState(this._name);
				if(qsp != null && qsp.getCond() == 1)
				{
					qsp.setCond(2);
				}
			}
			addSpawnToInstance(mSoI.npcTepios2, mSoI.Center, 0, refId);
			mSoI.setStage(2);
		}
		else if(npcId == mSoI.idMonsterYehanKlodekus)
		{
			if(inst.RessTask != null)
			{
				inst.RessTask.cancel(true);
			}
			inst.RessTask = ThreadPoolManager.getInstance().scheduleGeneral(
				new Runnable()
				{
					@Override
					public void run()
					{
						inst.monsterYehanKlodekus = addSpawnToInstance(mSoI.idMonsterYehanKlodekus, mSoI.KlodekusLoc, 0, refId);
					}
				}
				, 20000);
		}
		else if(npcId == mSoI.idMonsterYehanKlanikus)
		{
			if(inst.RessTask != null)
			{
				inst.RessTask.cancel(true);
			}
			inst.RessTask = ThreadPoolManager.getInstance().scheduleGeneral(
				new Runnable()
				{
					@Override
					public void run()
					{
						inst.monsterYehanKlanikus = addSpawnToInstance(mSoI.idMonsterYehanKlanikus, mSoI.KlanikusLoc, 0, refId);
					}
				}
				, 20000);
		}
		return null;
	}

	@Override
	public void onLoad()
	{
	}

	@Override
	public void onReload()
	{
	}

	@Override
	public void onShutdown()
	{
	}
}