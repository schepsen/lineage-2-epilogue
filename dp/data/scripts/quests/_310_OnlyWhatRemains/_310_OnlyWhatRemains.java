package quests._310_OnlyWhatRemains;

import l2p.extensions.scripts.ScriptFile;
import l2p.gameserver.model.instances.L2NpcInstance;
import l2p.gameserver.model.quest.Quest;
import l2p.gameserver.model.quest.QuestState;

public class _310_OnlyWhatRemains extends Quest implements ScriptFile
{
	private static int KINTAIJIN = 32640;
	private static int[] CANNIBALISTIC_STAKATO = {22624, 22625, 22626};
	private static int SPIKED_STAKATO = 22617;
	private static int DIRTY_BEADS = 14880;
	private static int GROWTH_ACCELERATOR = 14832;
	private static int MULTI_COLORED_JEWEL = 14835;

	public void onLoad()
	{
	}

	public void onReload()
	{
	}

	public void onShutdown()
	{
	}

	public _310_OnlyWhatRemains()
	{
		super(false);
		addStartNpc(KINTAIJIN);
		addTalkId(KINTAIJIN);
		addKillId(CANNIBALISTIC_STAKATO);
		addKillId(SPIKED_STAKATO);
		addQuestItem(DIRTY_BEADS);
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int id = st.getState();
		int cond = st.getCond();
		if(id == COMPLETED)
		{
			htmltext = "32640-10.htm";
		}
		else if(id == CREATED)
		{
			QuestState ImTheOnlyOneYouCan = st.getPlayer().getQuestState("_240_ImTheOnlyOneYouCanTrust");
			if(ImTheOnlyOneYouCan != null && ImTheOnlyOneYouCan.isCompleted() && st.getPlayer().getLevel() >= 81)
			{
				htmltext = "32640-1.htm";
			}
			else
			{
				htmltext = "32640-0.htm";
				st.exitCurrentQuest(true);
			}
		}
		else
		{
			if(cond == 1)
			{
				htmltext = "32640-8.htm";
			}
			if(cond == 2)
			{
				htmltext = "32640-9.htm";
			}
		}
		return htmltext;
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equals("give_item"))
		{
			st.setCond(1);
			st.takeItems(DIRTY_BEADS, 500);
			st.giveItems(GROWTH_ACCELERATOR, 1);
			if(st.getQuestItemsCount(MULTI_COLORED_JEWEL) == 0)
			{
				st.giveItems(MULTI_COLORED_JEWEL, 1);
			}
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(true);
			htmltext = "32640-12.htm";
		}
		if(event.equalsIgnoreCase("32640-3.htm"))
		{
			st.setCond(1);
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		if(st.getCond() == 1)
		{
			st.rollAndGive(DIRTY_BEADS, 1, 100);
			if(st.getQuestItemsCount(DIRTY_BEADS) >= 500)
			{
				st.setCond(2);
				st.playSound(SOUND_MIDDLE);
			}
			else
			{
				st.playSound(SOUND_ITEMGET);
			}
		}
		return null;
	}
}