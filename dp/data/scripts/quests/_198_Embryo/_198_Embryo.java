package quests._198_Embryo;

import javolution.util.FastMap;
import l2p.extensions.scripts.Functions;
import l2p.extensions.scripts.ScriptFile;
import l2p.gameserver.ai.CtrlIntention;
import l2p.gameserver.instancemanager.InstancedZoneManager.InstancedZone;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.model.Reflection;
import l2p.gameserver.model.instances.L2NpcInstance;
import l2p.gameserver.model.quest.Quest;
import l2p.gameserver.model.quest.QuestState;
import l2p.gameserver.modules.instance.mInstance;
import l2p.util.Location;

import java.util.HashMap;

public class _198_Embryo extends Quest implements ScriptFile
{
	// NPCs
	private static int WOOD = 32593;
	private static int FRANZ = 32597;
	private static int JAINA = 32582;
	private static int SHILENSEVIL1 = 27346;
	// ITEMS
	private static int AA = 5575;
	private static int SCULPTURE = 14360;
	private static int BRACELET = 15312;
	private static int AARATE = 1;
	private static HashMap<Long, World> worlds = new HashMap<Long, World>();
	private static FastMap<Integer, InstancedZone> ils;
	private static InstancedZone il;

	public class World
	{
		public long instanceId;
		public int status;
	}

	@Override
	public void onLoad()
	{
	}

	@Override
	public void onReload()
	{
	}

	@Override
	public void onShutdown()
	{
	}
	public L2NpcInstance monster1;
	public L2NpcInstance monster2;
	public L2NpcInstance monster3;

	public _198_Embryo()
	{
		super(false);
		addStartNpc(WOOD);
		addTalkId(FRANZ, JAINA);
		addQuestItem(SCULPTURE);
		addKillId(SHILENSEVIL1);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		L2Player player = st.getPlayer();
		Reflection r = player.getReflection();
		String htmltext = event;
		if(event.equalsIgnoreCase("32593-02.htm"))
		{
			st.setCond(1);
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("32597-10.htm"))
		{
			st.takeItems(SCULPTURE, 1);
			st.setCond(3);
		}
		else if(event.equalsIgnoreCase("go"))
		{
			mInstance.enterInstance(player, 113, false, false);
			return null;
		}
		else if(event.equalsIgnoreCase("32597-05.htm"))
		{
			monster1 = Quest.addSpawnToInstance(SHILENSEVIL1, new Location(-23768, -8968, -5412), 0, player.getReflection().getId());
			Functions.npcSay(monster1, "You are not the owner of that item!");
			monster1.setRunning();
			player.addDamageHate(monster1, 0, 999);
			monster1.getAI().setIntention(CtrlIntention.AI_INTENTION_ATTACK, player);
			int SHILENSEVIL2 = 27343;
			monster2 = Quest.addSpawnToInstance(SHILENSEVIL2, new Location(-23768, -9080, -5413), 0, player.getReflection().getId());
			monster2.setRunning();
			player.addDamageHate(monster2, 0, 999);
			monster2.getAI().setIntention(CtrlIntention.AI_INTENTION_ATTACK, player);
			monster3 = Quest.addSpawnToInstance(SHILENSEVIL2, new Location(-23768, -8824, -5413), 0, player.getReflection().getId());
			monster3.setRunning();
			player.addDamageHate(monster3, 0, 999);
			monster3.getAI().setIntention(CtrlIntention.AI_INTENTION_ATTACK, player);
		}
		else if(event.equalsIgnoreCase("exit"))
		{
			r.startCollapseTimer(1000);
			return null;
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		int cond = st.getCond();
		int id = st.getState();
		L2Player player = st.getPlayer();
		if(npcId == WOOD)
		{
			if(npcId == WOOD)
			{
				if(player.getLevel() < 79)
				{
					st.exitCurrentQuest(true);
					return "32593-00.htm";
				}
				QuestState qs = player.getQuestState("_197_SevenSignTheSacredBookOfSeal");
				if(qs == null)
				{
					return null;
				}
				if(qs.isCompleted() && id == CREATED)
				{
					return "32593-01.htm";
				}
				else if(cond == 1)
				{
					return "32593-02.htm";
				}
				else if(cond == 3)
				{
					if(player.getBaseClassId() == player.getActiveClassId())
					{
						st.addExpAndSp(315108096, 34906059);
						st.giveItems(BRACELET, 1);
						st.giveItems(AA, 1500000);
						st.takeItems(SCULPTURE, 1);
						st.setState(COMPLETED);
						st.exitCurrentQuest(false);
						st.playSound(SOUND_FINISH);
						return "32593-04.htm";
					}
					else
					{
						return "subclass_forbidden.htm";
					}
				}
				else if(cond == 0)
				{
					st.exitCurrentQuest(true);
					return "32593-00.htm";
				}
			}
		}
		else if(npcId == FRANZ)
		{
			if(cond == 1)
			{
				return "32597-01.htm";
			}
			else if(cond == 2)
			{
				return "32597-06.htm";
			}
			else if(cond == 3)
			{
				return "32597-11.htm";
			}
		}
		else if(npcId == JAINA)
		{
			if(cond == 3)
			{
				return "32582.htm";
			}
		}
		return null;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		L2Player player = st.getPlayer();
		int npcId = npc.getNpcId();
		if(npcId == SHILENSEVIL1 && st.getCond() == 1)
		{
			st.giveItems(SCULPTURE, 1);
			st.setCond(2);
			st.playSound(SOUND_MIDDLE);
		}
		monster1.deleteMe();
		monster2.deleteMe();
		monster3.deleteMe();
		Functions.npcSay(npc, "... You may have won this time... But next time, I will surely capture you!");
		player.showQuestMovie(14);
		return null;
	}
}