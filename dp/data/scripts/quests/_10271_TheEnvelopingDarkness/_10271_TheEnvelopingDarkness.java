package quests._10271_TheEnvelopingDarkness;

import l2p.extensions.scripts.ScriptFile;
import l2p.gameserver.model.instances.L2NpcInstance;
import l2p.gameserver.model.quest.Quest;
import l2p.gameserver.model.quest.QuestState;

public class _10271_TheEnvelopingDarkness extends Quest implements ScriptFile
{
	//NPC'S
	private final static int ORBYU = 32560;
	private final static int EL = 32556;
	private final static int MEDIBAL = 32528;
	//ITEMS
	private final static int DOCUMENT = 13852;
	public CheckStatus LAST_CHECK_STATUS = CheckStatus.GRACIA_EPILOGUE;

	public void onLoad()
	{
	}

	public void onReload()
	{
	}

	public void onShutdown()
	{
	}

	public _10271_TheEnvelopingDarkness()
	{
		super(false);
		addStartNpc(ORBYU);
		addTalkId(EL);
		addTalkId(MEDIBAL);
	}

	@Override
	public String onEvent(String event, QuestState qs, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("32560-02.htm"))
		{
			qs.setCond(1);
			qs.setState(STARTED);
			qs.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("32556-02.htm"))
		{
			qs.setCond(2);
			qs.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("32556-05.htm"))
		{
			qs.setCond(4);
			qs.playSound(SOUND_MIDDLE);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getCond();
		if(npcId == ORBYU)
		{
			if(st.getState() == CREATED)
			{
				if(st.getPlayer().getLevel() >= 75)
				{
					htmltext = "32560-01.htm";
				}
				else
				{
					htmltext = "32560-00.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(cond >= 1 && cond <= 3)
			{
				htmltext = "32560-03.htm";
			}
			else if(cond == 4 && st.getQuestItemsCount(DOCUMENT) >= 1)
			{
				st.takeItems(DOCUMENT, 1);
				htmltext = "32560-04.htm";
				st.addExpAndSp(377403, 37867);
				st.giveItems(57, 62516);
				st.setState(COMPLETED);
				st.playSound(SOUND_FINISH);
				st.exitCurrentQuest(false);
			}
		}
		else if(npcId == EL)
		{
			if(cond == 1)
			{
				htmltext = "32556-01.htm";
			}
			else if(cond == 2)
			{
				htmltext = "32556-03.htm";
			}
			else if(cond == 3)
			{
				htmltext = "32556-04.htm";
			}
			else if(cond == 4)
			{
				htmltext = "32556-06.htm";
			}
		}
		else if(npcId == MEDIBAL)
		{
			if(cond == 2)
			{
				st.setCond(3);
				st.giveItems(DOCUMENT, 1);
				htmltext = "32528-01.htm";
			}
			else
			{
				htmltext = "32528-02.htm";
			}
		}
		return htmltext;
	}
}