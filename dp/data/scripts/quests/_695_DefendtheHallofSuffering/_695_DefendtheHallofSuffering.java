package quests._695_DefendtheHallofSuffering;

import l2p.Config;
import l2p.common.ThreadPoolManager;
import l2p.extensions.scripts.ScriptFile;
import l2p.gameserver.model.L2Party;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.model.instances.L2NpcInstance;
import l2p.gameserver.model.quest.Quest;
import l2p.gameserver.model.quest.QuestState;
import l2p.gameserver.modules.instance.SoI.DefendTheHallofSuffering;
import l2p.gameserver.modules.instance.SoI.mSoI;
import l2p.gameserver.modules.instance.mInstance;
import l2p.gameserver.serverpackets.SystemMessage;
import l2p.gameserver.skills.Stats;
import l2p.gameserver.skills.funcs.FuncMul;
import l2p.util.Location;
import l2p.util.Rnd;

public class _695_DefendtheHallofSuffering extends Quest implements ScriptFile
{
	private static final int[] idMonsters = {22509, 22510, 22511, 22512, 22513, 22514, 22515};
	private static final int[] monsterWaves = {8, 14, 20, 26, 32};

	public _695_DefendtheHallofSuffering()
	{
		super(PARTY_NONE);
		addStartNpc(mSoI.npcTepios);
		addTalkId(mSoI.npcMouthOfEkimus, mSoI.npcTepios2);
		addKillId(mSoI.idMonsterTumorOfDeath);
		addKillId(mSoI.idMonsterYehanKlodekus);
		addKillId(mSoI.idMonsterYehanKlanikus);
		addKillId(idMonsters);
	}

	@Override
	public String onEvent(String event, QuestState qs, L2NpcInstance npc)
	{
		String htmltext = event;
		//
		L2Player player = qs.getPlayer();
		//
		long refId = player.getReflectionId();
		DefendTheHallofSuffering inst = mSoI.defendTheHallofSuffering.get(refId);
		//
		if(event.equalsIgnoreCase("32603-05.htm"))
		{
			qs.setCond(1);
			qs.setState(STARTED);
			qs.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("ai_is_time_attack_reward_q0695_12.htm"))
		{
			L2Party party = player.getParty();
			if(party != null)
			{
				if(party.isLeader(player))
				{
					for(L2Player member : player.getParty().getPartyMembers())
					{
						QuestState qsp = member.getQuestState(this._name);
						if(qsp != null && qsp.getCond() == 2)
						{
							qsp.giveItems(13776 + inst.rewardType, 1);
							qsp.exitCurrentQuest(true);
						}
					}
					player.getReflection().startCollapseTimer(60 * 1000);
					mSoI.defendTheHallofSuffering.remove(refId);
					htmltext = "ai_is_time_attack_reward_q0695_13.htm";
				}
				else
				{
					htmltext = "ai_is_time_attack_reward_q0695_12.htm";
				}
			}
			else
			{
				htmltext = "ai_is_time_attack_reward001.htm";
			}
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState qs)
	{
		String htmltext = "noquest";
		//
		int npcId = npc.getNpcId();
		int cond = qs.getCond();
		//
		L2Player player = qs.getPlayer();
		L2Party party = player.getParty();
		//
		long refId = player.getReflectionId();
		DefendTheHallofSuffering inst = mSoI.defendTheHallofSuffering.get(refId);
		//
		if(npcId == mSoI.npcTepios)
		{
			if(player.getLevel() < 75)
			{
				htmltext = "32603-02.htm";
				qs.exitCurrentQuest(true);
			}
			else if(player.getLevel() > 82)
			{
				htmltext = "32603-02a.htm";
				qs.exitCurrentQuest(true);
			}
			else if(cond == 0)
			{
				htmltext = "32603-01.htm";
			}
			else if(cond == 1)
			{
				htmltext = "32603-06.htm";
			}
		}
		else if(npcId == mSoI.npcMouthOfEkimus)
		{
			if(mSoI.getStage() != 2)
			{
				player.sendMessage("В данный момент прохождение Defend The Hall Of Suffering не доступно.");
				return null;
			}
			if(party != null)
			{
				if(mSoI.defendTheHallofSuffering.size() >= Config.DefendTheHallofSuffering)
				{
					player.sendMessage("Проходить Defend The Hall Of Suffering могут параллельно не более" + Config.DefendTheHallofSuffering + " групп.");
					return null;
				}
				for(L2Player member : party.getPartyMembers())
				{
					QuestState qsm = member.getQuestState(this.getName());
					if(qsm == null || qsm.getCond() != 1)
					{
						party.broadcastToPartyMembers(new SystemMessage("У персонажа " + member.getName() + ", не взят квест для входа."));
						return null;
					}
				}
				if(mInstance.enterInstance(player, 116, true, true))
				{
					refId = player.getReflectionId();
					inst = new DefendTheHallofSuffering();
					inst.instance.setName(this.getName());
					mSoI.defendTheHallofSuffering.put(refId, inst);
					ThreadPoolManager.getInstance().scheduleGeneral(new Start(refId), 60000);
				}
			}
			htmltext = null;
		}
		else if(npcId == mSoI.npcTepios2)
		{
			if(inst == null)
			{
				player.sendMessage("В данный момент прохождение Defend The Hall Of Suffering не доступно.");
				return null;
			}
			int time = inst.timer.getSeconds();
			// 0 мин - 20 мин
			if(time < 20 * 60)
			{
				inst.rewardType = 1;
			}
			// 20 мин - 23 мин
			else if(time >= 20 * 60 && time <= 21 * 60)
			{
				inst.rewardType = 2;
			}
			// 21 мин - 24 мин
			else if(time > 21 * 60 && time <= 22 * 60)
			{
				inst.rewardType = 3;
			}
			// 22 мин - 25 мин
			else if(time > 22 * 60 && time <= 23 * 60)
			{
				inst.rewardType = 4;
			}
			// 23 мин - 26 мин
			else if(time > 23 * 60 && time <= 24 * 60)
			{
				inst.rewardType = 5;
			}
			// 24 мин - 60 мин
			else if(time > 24 * 60 && time <= 25 * 60)
			{
				inst.rewardType = 6;
			}
			// 25 мин - 60 мин
			else if(time > 25 * 60 && time <= 26 * 60)
			{
				inst.rewardType = 7;
			}
			// 26 мин - 60 мин
			else if(time > 26 * 60 && time <= 27 * 60)
			{
				inst.rewardType = 8;
			}
			// 27 мин - 60 мин
			else if(time > 27 * 60 && time <= 38 * 60)
			{
				inst.rewardType = 9;
			}
			// 28 мин - 60 мин
			else if(time > 28 * 60)
			{
				inst.rewardType = 10;
			}
			htmltext = "ai_is_time_attack_reward_q0695_0" + inst.rewardType + ".htm";
		}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState qs)
	{
		int npcId = npc.getNpcId();
		//
		L2Player player = qs.getPlayer();
		//
		final long refId = player.getReflectionId();
		final DefendTheHallofSuffering inst = mSoI.defendTheHallofSuffering.get(refId);
		//
		if(inst == null)
		{
			return null;
		}
		//
		if(npcId == mSoI.idMonsterTumorOfDeath)
		{
			for(L2NpcInstance monster : inst.instance.getMonsters())
			{
				monster.addStatFunc(new FuncMul(Stats.POWER_DEFENCE, 0x30, this, 0.7));
				monster.addStatFunc(new FuncMul(Stats.MAGIC_DEFENCE, 0x30, this, 0.7));
			}
		}
		else if(isQMob(npcId))
		{
			inst.countMob++;
			if(inst.waveId < monsterWaves.length && inst.countMob == monsterWaves[inst.waveId])
			{
				inst.waveId++;
				inst.countMob = 0;
				inst.instance.clearMonster();
				ThreadPoolManager.getInstance().scheduleGeneral(new Wave(inst.waveId, refId), 60 * 1000);
			}
		}
		else if((npcId == mSoI.idMonsterYehanKlodekus || npcId == mSoI.idMonsterYehanKlanikus) && (inst.monsterYehanKlodekus.isDead() && inst.monsterYehanKlanikus.isDead()))
		{
			if(inst.RessTask != null)
			{
				inst.RessTask.cancel(true);
			}
			inst.timer.stop();
			for(L2Player member : player.getParty().getPartyMembers())
			{
				QuestState qsp = member.getQuestState(this._name);
				if(qsp != null && qsp.getCond() == 1)
				{
					qsp.setCond(2);
				}
			}
			addSpawnToInstance(mSoI.npcTepios2, mSoI.Center, 0, player.getReflectionId());
			mSoI.setStage(1);
		}
		else if(npcId == mSoI.idMonsterYehanKlodekus)
		{
			if(inst.RessTask != null)
			{
				inst.RessTask.cancel(true);
			}
			inst.RessTask = ThreadPoolManager.getInstance().scheduleGeneral(
				new Runnable()
				{
					@Override
					public void run()
					{
						inst.monsterYehanKlodekus = addSpawnToInstance(mSoI.idMonsterYehanKlodekus, mSoI.KlodekusLoc, 0, refId);
					}
				}
				, 20000);
		}
		else if(npcId == mSoI.idMonsterYehanKlanikus)
		{
			if(inst.RessTask != null)
			{
				inst.RessTask.cancel(true);
			}
			inst.RessTask = ThreadPoolManager.getInstance().scheduleGeneral(
				new Runnable()
				{
					@Override
					public void run()
					{
						inst.monsterYehanKlanikus = addSpawnToInstance(mSoI.idMonsterYehanKlanikus, mSoI.KlanikusLoc, 0, refId);
					}
				}
				, 20000);
		}
		return null;
	}

	private class Start implements Runnable
	{
		private long refId;

		Start(long refId)
		{
			this.refId = refId;
		}

		@Override
		public void run()
		{
			DefendTheHallofSuffering inst = mSoI.defendTheHallofSuffering.get(refId);
			inst.timer.start();
			ThreadPoolManager.getInstance().scheduleGeneral(new Wave(0, refId), 3 * 1000);
		}
	}

	private class Wave implements Runnable
	{
		private int stage;
		private long refId;

		Wave(int stage, long refId)
		{
			this.stage = stage;
			this.refId = refId;
		}

		@Override
		public void run()
		{
			DefendTheHallofSuffering inst = mSoI.defendTheHallofSuffering.get(refId);
			if(stage <= 4)
			{
				if(inst.monsterTumorOfDeath == null || inst.monsterTumorOfDeath.isDead())
				{
					inst.monsterTumorOfDeath = addSpawnToInstance(mSoI.idMonsterTumorOfDeath, mSoI.Center, 0, refId);
				}
				int x, y, z = mSoI.Center.z;
				for(int i = 0; i < monsterWaves[stage]; i++)
				{
					x = Rnd.get(mSoI.Center.x - 750, mSoI.Center.x + 750);
					y = Rnd.get(mSoI.Center.y - 750, mSoI.Center.y + 750);
					inst.instance.addMonster(addSpawnToInstance(idMonsters[Rnd.get(idMonsters.length)], new Location(x, y, z, Rnd.get(65536)), 0, refId));
				}
			}
			else
			{
				if(!inst.monsterTumorOfDeath.isDead())
				{
					inst.monsterTumorOfDeath.deleteMe();
				}
				inst.monsterYehanKlodekus = addSpawnToInstance(mSoI.idMonsterYehanKlodekus, mSoI.KlodekusLoc, 0, refId);
				inst.monsterYehanKlanikus = addSpawnToInstance(mSoI.idMonsterYehanKlanikus, mSoI.KlanikusLoc, 0, refId);
			}
		}
	}

	private boolean isQMob(int npcId)
	{
		for(int i : idMonsters)
		{
			if(i == npcId)
			{
				return true;
			}
		}
		return false;
	}

	@Override
	public void onLoad()
	{
	}

	@Override
	public void onReload()
	{
	}

	@Override
	public void onShutdown()
	{
	}
}