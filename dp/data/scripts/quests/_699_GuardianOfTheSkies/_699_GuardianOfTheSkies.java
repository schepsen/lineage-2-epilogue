package quests._699_GuardianOfTheSkies;

import l2p.Config;
import l2p.extensions.scripts.ScriptFile;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.model.instances.L2NpcInstance;
import l2p.gameserver.model.quest.Quest;
import l2p.gameserver.model.quest.QuestState;
import quests._10273_GoodDayToFly._10273_GoodDayToFly;

public class _699_GuardianOfTheSkies extends Quest implements ScriptFile
{
	//Npc
	private static final int LEKON = 32557;
	//Quest_items
	private static int GOLDEN_FEATHER = 13871;
	//MOBS
	private static int[] MOBS = {22614, 22615, 25623, 25633};
	//Settings
	private static int DROP_CHANCE = 80;

	public void onLoad()
	{
	}

	public void onReload()
	{
	}

	public void onShutdown()
	{
	}

	public _699_GuardianOfTheSkies()
	{
		super(false);
		addStartNpc(LEKON);
		addTalkId(LEKON);
		addKillId(MOBS);
		addQuestItem(GOLDEN_FEATHER);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("32557-03.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("32557-quit.htm"))
		{
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(true);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getCond();
		L2Player player = st.getPlayer();
		if(npcId == LEKON)
		{
			QuestState qs = player.getQuestState(_10273_GoodDayToFly.class);
			if((qs != null) && (qs.getState() == 2) && (st.getState() == 0) && (player.getLevel() >= 75))
			{
				htmltext = "32557-01.htm";
			}
			else if(cond == 1)
			{
				long itemcount = st.getQuestItemsCount(GOLDEN_FEATHER);
				if(itemcount > 0)
				{
					st.takeItems(GOLDEN_FEATHER, -1);
					st.giveItems(ADENA_ID, itemcount * 2300);
					st.playSound(SOUND_ITEMGET);
					htmltext = "32557-06.htm";
				}
				else
				{
					htmltext = "32557-04.htm";
				}
			}
			else if(cond == 0)
			{
				htmltext = "32557-00.htm";
				st.exitCurrentQuest(true);
			}
		}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		if(st.getState() != STARTED)
		{
			return null;
		}
		if((st.getCond() == 1) && (contains(MOBS, npc.getNpcId())))
		{
			st.rollAndGive(GOLDEN_FEATHER, (int) (1 * Config.RATE_QUESTS_REWARD), DROP_CHANCE);
			st.playSound(SOUND_ITEMGET);
		}
		return null;
	}
}