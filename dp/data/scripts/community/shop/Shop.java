package community.shop;

import community.Community;
import community.mStaticPage;
import l2p.Config;
import l2p.gameserver.model.L2Multisell;
import l2p.gameserver.model.L2ObjectsStorage;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.modules.community.mCommunityHandler;
import l2p.gameserver.modules.community.mICommunityHandler;
import l2p.gameserver.serverpackets.ExBuySellList;

import java.io.File;
import java.util.StringTokenizer;

/**
 * User: Shaitan
 * Date: 01.11.2010
 * Time: 15:06:13
 */
public class Shop implements mICommunityHandler
{
	int[] multisell;

	public void onLoad()
	{
		if(!Config.communityShop)
		{
			return;
		}
		mCommunityHandler.getInstance().addHandler(this);
		File dir = new File(Config.DATAPACK_ROOT, "custom/multisell");
		multisell = new int[dir.list().length];
		int i = 0;
		for(File f : dir.listFiles())
		{
			if(f.getName().contains(".svn"))
			{
				continue;
			}
			multisell[i] = Integer.parseInt(f.getName().replaceAll(".xml", ""));
			i++;
		}
	}

	@Override
	public void useHandler(int objectId, String command)
	{
		if(command.equalsIgnoreCase("_bbsshop"))
		{
			Community.getInstance().show(objectId, mStaticPage.pageShop);
		}
		else if(command.equalsIgnoreCase("_bbsshopdonate"))
		{
			Community.getInstance().show(objectId, mStaticPage.pageShopDonate);
		}
		else if(command.startsWith("_bbsshop_multisell"))
		{
			L2Player player = L2ObjectsStorage.getPlayer(objectId);
			StringTokenizer st = new StringTokenizer(command, " ");
			st.nextToken();
			int id = Integer.parseInt(st.nextToken());
			for(int i = 0; i < multisell.length; i++)
			{
				if(id == multisell[i])
				{
					L2Multisell.getInstance().SeparateAndSend(id, player, 0);
					break;
				}
			}
		}
		else if(command.equalsIgnoreCase("_bbsshop_sell"))
		{
			L2Player player = L2ObjectsStorage.getPlayer(objectId);
			player.tempInventoryDisable();
			player.sendPacket(new ExBuySellList(null, player, 0));
		}
	}

	@Override
	public String[] getHandlerList()
	{
		String[] s =
			{
				"_bbsshop",
				"_bbsshopdonate",
				"_bbsshop_multisell",
				"_bbsshop_sell"
			};
		return s;
	}
}