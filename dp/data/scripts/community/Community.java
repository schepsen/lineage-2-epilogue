package community;

import l2p.Config;
import l2p.gameserver.cache.Msg;
import l2p.gameserver.model.L2ObjectsStorage;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.modules.community.mCommunity;
import l2p.gameserver.modules.community.mCommunityHandler;
import l2p.gameserver.modules.community.mICommunity;
import l2p.gameserver.modules.community.mICommunityHandler;
import l2p.gameserver.serverpackets.ShowBoard;
import l2p.util.Files;

import java.util.StringTokenizer;

/**
 * User: Shaitan
 * Date: 01.11.2010
 * Time: 7:37:56
 */
public class Community implements mICommunity, mICommunityHandler
{
	private static Community Community = new Community();

	public static Community getInstance()
	{
		return Community;
	}

	public void onLoad()
	{
		if(!Config.communityEnabled)
		{
			return;
		}
		mCommunity.getInstance().set(this);
		mCommunityHandler.getInstance().addHandler(this);
	}

	public void show(int objectId, String s)
	{
		L2Player player = L2ObjectsStorage.getPlayer(objectId);
		if(!Config.communityEnabled)
		{
			player.sendPacket(Msg.THE_COMMUNITY_SERVER_IS_CURRENTLY_OFFLINE);
			return;
		}
		player.sendPacket(new ShowBoard(s));
	}

	public void useHandler(int objectId, String command)
	{
		if(command.equals("_bbshome"))
		{
			show(objectId, mCommunity.pageMain);
		}
		else if(command.equals("_bbsclan"))
		{
			show(objectId, mCommunity.pageClan);
		}
		else if(command.startsWith("_bbscustompage"))
		{
			StringTokenizer st = new StringTokenizer(command, " ");
			st.nextElement();
			String fileName = st.nextToken();
			String htm = Files.read(fileName);
			show(objectId, htm);
		}
		else if(command.startsWith("_bbsinpage"))
		{
			StringTokenizer st = new StringTokenizer(command, " ");
			st.nextElement();
			String fileName = st.nextToken();
			String htm = mGeneratePage.addToTemplate(Files.read(fileName));
			show(objectId, htm);
		}
	}

	public String[] getHandlerList()
	{
		String[] s = {
			"_bbshome", "_bbsclan", "_bbscustompage", "_bbsinpage"
		};
		return s;
	}
}