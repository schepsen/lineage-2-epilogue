package community;

import l2p.Config;
import l2p.common.ThreadPoolManager;
import l2p.database.DatabaseUtils;
import l2p.database.FiltredPreparedStatement;
import l2p.database.L2DatabaseFactory;
import l2p.database.ThreadConnection;
import l2p.gameserver.instancemanager.CastleManager;
import l2p.gameserver.model.L2Alliance;
import l2p.gameserver.model.L2Clan;
import l2p.gameserver.model.entity.residence.Castle;
import l2p.gameserver.modules.community.mCommunity;
import l2p.gameserver.modules.community.mGenerateElement;
import l2p.gameserver.modules.event.mEventList;
import l2p.gameserver.modules.event.mIEvent;
import l2p.gameserver.tables.ClanTable;
import l2p.util.Files;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;

/**
 * @authors Shaitan, hex1r0
 */
public class mGeneratePage
{
	private static mGeneratePage ourInstance;

	public static mGeneratePage getInstance()
	{
		if(ourInstance == null)
		{
			return ourInstance = new mGeneratePage();
		}
		else
		{
			return ourInstance;
		}
	}

	public mGeneratePage()
	{
		mStaticPage.setPageTemplate(generateTemplate());
		mCommunity.pageMain = addToTemplate(Files.read("custom/community/main.htm"));
		mCommunity.pageClan = addToTemplate(addStatisticClan());
		mStaticPage.pageShop = addToTemplate(Files.read("custom/community/shop.htm"));
		mStaticPage.pageShopDonate = addToTemplate(Files.read("custom/community/shopdonate.htm"));
		mStaticPage.pageCommand = addToTemplate(Files.read("custom/community/command.htm"));
		mStaticPage.pageService = addToTemplate(addService());
		generateEvent();
		ThreadPoolManager.getInstance().scheduleGeneralAtFixedRate(new generateStatistic(), 1000, 10 * 60 * 1000);
	}
	//--------------------------------------------------------------------------------------------------------------

	public static String generateTemplate()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("<html>").append("<body>").append("<center>");
		sb.append("<table width=751><tr>");
		sb.append("<td fixwidth=100 valign=top><table width=100>");
		sb.append(mGenerateElement.buttonTDTR("Главная", "_bbshome", 100, 25));
		sb.append(Config.communityShop ? mGenerateElement.buttonTDTR("Магазин", "_bbsshop", 100, 25) : "");
		sb.append(Config.communityShopDonate ? mGenerateElement.buttonTDTR("Донейт Магазин", "_bbsshopdonate", 100, 25) : "");
		sb.append(Config.communityBuffer ? mGenerateElement.buttonTDTR("Баффер", "_bbsbaffer", 100, 25) : "");
		sb.append(Config.communityTeleport ? mGenerateElement.buttonTDTR("Телепорт", "_bbsteleport", 100, 25) : "");
		sb.append(Config.communityCareer ? mGenerateElement.buttonTDTR("Карьера", "_bbscareer", 100, 25) : "");
		sb.append(Config.communityService ? mGenerateElement.buttonTDTR("Сервисы", "_bbsservice", 100, 25) : "");
		sb.append(Config.communitySmsDonate ? mGenerateElement.buttonTDTR("Смс донейт", "_bbssmsdonate", 100, 25) : "");
		sb.append(Config.communityEvent ? mGenerateElement.buttonTDTR("Эвенты", "_bbsevent", 100, 25) : "");
		sb.append(Config.communityStatistic ? mGenerateElement.buttonTDTR("Статистика", "_bbsstatistic", 100, 25) : "");
		sb.append(mGenerateElement.buttonTDTR("Полезное", "_bbscommand", 100, 25));
		sb.append(mGenerateElement.buttonTDTR("Настройки", "user_cfg", 100, 25));
		sb.append("</table></td>");
		sb.append("<td fixwidth=1 valign=top>");
		sb.append(mGenerateElement.line(1, 495));
		sb.append("</td>");
		sb.append("<td fixwidth=650 valign=top><br>");
		sb.append("%main%");
		sb.append("</td>");
		sb.append("</tr></table>");
		sb.append("</center>").append("</body>").append("</html>");
		return sb.toString();
	}

	public static String addToTemplate(String s)
	{
		return mStaticPage.getPageTemplate().replace("%main%", s);
	}
	//--------------------------------------------------------------------------------------------------------------

	private static String addService()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("<center>");
		sb.append("<table fixwidth=200>");
		sb.append(Config.SellNobleEnable ? mGenerateElement.buttonTDTR("Ноблес", "scripts_services.NoblessSell:list", 200, 25) : "");
		sb.append(Config.SERVICES_RATE_BONUS_ENABLED ? mGenerateElement.buttonTDTR("Премиум Аккаунт", "scripts_services.RateBonus:list", 200, 25) : "");
		sb.append(Config.SERVICES_EXPAND_CWH_ENABLED ? mGenerateElement.buttonTDTR("Расширение кланового склада", "scripts_services.ExpandCWH:show", 200, 25) : "");
		sb.append(Config.SERVICES_EXPAND_INVENTORY_ENABLED ? mGenerateElement.buttonTDTR("Расширение инвентаря", "scripts_services.ExpandInventory:show", 200, 25) : "");
		sb.append(Config.SERVICES_EXPAND_WAREHOUSE_ENABLED ? mGenerateElement.buttonTDTR("Расширение личного склада", "scripts_services.ExpandWarhouse:show", 200, 25) : "");
		sb.append(Config.SERVICES_CHANGE_NICK_ENABLED ? mGenerateElement.buttonTDTR("Смена ника", "scripts_services.Rename:rename_page", 200, 25) : "");
		sb.append(Config.SERVICES_CHANGE_SEX_ENABLED ? mGenerateElement.buttonTDTR("Смена пола", "scripts_services.Rename:changesex_page", 200, 25) : "");
		sb.append(Config.SERVICES_CHANGE_NICK_COLOR_ENABLED ? mGenerateElement.buttonTDTR("Цвет ника", "scripts_services.NickColor:list", 200, 25) : "");
		sb.append(Config.SERVICES_CHANGE_TITLE_COLOR_ENABLED ? mGenerateElement.buttonTDTR("Цвет титула", "scripts_services.TitleColor:list", 200, 25) : "");
		sb.append(Config.SERVICES_SEPARATE_SUB_ENABLED ? mGenerateElement.buttonTDTR("Отделить саб", "scripts_services.Rename:separate_page", 200, 25) : "");
		sb.append(Config.SERVICES_CHANGE_BASE_ENABLED ? mGenerateElement.buttonTDTR("Сменить класс", "scripts_services.Rename:changebase_page", 200, 25) : "");
		sb.append(Config.SERVICES_DELEVEL_ENABLED ? mGenerateElement.buttonTDTR("Понизить уровень", "scripts_services.Delevel:delevel_page", 200, 25) : "");
		sb.append(Config.PlayerKillerEnable ? mGenerateElement.buttonTDTR("Отмытие ПК", "scripts_services.Pk:pk_page", 200, 25) : "");
		sb.append(Config.SellHeroEnable ? mGenerateElement.buttonTDTR("Геройство", "scripts_services.HeroService:hero_list", 200, 25) : "");
		sb.append(Config.SERVICES_CHANGE_PET_NAME_ENABLED ? mGenerateElement.buttonTDTR("Обнулить имя у пета", "scripts_services.petevolve.exchange:showErasePetName", 200, 25) : "");
		sb.append(Config.SERVICES_BASH_ENABLED? mGenerateElement.buttonTDTR("Почитать Баш орг", "scripts_services.Bash:showQuote 1", 200, 25) : "");
		sb.append(mGenerateElement.buttonTDTR("Клановые сервисы", "scripts_services.Clans:list", 200, 25));
		sb.append("</table>");
		sb.append("</center>");
		return sb.toString();
	}
	//--------------------------------------------------------------------------------------------------------------

	private static class generateStatistic implements Runnable
	{
		public void run()
		{
			mStaticPage.pageStatistic = addToTemplate(addStatisticMenu(addStatisticPvP()));
			mStaticPage.pageStatisticPvP = mStaticPage.pageStatistic;
			mStaticPage.pageStatisticPK = addToTemplate(addStatisticMenu(addStatisticPK()));
			mStaticPage.pageStatisticFame = addToTemplate(addStatisticMenu(addStatisticFame()));
			mStaticPage.pageStatisticClan = addToTemplate(addStatisticMenu(addStatisticClan()));
			mStaticPage.pageStatisticCastle = addToTemplate(addStatisticMenu(addStatisticCastle()));
			mStaticPage.pageStatisticNoble = addToTemplate(addStatisticMenu(addStatisticNoble()));
		}
	}

	private static String addStatisticMenu(String s)
	{
		StringBuilder sb = new StringBuilder();
		sb.append("<center><table width=200>");
		sb.append("<tr>");
		sb.append("<td FIXWIDTH = 80>").append(mGenerateElement.button("PK", "_bbsstatistic_pk", 80, 25)).append("</td>");
		sb.append("<td FIXWIDTH = 80>").append(mGenerateElement.button("PVP", "_bbsstatistic_pvp", 80, 25)).append("</td>");
		sb.append("<td FIXWIDTH = 80>").append(mGenerateElement.button("Слава", "_bbsstatistic_fame", 80, 25)).append("</td>");
		sb.append("<td FIXWIDTH = 80>").append(mGenerateElement.button("Кланы", "_bbsstatistic_clan", 80, 25)).append("</td>");
		sb.append("<td FIXWIDTH = 80>").append(mGenerateElement.button("Замки", "_bbsstatistic_castle", 80, 25)).append("</td>");
		sb.append("<td FIXWIDTH = 80>").append(mGenerateElement.button("Дворяне", "_bbsstatistic_noble", 80, 25)).append("</td>");
		sb.append("</tr>");
		sb.append("</table>");
		sb.append("<table width=200>");
		sb.append("<tr><td>Обновляется раз в 10 минут</td></tr>");
		sb.append("</table></center>");
		sb.append(s);
		return sb.toString();
	}

	private static String addStatisticPvP()
	{
		ThreadConnection tc = null;
		FiltredPreparedStatement fps = null;
		ResultSet rs = null;
		try
		{
			tc = L2DatabaseFactory.getInstance().getConnection();
			fps = tc.prepareStatement("SELECT * FROM characters ORDER BY pvpkills DESC LIMIT 20;");
			rs = fps.executeQuery();
			StringBuilder sb = new StringBuilder();
			sb.append("<table width=650>");
			sb.append("<tr><td><center>ТОП 20 PVP");
			sb.append("<img src=L2UI.SquareWhite width=450 height=1>");
			sb.append("<table width=450 bgcolor=CCCCCC>");
			sb.append("<tr>");
			sb.append("<td width=250>Ник</td>");
			sb.append("<td width=50>Пол</td>");
			sb.append("<td width=100>Время в игре</td>");
			sb.append("<td width=50>PK</td>");
			sb.append("<td width=50><font color=00CC00>PVP</font></td>");
			sb.append("<td width=100>Статус</td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("<img src=L2UI.SquareWhite width=450 height=1>");
			sb.append("<table width=450>");
			while(rs.next())
			{
				String ChName = rs.getString("char_name");
				int ChSex = rs.getInt("sex");
				int ChGameTime = rs.getInt("onlinetime");
				int ChPk = rs.getInt("pkkills");
				int ChPvP = rs.getInt("pvpkills");
				int ChOnOff = rs.getInt("online");
				String OnOff;
				String color;
				String sex;
				sex = ChSex == 1 ? "Ж" : "М";
				if(ChOnOff == 1)
				{
					OnOff = "Онлайн";
					color = "00CC00";
				}
				else
				{
					OnOff = "Оффлайн";
					color = "D70000";
				}
				sb.append("<tr>");
				sb.append("<td width=250>").append(ChName).append("</td>");
				sb.append("<td width=50>").append(sex).append("</td>");
				sb.append("<td width=100>").append(OnlineTime(ChGameTime)).append("</td>");
				sb.append("<td width=50>").append(ChPk).append("</td>");
				sb.append("<td width=50><font color=00CC00>").append(ChPvP).append("</font></td>");
				sb.append("<td width=100><font color=").append(color).append(">").append(OnOff).append("</font></td>");
				sb.append("</tr>");
			}
			sb.append("</table>");
			sb.append("</center></td></tr></table>");
			return sb.toString();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DatabaseUtils.closeDatabaseCSR(tc, fps, rs);
		}
		return null;
	}

	private static String addStatisticPK()
	{
		ThreadConnection tc = null;
		FiltredPreparedStatement fps = null;
		ResultSet rs = null;
		try
		{
			tc = L2DatabaseFactory.getInstance().getConnection();
			fps = tc.prepareStatement("SELECT * FROM characters ORDER BY pkkills DESC LIMIT 20;");
			rs = fps.executeQuery();
			StringBuilder sb = new StringBuilder();
			sb.append("<table width=650>");
			sb.append("<tr><td><center>ТОП 20 PK");
			sb.append("<img src=L2UI.SquareWhite width=450 height=1>");
			sb.append("<table width=450 bgcolor=CCCCCC>");
			sb.append("<tr>");
			sb.append("<td width=250>Ник</td>");
			sb.append("<td width=50>Пол</td>");
			sb.append("<td width=100>Время в игре</td>");
			sb.append("<td width=50><font color=00CC00>PK</font></td>");
			sb.append("<td width=50>PVP</td>");
			sb.append("<td width=100>Статус</td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("<img src=L2UI.SquareWhite width=450 height=1>");
			sb.append("<table width=450>");
			while(rs.next())
			{
				String ChName = rs.getString("char_name");
				int ChSex = rs.getInt("sex");
				int ChGameTime = rs.getInt("onlinetime");
				int ChPk = rs.getInt("pkkills");
				int ChPvP = rs.getInt("pvpkills");
				int ChOnOff = rs.getInt("online");
				String OnOff;
				String color;
				String sex;
				sex = ChSex == 1 ? "Ж" : "М";
				if(ChOnOff == 1)
				{
					OnOff = "Онлайн";
					color = "00CC00";
				}
				else
				{
					OnOff = "Оффлайн";
					color = "D70000";
				}
				sb.append("<tr>");
				sb.append("<td width=250>").append(ChName).append("</td>");
				sb.append("<td width=50>").append(sex).append("</td>");
				sb.append("<td width=100>").append(OnlineTime(ChGameTime)).append("</td>");
				sb.append("<td width=50><font color=00CC00>").append(ChPk).append("</font></td>");
				sb.append("<td width=50>").append(ChPvP).append("</td>");
				sb.append("<td width=100><font color=").append(color).append(">").append(OnOff).append("</font></td>");
				sb.append("</tr>");
			}
			sb.append("</table>");
			sb.append("</center></td></tr></table>");
			return sb.toString();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DatabaseUtils.closeDatabaseCSR(tc, fps, rs);
		}
		return null;
	}

	private static String addStatisticNoble()
	{
		ThreadConnection tc = null;
		FiltredPreparedStatement fps = null;
		ResultSet rs = null;
		try
		{
			tc = L2DatabaseFactory.getInstance().getConnection();
			fps = tc.prepareStatement("SELECT cha.char_name, cha.sex, cha.onlinetime, cha.pkkills, cha.pvpkills, cha.online FROM characters cha, olympiad_nobles oly WHERE cha.obj_Id = oly.char_id ORDER BY cha.pvpkills DESC LIMIT 20");
			rs = fps.executeQuery();
			StringBuilder sb = new StringBuilder();
			sb.append("<table width=650>");
			sb.append("<tr><td><center>ТОП 20 Дворян");
			sb.append("<img src=L2UI.SquareWhite width=450 height=1>");
			sb.append("<table width=450 bgcolor=CCCCCC>");
			sb.append("<tr>");
			sb.append("<td width=250>Ник</td>");
			sb.append("<td width=50>Пол</td>");
			sb.append("<td width=100>Время в игре</td>");
			sb.append("<td width=50>PK</td>");
			sb.append("<td width=50>PVP</td>");
			sb.append("<td width=100>Статус</td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("<img src=L2UI.SquareWhite width=450 height=1>");
			sb.append("<table width=450>");
			while(rs.next())
			{
				String ChName = rs.getString("char_name");
				int ChSex = rs.getInt("sex");
				int ChGameTime = rs.getInt("onlinetime");
				int ChPk = rs.getInt("pkkills");
				int ChPvP = rs.getInt("pvpkills");
				int ChOnOff = rs.getInt("online");
				String OnOff;
				String color;
				String sex;
				sex = ChSex == 1 ? "Ж" : "М";
				if(ChOnOff == 1)
				{
					OnOff = "Онлайн";
					color = "00CC00";
				}
				else
				{
					OnOff = "Оффлайн";
					color = "D70000";
				}
				sb.append("<tr>");
				sb.append("<td width=250>").append(ChName).append("</td>");
				sb.append("<td width=50>").append(sex).append("</td>");
				sb.append("<td width=100>").append(OnlineTime(ChGameTime)).append("</td>");
				sb.append("<td width=50>").append(ChPk).append("</td>");
				sb.append("<td width=50>").append(ChPvP).append("</td>");
				sb.append("<td width=100><font color=").append(color).append(">").append(OnOff).append("</font></td>");
				sb.append("</tr>");
			}
			sb.append("</table>");
			sb.append("</center></td></tr></table>");
			return sb.toString();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DatabaseUtils.closeDatabaseCSR(tc, fps, rs);
		}
		return null;
	}

	private static String addStatisticCastle()
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
		ThreadConnection tc = null;
		FiltredPreparedStatement fps = null;
		ResultSet rs = null;
		try
		{
			tc = L2DatabaseFactory.getInstance().getConnection();
			fps = tc.prepareStatement("SELECT * FROM castle ORDER BY id DESC LIMIT 10;");
			rs = fps.executeQuery();
			StringBuilder sb = new StringBuilder();
			sb.append("<table width=650>");
			sb.append("<tr><td><center>Статистика Замков");
			sb.append("<img src=L2UI.SquareWhite width=450 height=1>");
			sb.append("<table width=450 bgcolor=CCCCCC>");
			sb.append("<tr>");
			sb.append("<td width=150>Замок</td>");
			sb.append("<td width=150>Налог</td>");
			sb.append("<td width=250>Владелец</td>");
			sb.append("<td width=150>Дата осады</td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("<img src=L2UI.SquareWhite width=450 height=1>");
			sb.append("<table width=450>");
			while(rs.next())
			{
				String name = rs.getString("name");
				String taxPercent = rs.getString("taxPercent");
				String owner = "Нет владельца";
				String siegeDate = "";
				Castle c = CastleManager.getInstance().getCastleByIndex(rs.getInt("id"));
				if(c != null)
				{
					siegeDate = dateFormat.format(c.getSiege().getSiegeDate().getTime());
					L2Clan o = c.getOwner();
					if(o != null)
					{
						owner = o.getName();
					}
				}
				sb.append("<tr>");
				sb.append("<td width=150>").append(name).append("</td>");
				sb.append("<td width=150>").append(taxPercent).append("</td>");
				sb.append("<td width=250>").append(owner).append("</td>");
				sb.append("<td width=150>").append(siegeDate).append("</td>");
				sb.append("</tr>");
			}
			sb.append("</table>");
			sb.append("</center></td></tr></table>");
			return sb.toString();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DatabaseUtils.closeDatabaseCSR(tc, fps, rs);
		}
		return null;
	}

	private static String addStatisticClan()
	{
		ThreadConnection tc = null;
		FiltredPreparedStatement fps = null;
		ResultSet rs = null;
		try
		{
			tc = L2DatabaseFactory.getInstance().getConnection();
			fps = tc.prepareStatement("SELECT * FROM clan_data c WHERE c.clan_level > 0 ORDER BY c.clan_level DESC LIMIT 10;");
			rs = fps.executeQuery();
			StringBuilder sb = new StringBuilder();
			sb.append("<table width=650>");
			sb.append("<tr><td><center>Топ 10 Кланов");
			sb.append("<img src=L2UI.SquareWhite width=450 height=1>");
			sb.append("<table width=450 bgcolor=CCCCCC>");
			sb.append("<tr>");
			sb.append("<td width=250>Клан</td>");
			sb.append("<td width=250>Альянс</td>");
			sb.append("<td width=120>Репутация</td>");
			sb.append("<td width=120>Уровень</td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("<img src=L2UI.SquareWhite width=450 height=1>");
			sb.append("<table width=450>");
			while(rs.next())
			{
				String clanName = rs.getString("clan_name");
				String allyName = "Нет альянса";
				String rep = Integer.toString(rs.getInt("reputation_score"));
				String level = Integer.toString(rs.getInt("clan_level"));
				L2Alliance a = ClanTable.getInstance().getAlliance(rs.getInt("ally_id"));
				if(a != null)
				{
					allyName = a.getAllyName();
				}
				sb.append("<tr>");
				sb.append("<td width=250>").append(clanName).append("</td>");
				sb.append("<td width=250>").append(allyName).append("</td>");
				sb.append("<td width=120>").append(rep).append("</td>");
				sb.append("<td width=120>").append(level).append("</td>");
				sb.append("</tr>");
			}
			sb.append("</table>");
			sb.append("</center></td></tr></table>");
			return sb.toString();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DatabaseUtils.closeDatabaseCSR(tc, fps, rs);
		}
		return null;
	}

	private static String addStatisticFame()
	{
		ThreadConnection tc = null;
		FiltredPreparedStatement fps = null;
		ResultSet rs = null;
		try
		{
			tc = L2DatabaseFactory.getInstance().getConnection();
			fps = tc.prepareStatement("SELECT * FROM characters ORDER BY fame DESC LIMIT 20;");
			rs = fps.executeQuery();
			StringBuilder sb = new StringBuilder();
			sb.append("<table width=650>");
			sb.append("<tr><td><center>ТОП 20 Славы");
			sb.append("<img src=L2UI.SquareWhite width=450 height=1>");
			sb.append("<table width=450 bgcolor=CCCCCC>");
			sb.append("<tr>");
			sb.append("<td width=250>Ник</td>");
			sb.append("<td width=50>Пол</td>");
			sb.append("<td width=100>Время в игре</td>");
			sb.append("<td width=50><font color=00CC00>Слава</font></td>");
			sb.append("<td width=100>Статус</td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("<img src=L2UI.SquareWhite width=450 height=1>");
			sb.append("<table width=450>");
			while(rs.next())
			{
				String ChName = rs.getString("char_name");
				int ChSex = rs.getInt("sex");
				int ChGameTime = rs.getInt("onlinetime");
				int fame = rs.getInt("fame");
				int ChOnOff = rs.getInt("online");
				String OnOff;
				String color;
				String sex;
				sex = ChSex == 1 ? "Ж" : "М";
				if(ChOnOff == 1)
				{
					OnOff = "Онлайн";
					color = "00CC00";
				}
				else
				{
					OnOff = "Оффлайн";
					color = "D70000";
				}
				sb.append("<tr>");
				sb.append("<td width=250>").append(ChName).append("</td>");
				sb.append("<td width=50>").append(sex).append("</td>");
				sb.append("<td width=100>").append(OnlineTime(ChGameTime)).append("</td>");
				sb.append("<td width=50><font color=00CC00>").append(fame).append("</font></td>");
				sb.append("<td width=100><font color=").append(color).append(">").append(OnOff).append("</font></td>");
				sb.append("</tr>");
			}
			sb.append("</table>");
			sb.append("</center></td></tr></table>");
			return sb.toString();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DatabaseUtils.closeDatabaseCSR(tc, fps, rs);
		}
		return null;
	}

	private static String OnlineTime(int time)
	{
		long onlinetimeH;
		int onlinetimeM;
		if(time / 60 / 60 - 0.5 <= 0)
		{
			onlinetimeH = 0;
		}
		else
		{
			onlinetimeH = Math.round((time / 60 / 60) - 0.5);
		}
		onlinetimeM = Math.round(((time / 60 / 60) - onlinetimeH) * 60);
		return "" + onlinetimeH + " ч. " + onlinetimeM + " м.";
	}

	public static void generateEvent()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("<table width=650>");
		for(mIEvent mIEvent : mEventList.getEvents())
		{
			sb.append("<tr>");
			sb.append("<td>");
			sb.append(mIEvent.getName()).append(": ");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(mIEvent.getStatus());
			sb.append("</td>");
			sb.append("<td>");
			sb.append(mGenerateElement.button("Регистрация", "scripts_events_new." + mIEvent.getName() + ":reg", 100, 25));
			sb.append("</td>");
			sb.append("<td>");
			sb.append(mGenerateElement.button("Описание", "scripts_events_new." + mIEvent.getName() + ":getDescription", 100, 25));
			sb.append("</td>");
			if(mIEvent.isStat())
			{
				sb.append("<td>");
				sb.append(mGenerateElement.button("Статистика", "scripts_events_new." + mIEvent.getName() + ":getStat", 100, 25));
				sb.append("</td>");
			}
			sb.append("</tr>");
		}
		sb.append("</table>");
		mStaticPage.pageEvent = addToTemplate(sb.toString());
	}
}