package community.command;

import community.Community;
import l2p.gameserver.modules.community.mCommunityHandler;
import l2p.gameserver.modules.community.mICommunityHandler;
import community.mStaticPage;

/**
 * User: Shaitan
 * Date: 15.02.11
 * Time: 13:05
 */
public class Command implements mICommunityHandler
{
	public void onLoad()
	{
		mCommunityHandler.getInstance().addHandler(this);
	}

	public void useHandler(int objectId, String command)
	{
		if(command.equalsIgnoreCase("_bbscommand"))
		{
			Community.getInstance().show(objectId, mStaticPage.pageCommand);
		}
	}

	public String[] getHandlerList()
	{
		String[] s =
			{
				"_bbscommand"
			};
		return s;
	}
}