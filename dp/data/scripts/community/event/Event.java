package community.event;

import community.Community;
import community.mStaticPage;
import l2p.Config;
import l2p.gameserver.modules.community.mCommunityHandler;
import l2p.gameserver.modules.community.mICommunityHandler;

/**
 * User: Shaitan
 * Date: 27.02.2011
 * Time: 17:05:37
 */
public class Event implements mICommunityHandler
{
	public void onLoad()
	{
		if(!Config.communityEvent)
		{
			return;
		}
		mCommunityHandler.getInstance().addHandler(this);
	}

	@Override
	public void useHandler(int objectId, String command)
	{
		if(command.equals("_bbsevent"))
		{
			Community.getInstance().show(objectId, mStaticPage.pageEvent);
		}
	}

	@Override
	public String[] getHandlerList()
	{
		String[] s =
			{
				"_bbsevent"
			};
		return s;
	}
}