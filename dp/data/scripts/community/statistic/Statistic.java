package community.statistic;

import community.Community;
import community.mStaticPage;
import l2p.Config;
import l2p.gameserver.modules.community.mCommunityHandler;
import l2p.gameserver.modules.community.mICommunityHandler;

/**
 * @authors Shaitan, hex1r0
 */
public class Statistic implements mICommunityHandler
{
	public void onLoad()
	{
		if(!Config.communityStatistic)
		{
			return;
		}
		mCommunityHandler.getInstance().addHandler(this);
	}

	public void useHandler(int objectId, String command)
	{
		if(command.equalsIgnoreCase("_bbsstatistic"))
		{
			Community.getInstance().show(objectId, mStaticPage.pageStatistic);
		}
		else if(command.equalsIgnoreCase("_bbsstatistic_pvp"))
		{
			Community.getInstance().show(objectId, mStaticPage.pageStatisticPvP);
		}
		else if(command.equalsIgnoreCase("_bbsstatistic_pk"))
		{
			Community.getInstance().show(objectId, mStaticPage.pageStatisticPK);
		}
		else if(command.equalsIgnoreCase("_bbsstatistic_fame"))
		{
			Community.getInstance().show(objectId, mStaticPage.pageStatisticFame);
		}
		else if(command.equalsIgnoreCase("_bbsstatistic_clan"))
		{
			Community.getInstance().show(objectId, mStaticPage.pageStatisticClan);
		}
		else if(command.equalsIgnoreCase("_bbsstatistic_castle"))
		{
			Community.getInstance().show(objectId, mStaticPage.pageStatisticCastle);
		}
		else if(command.equalsIgnoreCase("_bbsstatistic_noble"))
		{
			Community.getInstance().show(objectId, mStaticPage.pageStatisticNoble);
		}
	}

	public String[] getHandlerList()
	{
		String[] s =
			{
				"_bbsstatistic",
				"_bbsstatistic_pvp",
				"_bbsstatistic_pk",
				"_bbsstatistic_fame",
				"_bbsstatistic_clan",
				"_bbsstatistic_castle",
				"_bbsstatistic_noble",
			};
		return s;
	}
}