package community;

/**
 * User: Shaitan
 * Date: 29.12.10
 * Time: 18:44
 */
public class mStaticPage
{
	private static String pageTemplate;
	public static String pageCommand;
	public static String pageShop;
	public static String pageShopDonate;
	public static String pageBuffer;
	public static String pageService;
	public static String pageEvent;
	public static String pageCareer;
	public static String pageTeleport;
	public static String pageStatistic;
	public static String pageStatisticPvP;
	public static String pageStatisticPK;
	public static String pageStatisticFame;
	public static String pageStatisticClan;
	public static String pageStatisticCastle;
	public static String pageStatisticNoble;

	public static void setPageTemplate(String s)
	{
		pageTemplate = s;
	}

	public static String getPageTemplate()
	{
		return pageTemplate;
	}
}