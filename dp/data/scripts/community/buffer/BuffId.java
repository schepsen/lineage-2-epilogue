package community.buffer;

/**
 *
 * @author voron.dev
 */
public class BuffId
{
	private int id;
	private int level;

	public BuffId(int id, int level)
	{
		this.id = id;
		this.level = level;
	}

	public int getId()
	{
		return id;
	}

	public int getLevel()
	{
		return level;
	}
}