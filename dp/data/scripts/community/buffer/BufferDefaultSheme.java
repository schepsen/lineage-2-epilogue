/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package community.buffer;

import java.util.ArrayList;

/**
 *
 * @author voron.dev
 */
public class BufferDefaultSheme
{
	private int id;
	private String name;
	private int priceId;
	private int priceCount;
	private ArrayList<BuffId> buffIds = new ArrayList<BuffId>();

	public BufferDefaultSheme(int id, String name, int priceId, int priceCount)
	{
		this.id = id;
		this.name = name;
		this.priceId = priceId;
		this.priceCount = priceCount;
	}

	public int getId()
	{
		return id;
	}

	public String getName()
	{
		return name;
	}

	public int getPriceId()
	{
		return priceId;
	}

	public int getPriceCount()
	{
		return priceCount;
	}

	public void addBuff(int id, int level)
	{
		BuffId buffId = new BuffId(id, level);
		buffIds.add(buffId);
	}

	public ArrayList<BuffId> getBuffIds()
	{
		return buffIds;
	}
}