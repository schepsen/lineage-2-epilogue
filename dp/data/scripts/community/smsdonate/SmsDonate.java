package community.smsdonate;

import community.Community;
import community.mGeneratePage;
import javolution.text.TextBuilder;
import l2p.Config;
import l2p.gameserver.modules.community.mCommunityHandler;
import l2p.gameserver.modules.community.mGenerateElement;
import l2p.gameserver.modules.community.mICommunityHandler;
import l2p.util.Files;

import java.io.File;
import java.util.StringTokenizer;

/**
 * User: Shaitan
 * Date: 28.01.11
 * Time: 7:30
 */
public class SmsDonate implements mICommunityHandler
{
	private String menu = "";

	public void onLoad()
	{
		if(!Config.communitySmsDonate)
		{
			return;
		}
		File dir = new File(Config.DATAPACK_ROOT, "./custom/community/smsdonate");
		for(File f : dir.listFiles())
		{
			menu += f.getName().replace(".htm", "") + ";";
		}
		mCommunityHandler.getInstance().addHandler(this);
	}

	public void useHandler(int objectId, String command)
	{
		if(command.equalsIgnoreCase("_bbssmsdonate"))
		{
			Community.getInstance().show(objectId, mGeneratePage.addToTemplate(showSmsDonate(null)));
		}
		else if(command.startsWith("_bbssmsdonate_page"))
		{
			StringTokenizer st = new StringTokenizer(command, " ");
			st.nextToken();
			Community.getInstance().show(objectId, mGeneratePage.addToTemplate(showSmsDonate(st.nextToken())));
		}
	}

	public String[] getHandlerList()
	{
		String[] s =
			{
				"_bbssmsdonate",
				"_bbssmsdonate_page"
			};
		return s;
	}

	private String showSmsDonate(String text)
	{
		TextBuilder html = new TextBuilder("");
		html.append("<table width=600>");
		html.append("<tr><td>");
		html.append("<combobox width=145 var=\"type\" list=\"" + menu.substring(0, menu.length() - 1) + "\"/>");
		html.append(mGenerateElement.button("Выбрать", "_bbssmsdonate_page $type", 145, 25));
		html.append("</td></tr>");
		if(text != null)
		{
			html.append(Files.read("custom/community/smsdonate/" + text + ".htm"));
		}
		html.append("</table>");
		return html.toString();
	}
}