package ai;

import l2p.gameserver.ai.CtrlIntention;
import l2p.gameserver.ai.DefaultAI;
import l2p.gameserver.model.L2Character;
import l2p.gameserver.model.instances.L2NpcInstance;
import l2p.gameserver.tables.SkillTable;
import l2p.util.GArray;

public class PowderKeg extends DefaultAI
{
	public PowderKeg(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{
		L2NpcInstance actor = getActor();
		if(actor == null)
		{
			return;
		}
		GArray<L2Character> targetList = new GArray<L2Character>();
		for(L2Character character : actor.getAroundCharacters(900, 200))
		{
			if(character.isNpc())
			{
				targetList.add(character);
			}
		}
		if(targetList.size() == 0)
		{
			return;
		}
		for(int i = 0; i < targetList.size(); i++)
		{
			L2Character target = targetList.get(i);
			target.setCurrentHp(target.getCurrentHp() - 10000, true);
			if(target.getCurrentHp() <= 0)
			{
				target.getAI().setIntention(CtrlIntention.AI_INTENTION_IDLE);
			}
		}
		// Only for displaying animation in client
		actor.doCast(SkillTable.getInstance().getInfo(5714, 1), attacker, true);
		actor.getAI().setIntention(CtrlIntention.AI_INTENTION_IDLE);
	}

	@Override
	protected boolean randomWalk()
	{
		return false;
	}

	@Override
	protected boolean randomAnimation()
	{
		return false;
	}
}