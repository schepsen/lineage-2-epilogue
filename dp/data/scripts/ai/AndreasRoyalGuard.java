package ai;

import l2p.gameserver.ai.Fighter;
import l2p.gameserver.model.L2Character;
import l2p.gameserver.model.instances.L2NpcInstance;
import l2p.gameserver.tables.SkillTable;

public class AndreasRoyalGuard extends Fighter
{
	public AndreasRoyalGuard(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{
		L2NpcInstance actor = getActor();
		if((actor.getCurrentHpPercents() <= 70))
		{
			actor.doCast(SkillTable.getInstance().getInfo(4612, 9), attacker, true);
			actor.doDie(actor);
		}
		super.onEvtAttacked(attacker, damage);
	}
}