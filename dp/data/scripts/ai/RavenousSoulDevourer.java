package ai;

import l2p.gameserver.ai.CtrlEvent;
import l2p.gameserver.ai.CtrlIntention;
import l2p.gameserver.ai.Fighter;
import l2p.gameserver.model.L2Character;
import l2p.gameserver.model.L2ObjectsStorage;
import l2p.gameserver.model.instances.L2NpcInstance;

public class RavenousSoulDevourer extends Fighter
{
	private int destroyedTumor = 32531;

	public RavenousSoulDevourer(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtSpawn()
	{
		L2NpcInstance actor = getActor();
		if(actor == null)
		{
			return;
		}
		for(L2NpcInstance npc : L2ObjectsStorage.getAllByNpcId(destroyedTumor, true))
		{
			if(npc.getReflectionId() == actor.getReflectionId())
			{
				actor.getAI().notifyEvent(CtrlEvent.EVT_ATTACKED, npc, 5000);
			}
		}
		super.onEvtSpawn();
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{
		L2NpcInstance actor = getActor();
		if(actor == null)
		{
			return;
		}
		if(attacker.getNpcId() == destroyedTumor)
		{
			actor.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, attacker, 5000);
			attacker.addDamageHate(actor, 0, 2);
			startRunningTask(2000);
			setIntention(CtrlIntention.AI_INTENTION_ATTACK, attacker);
		}
		super.onEvtAttacked(attacker, damage);
	}
}