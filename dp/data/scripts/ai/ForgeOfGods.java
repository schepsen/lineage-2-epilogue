package ai;

import l2p.gameserver.ai.CtrlEvent;
import l2p.gameserver.ai.Fighter;
import l2p.gameserver.model.L2Character;
import l2p.gameserver.model.L2Spawn;
import l2p.gameserver.model.instances.L2NpcInstance;
import l2p.gameserver.tables.NpcTable;
import l2p.util.Rnd;

public class ForgeOfGods extends Fighter
{
	private static final int MOBS[] = {18799, 18800, 18801, 18802, 18803};

	public ForgeOfGods(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtDead(L2Character killer)
	{
		L2NpcInstance actor = getActor();
		if(actor == null)
		{
			return;
		}
		if(actor.isDead())
		{
			if(Rnd.chance(40)) //TODO: Уточнить шанс
			{
				for(int i = 0; i < 1; i++)
				{
					try
					{
						L2Spawn spawn = new L2Spawn(NpcTable.getTemplate(MOBS[Rnd.get(MOBS.length)]));
						spawn.setLoc(actor.getLoc());
						L2NpcInstance npc = spawn.doSpawn(true);
						npc.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, killer, Rnd.get(1, 100));
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
				}
			}
		}
		super.onEvtDead(killer);
	}
}