package ai;

import l2p.extensions.scripts.Functions;
import l2p.gameserver.ai.DefaultAI;
import l2p.gameserver.model.L2Character;
import l2p.gameserver.model.instances.L2NpcInstance;
import l2p.util.Location;
import l2p.util.Rnd;

public class GhostOfVonHellmannsPage extends DefaultAI
{
	static final Location[] points = {new Location(51462, -54539, -3176), new Location(51870, -54398, -3176),
		new Location(52164, -53964, -3176), new Location(52390, -53282, -3176), new Location(52184, -52968, -3154),
		new Location(52120, -52776, -3136), new Location(52136, -51562, -3096)};
	private static final String[] sayToPlayer = {
		"Follow me...", "This where that here...", "I want to speak to you..."};
	private long _lastSay;
	private int current_point = -1;
	private long wait_timeout = 0;
	private boolean wait = false;

	public GhostOfVonHellmannsPage(L2Character actor)
	{
		super(actor);
	}

	@Override
	public boolean isGlobalAI()
	{
		return true;
	}

	@Override
	protected boolean thinkActive()
	{
		L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
		{
			return true;
		}
		if(_def_think)
		{
			doTask();
			return true;
		}
		if(System.currentTimeMillis() > wait_timeout && (current_point > -1 || Rnd.chance(5)))
		{
			if(!wait)
			{
				switch(current_point)
				{
					case 0:
						wait_timeout = System.currentTimeMillis() + 10000;
						wait = true;
						return true;
					case 8:
						wait_timeout = System.currentTimeMillis() + 10000;
						wait = true;
						return true;
				}
			}
			wait_timeout = 0;
			wait = false;
			current_point++;
			if(current_point >= points.length)
			{
				current_point = 0;
			}
			actor.setRunning();
			addTaskMove(points[current_point], true);
			doTask();
			return true;
		}
		if(System.currentTimeMillis() - _lastSay > 20000)
		{
			Functions.npcSay(actor, sayToPlayer[Rnd.get(sayToPlayer.length)]);
			_lastSay = System.currentTimeMillis();
		}
		if(randomAnimation())
		{
			return true;
		}
		return false;
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{
	}

	@Override
	protected void onEvtAggression(L2Character target, int aggro)
	{
	}
}