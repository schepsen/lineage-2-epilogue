package ai;

import java.util.ArrayList;
import java.util.Random;

import l2p.gameserver.ai.CtrlIntention;
import l2p.gameserver.ai.Fighter;
import l2p.gameserver.model.L2Character;
import l2p.gameserver.model.instances.L2NpcInstance;
import l2p.util.Rnd;

//здесь айди мобов, которые будут атакованы теми, у кого будет прописан данный АИ. Вся группа мобов будеть бить рандомную цель из списка.

public class MobVSMobRandom extends Fighter {
	int[] targets = {20016};
	private static final int range = 600;

	public MobVSMobRandom(L2Character actor) {
		super(actor);
	}

	public MobVSMobRandom(L2Character actor, int[] TargetNpcIds) {
		super(actor);
		targets = TargetNpcIds;
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage) {
		if (getActor().isConfused()) {
			getActor().stopConfused();
		}

		super.onEvtAttacked(attacker, damage);
	}

	@Override
	protected boolean thinkActive() {

		ArrayList<L2NpcInstance> aroundnpc = new ArrayList<L2NpcInstance>();

		for (L2NpcInstance npc : getActor().getAroundNpc(
				getActor().getAggroRange(), range)) {

			if (isTargetNPC(npc.getNpcId())) {

				aroundnpc.add(npc);

			}

		}

		if (!aroundnpc.isEmpty()) {

			Random test = new Random(1);

			int randomMob = test.nextInt(aroundnpc.size());

			L2NpcInstance randmob = aroundnpc.get(randomMob);

			getActor().setTarget(randmob);
			getActor().startConfused();
			getActor().addDamageHate(randmob, 0, Rnd.get(100, 1000));
			getActor().sendChanges();

			setIntention(CtrlIntention.AI_INTENTION_ATTACK, randmob, null);

		}

		return super.thinkActive();
	}

	private boolean isTargetNPC(int id) {
		for (int n : targets) {
			if (n == id) {
				return true;
			}
		}

		return false;
	}

}
