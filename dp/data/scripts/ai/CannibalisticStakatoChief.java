package ai;

import l2p.extensions.scripts.Functions;
import l2p.gameserver.ai.Fighter;
import l2p.gameserver.model.L2Character;
import l2p.gameserver.model.L2Player;
import l2p.util.Rnd;

/**
 * User: Shaitan
 * Date: 11.02.11
 * Time: 16:02
 */
public class CannibalisticStakatoChief extends Fighter
{
	private static int[] items = {14833, 14834};

	public CannibalisticStakatoChief(L2Character actor)
	{
		super(actor);
	}

	protected void onEvtDead(L2Character killer)
	{
		if(killer instanceof L2Player)
		{
			L2Player player = (L2Player) killer;
			if(player.getParty() != null)
			{
				for(L2Player member : player.getParty().getPartyMembers())
				{
					Functions.addItem(member, items[Rnd.get(1)], 1);
				}
			}
			else
			{
				Functions.addItem(player, items[Rnd.get(1)], 1);
			}
		}
		super.onEvtDead(killer);
	}
}