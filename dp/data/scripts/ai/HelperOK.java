package ai;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import l2p.extensions.scripts.Functions;
import l2p.gameserver.ai.CtrlIntention;
import l2p.gameserver.ai.Fighter;
import l2p.gameserver.model.L2Character;
import l2p.gameserver.model.instances.L2NpcInstance;
import l2p.gameserver.tables.SkillTable;
import l2p.gameserver.templates.L2NpcTemplate;
import l2p.util.Rnd;



public class HelperOK extends Fighter
{

	
	int[] targets = {20012, 20016, 20015};

	public HelperOK(L2Character actor)
	{
		super(actor);

	}

	public HelperOK(L2Character actor, int[] TargetNpcIds)
	{
		super(actor);
		targets = TargetNpcIds;
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{
		if(getActor().isConfused())
		{
			getActor().stopConfused();
		}

		super.onEvtAttacked(attacker, damage);
		
		
		
		
		
		
		
		
		
		
		
		L2NpcInstance actor = getActor();
		if(actor == null || attacker == null || attacker.getPlayer() == null)
		{
			return;
		}
		actor.startAttackStanceTask();

		if(System.currentTimeMillis() - _lastAction > 3000)
		{
			int chance = Rnd.get(0, 100);
			if(chance < 2)
			{
				attacker.getPlayer().setKarma(attacker.getPlayer().getKarma() + 5);
				attacker.sendChanges();
			}
			else if(chance < 4)
			{
				actor.doCast(SkillTable.getInstance().getInfo(4578, 1), attacker, true);
			}
			else
			{
				actor.doCast(SkillTable.getInstance().getInfo(4185, 7), attacker, true);
			}
			Functions.npcSay(actor, attacker.getName() + ", " + _retreatText[Rnd.get(_retreatText.length)]);
			_lastAction = System.currentTimeMillis();

		}
		
		
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	

	@Override
	protected boolean thinkActive()
	{
		
		
		ArrayList<L2NpcInstance> aroundnpc = new ArrayList<L2NpcInstance>();
		
		
		for(L2NpcInstance npc : getActor().getAroundNpc(getActor().getAggroRange(), range))
		{
		
			
			
			if(isTargetNPC(npc.getNpcId()))
			{
				
				aroundnpc.add(npc);
				
				
			}
			
			
			
			
		}
		
		if(!aroundnpc.isEmpty()){
			
			
			Random test = new Random(1);
			
			int randomMob = test.nextInt(aroundnpc.size());
			
			getActor().setTitle("Under Attack");
			getActor().isAggressive(); //need test
			getActor().getTemplate().shots = L2NpcTemplate.ShotsType.SOUL;
			
			L2NpcInstance randmob = aroundnpc.get(randomMob);
			
			getActor().setTarget(randmob);
			getActor().startConfused();
			//todo random hatetarget
			getActor().addDamageHate(randmob, 0, Rnd.get(100, 1000));
			getActor().sendChanges();
			
			
			setIntention(CtrlIntention.AI_INTENTION_ATTACK, randmob, null);
			
		}
		
		

		return super.thinkActive();
	}

	private boolean isTargetNPC(int id)
	{
		for(int n : targets)
		{
			if(n == id)
			{
				return true;
			}
		}

		return false;
	}

	
	
	
	
	
	
	
	



	
	
	
	
	private  final int range = 600;
	private  final int voicetime = 40000;
	private long _lastAction;

	
	
	
	
	

	
	
	final String filename = "./data/scripts/ai/Replics/Allgemeines.txt";
	File theFile = new File(filename);

	BufferedReader theFileReader = null;

	String ReplicsFromTxT()

	{

		
		
		
		// подсчитываем кол-во строк
        int count = 0;
        try {
        	theFileReader = new BufferedReader(new FileReader(filename));
            while (true) {
                String line = theFileReader.readLine();
                if (line == null) {
                    break;
                }
                

                ++count;
            }
        }
            catch (IOException e) {
				e.printStackTrace();
			}
        
		
		
		// выбираем случайную строку
		String line = "";
		try {
			theFileReader = new BufferedReader(new FileReader(filename));
			Random generator = new Random();
			int randomNumber = generator.nextInt(count) + 1;
			for (int i = 0; i < randomNumber; ++i) {
				try {
					line = theFileReader.readLine();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		catch (FileNotFoundException e1) {

			e1.printStackTrace();
		}

		{
			return line;
		}

	}
	
	final String[] _retreatText = {(String) ReplicsFromTxT() };
	private final String[] _fightText = {"pizdez", "idi nah",
		"olololo"};

	
	

	

	@Override
	protected boolean createNewTask()
	{
		L2NpcInstance actor = getActor();
		if(actor == null)
		{
			return false;
		}
		if(Rnd.chance(60))
		{

			L2Character target;
			if((target = prepareTarget()) == null)
			{
				return false;
			}
			addTaskAttack(target);
			if(System.currentTimeMillis() - _lastAction > voicetime)
			{
				Functions.npcSay(actor, _fightText[Rnd.get(_fightText.length)]);
				_lastAction = System.currentTimeMillis();
			}
			return true;
		}

		if(System.currentTimeMillis() - _lastAction > voicetime)
		{
			Functions.npcSay(actor, _retreatText[Rnd.get(_retreatText.length)]);
			_lastAction = System.currentTimeMillis();
		}
		return true;
	}

	}

