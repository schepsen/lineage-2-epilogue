package ai;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import l2p.extensions.scripts.Functions;
import l2p.gameserver.ai.CtrlIntention;
import l2p.gameserver.ai.Fighter;
import l2p.gameserver.model.L2Character;
import l2p.gameserver.model.instances.L2NpcInstance;
import l2p.util.Rnd;

public class Legion extends Fighter
{

    int[] targets =
    { 20012, 20016, 20015 };

    ArrayList<String> Ausrufe = ReplicsFromTxT();
    private final String[] OnAttackText =
    { "Гасим его!", "Ассист!", "Навались!" };
    final String[] WithNameText =
    { "я убью тебя.", "пытаешься что-то доказать?", "хочешь, я предскажу твое будущее? :)" };
    private final int range = 1000;
    private final int voicetime = 4000;
    private long lastAction;

    public Legion(L2Character actor)
    {
        super(actor);

    }

    public Legion(L2Character actor, int[] TargetNpcIds)
    {
        super(actor);
        targets = TargetNpcIds;
    }

    @Override
    protected void onEvtAttacked(L2Character attacker, int damage)
    {
        if (getActor().isConfused())
        {
            getActor().stopConfused();
        }

        super.onEvtAttacked(attacker, damage);

        L2NpcInstance actor = getActor();
        if (actor == null || attacker == null || attacker.getPlayer() == null)
        {
            return;
        }
        actor.startAttackStanceTask();

        if (System.currentTimeMillis() - lastAction > 30000)
        {
            int chance = Rnd.get(0, 100);
            if (chance < 50)
            {		
            	//пишет в общий чат, указывая ник
            	String name = attacker.getName();
                Functions.npcSay(actor, name + ", " + WithNameText[Rnd.get(WithNameText.length)]);

                lastAction = System.currentTimeMillis();
            }
        }
    }

    @Override
    protected boolean thinkActive()
    {
        for (L2NpcInstance npc : getActor().getAroundNpc(getActor().getAggroRange(), range))
        {
            if (isTargetNPC(npc.getNpcId()))
            {
                getActor().startConfused();
                getActor().setTarget(npc);
                getActor().addDamageHate(npc, 0, 500);
                setIntention(CtrlIntention.AI_INTENTION_ATTACK, npc, null);
                break;
            }

            int chance2 = Rnd.get(0, 100);
            if (chance2 < 7)
            {
            // выкривает рандомные фразы из текстового файла
            Functions.npcSay(getActor(), Ausrufe.get(new Random().nextInt(Ausrufe.size())));

        }
        }

        return super.thinkActive();
    }

    private boolean isTargetNPC(int id)
    {
        for (int n : targets)
        {
            if (n == id)
            {
                return true;
            }
        }

        return false;
    }

    final String filename = "./data/scripts/ai/Replics/Allgemeines.txt";
    File theFile = new File(filename);

    ArrayList<String> ReplicsFromTxT()
    {
        ArrayList<String> Replics = new ArrayList<String>();
        try
        {
            BufferedReader replics = new BufferedReader(new FileReader(filename));

            while (replics.readLine() != null)
            {

                Replics.add(replics.readLine());
            }

        } catch (IOException e)
        {
            e.printStackTrace();
        }
        return Replics;

    }

    @Override
    protected boolean createNewTask()
    {
        L2NpcInstance actor = getActor();
        if (actor == null)
        {
            return false;
        }
        if (Rnd.chance(50))
        {

            L2Character target;
            if ((target = prepareTarget()) == null)
            {
                return false;
            }
            addTaskAttack(target);
            if (System.currentTimeMillis() - lastAction > voicetime)
            {
                Functions.npcSay(actor, OnAttackText[Rnd.get(OnAttackText.length)]);
                lastAction = System.currentTimeMillis();
            }

        }

        return true;

    }
}