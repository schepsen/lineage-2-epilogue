package ai;

import l2p.gameserver.ai.CtrlIntention;
import l2p.gameserver.ai.Fighter;
import l2p.gameserver.model.L2Character;
import l2p.gameserver.model.instances.L2NpcInstance;

//здесь айди мобов, которые будут атакованы теми, у кого будет прописан данный АИ. Вся группа мобов будеть бить одну цель пока она не умрет.

public class MobVSMobAssist extends Fighter {
	int[] targets = {60000};

	public MobVSMobAssist(L2Character actor) {
		super(actor);
	}

	public MobVSMobAssist(L2Character actor, int[] TargetNpcIds) {
		super(actor);
		targets = TargetNpcIds;
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage) {
		if (getActor().isConfused()) {
			getActor().stopConfused();
		}

		super.onEvtAttacked(attacker, damage);
	}

	@Override
	protected boolean thinkActive() {
		for (L2NpcInstance npc : getActor().getAroundNpc(
				getActor().getAggroRange(), 200)) {
			if (isTargetNPC(npc.getNpcId())) {
				getActor().startConfused();
				getActor().setTarget(npc);
				getActor().addDamageHate(npc, 0, 500);
				setIntention(CtrlIntention.AI_INTENTION_ATTACK, npc, null);
				break;
			}
		}

		return super.thinkActive();
	}

	private boolean isTargetNPC(int id) {
		for (int n : targets) {
			if (n == id) {
				return true;
			}
		}

		return false;
	}

}
