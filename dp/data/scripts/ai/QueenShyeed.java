package ai;

import l2p.gameserver.ai.Fighter;
import l2p.gameserver.instancemanager.ZoneManager;
import l2p.gameserver.model.L2Character;
import l2p.gameserver.model.L2Zone;
import l2p.gameserver.model.L2Zone.ZoneType;

/**
 * Ai для Queen Syeed в Stakato nest
 * При смерти меняет зонатор(Выключает одну зону включает другую...)
 */
public class QueenShyeed extends Fighter
{
	public QueenShyeed(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtSpawn()
	{
		L2Zone buffZone = ZoneManager.getInstance().getZoneById(ZoneType.dummy, 999222, false);
		L2Zone debuffZone = ZoneManager.getInstance().getZoneById(ZoneType.damage, 999223, false);
		if(buffZone != null && debuffZone != null)
		{
			buffZone.setActive(false);
			debuffZone.setActive(true);
		}
		super.onEvtSpawn();
	}

	@Override
	protected void onEvtDead(L2Character killer)
	{
		L2Zone buffZone = ZoneManager.getInstance().getZoneById(ZoneType.dummy, 999222, false);
		L2Zone debuffZone = ZoneManager.getInstance().getZoneById(ZoneType.damage, 999223, false);
		if(buffZone != null && debuffZone != null)
		{
			buffZone.setActive(true);
			debuffZone.setActive(false);
		}
		super.onEvtDead(killer);
	}
}