package ai;

import l2p.gameserver.ai.Fighter;
import l2p.gameserver.model.L2Character;
import l2p.gameserver.model.instances.L2DoorInstance;
import l2p.gameserver.modules.data.DoorTable;

/**
 * User: Shaitan
 * Date: 23.01.11
 * Time: 11:49
 */
public class Darion extends Fighter
{
	private static int[] doors = {20250004, 20250005, 20250006, 20250007, 20250009};

	public Darion(L2Character actor)
	{
		super(actor);
	}

	protected void onEvtSpawn()
	{
		doors(false);
		super.onEvtSpawn();
	}

	protected void onEvtDead(L2Character killer)
	{
		doors(true);
		super.onEvtDead(killer);
	}

	private static void doors(boolean open)
	{
		for(int i : doors)
		{
			L2DoorInstance door = DoorTable.getInstance().getDoor(i);
			if(open)
			{
				door.openMe();
			}
			else
			{
				door.closeMe();
			}
		}
	}
}