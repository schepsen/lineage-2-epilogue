package ai;

import javolution.util.FastMap;
import l2p.gameserver.ai.Fighter;
import l2p.gameserver.instancemanager.ZoneManager;
import l2p.gameserver.model.L2Character;
import l2p.gameserver.model.L2Player;
import l2p.gameserver.model.L2Skill;
import l2p.gameserver.model.L2Summon;
import l2p.gameserver.model.L2Zone;
import l2p.gameserver.model.L2Zone.ZoneType;
import l2p.gameserver.model.instances.L2NpcInstance;
import l2p.gameserver.tables.SkillTable;
import l2p.util.Location;

/**
 * AI боса Demon Prince для Tower of Infinitum:
 * - при смерти спаунит портал.
 * - на 10% ХП использует скилл NPC Ultimate Defense(5044.3)
 *
 * @author SYS
 */
public class DemonPrince extends Fighter
{
	private static final int ULTIMATE_DEFENSE_SKILL_ID = 5044;
	private static final L2Skill ULTIMATE_DEFENSE_SKILL = SkillTable.getInstance().getInfo(ULTIMATE_DEFENSE_SKILL_ID, 3);
	private static final long TELEPORT_BACK_BY_INACTIVITY_INTERVAL = 5 * 60 * 1000; // 5 мин
	private boolean _notUsedUltimateDefense = true;
	private long _lastAttackTime = 0;
	private static FastMap<Integer, L2Zone> _floorZones = null;
	private static final int ZONE_OFFSET = 705000;

	public DemonPrince(L2Character actor)
	{
		super(actor);
		if(_floorZones == null)
		{
			_floorZones = new FastMap<Integer, L2Zone>(10);
			for(int i = 1; i < 11; i++)
			{
				_floorZones.put(i, ZoneManager.getInstance().getZoneById(ZoneType.dummy, ZONE_OFFSET + i, true));
			}
		}
	}

	@Override
	protected void onEvtSpawn()
	{
		L2NpcInstance actor = getActor();
		if(actor == null)
		{
			return;
		}
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{
		L2NpcInstance actor = getActor();
		if(actor == null)
		{
			return;
		}
		_lastAttackTime = System.currentTimeMillis();
		if(_notUsedUltimateDefense && actor.getCurrentHpPercents() < 10)
		{
			_notUsedUltimateDefense = false;
			// FIXME Скилл использует, но эффект скила не накладывается.
			clearTasks();
			addTaskBuff(actor, ULTIMATE_DEFENSE_SKILL);
		}
		super.onEvtAttacked(attacker, damage);
	}

	@Override
	protected boolean thinkActive()
	{
		L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
		{
			return true;
		}
		// Портает на этаж вниз, если боса долго не били
		if(_lastAttackTime != 0 && _lastAttackTime + TELEPORT_BACK_BY_INACTIVITY_INTERVAL < System.currentTimeMillis())
		{
			// Портаем всех игроков с 5 на 4 этаж
			for(L2Character c : actor.getAroundCharacters(3500, 200))
			{
				if(c != null && c.isPlayer() && _floorZones.get(5).checkIfInZone(c))
				{
					c.teleToLocation(_floorZones.get(4).getSpawn());
				}
			}
			_lastAttackTime = 0;
			return true;
		}
		return super.thinkActive();
	}

	@Override
	protected void onEvtDead(L2Character killer)
	{
		_notUsedUltimateDefense = true;
		_lastAttackTime = 0;
		L2Player player = null;
		if(killer instanceof L2Player)
		{
			player = (L2Player) killer;
		}
		else if(killer instanceof L2Summon)
		{
			player = killer.getPlayer();
		}
		if(player != null)
		{
			player.getReflection().setReturnLoc(new Location(-19016, 280088, -8287));
			player.getReflection().setCoreLoc(player.getReflection().getReturnLoc());
			player.getReflection().startCollapseTimer(30 * 1000);
		}
		super.onEvtDead(killer);
	}
}