package ai;

import l2p.gameserver.ai.Fighter;
import l2p.gameserver.model.L2Character;
import l2p.gameserver.model.instances.L2NpcInstance;

/**
 * - АИ для РБ Golkonda.
 * - Если Z координаты меньше или больше предназначеных, телепортируется обратно и ресает хп.
 */
public class Golkonda extends Fighter
{
	private static final int z1 = 6900;
	private static final int z2 = 7500;

	public Golkonda(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{
		L2NpcInstance actor = getActor();
		int z = actor.getZ();
		if(z > z2 || z < z1)
		{
			actor.teleToLocation(116313, 15896, 6999);
			actor.setCurrentHp(actor.getMaxHp(), false);
		}
		super.onEvtAttacked(attacker, damage);
	}
}