package ai;

import instances.TullyWorkshop;
import l2p.gameserver.ai.Fighter;
import l2p.gameserver.model.L2Character;

/**
 * User: Shaitan
 * Date: 23.01.11
 * Time: 9:05
 */
public class Tully extends Fighter
{
	public Tully(L2Character actor)
	{
		super(actor);
	}

	protected void onEvtSpawn()
	{
		TullyWorkshop.Tully = true;
		super.onEvtSpawn();
	}

	protected void onEvtDead(L2Character killer)
	{
		TullyWorkshop.Tully = false;
		super.onEvtDead(killer);
	}
}